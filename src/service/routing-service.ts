import { ModelView } from "../util/model-view";
import { injectable, inject, Container } from "inversify";
import { UiService } from "./ui-service";
import { Router } from "framework7/modules/router/router";
import service from "../decorators/service";




@service()
class RoutingService {

    constructor(
        private uiService:UiService,
        @inject("framework7") public app
     ) {}


    navigate(navigateParams:Router.NavigateParameters, routeOptions?: Router.RouteOptions, viewName:string='main') {

        console.log(`${viewName}: navigating to ${JSON.stringify(navigateParams)}`)

        if (!routeOptions) routeOptions = {
            reloadCurrent: true,
            ignoreCache: false
        }

        let view = this.app.view[viewName]

        if (view) {
            view.router.navigate( navigateParams, routeOptions)
        } else {
            console.log(`Could not find view ${viewName}`)
        }

    }

    buildRoutesForContainer(existing, container:Container) : Router.RouteParameters[]  {

        let routes:Router.RouteParameters[] = []

        routes = routes.concat(existing)

        //Look up requestMappings 
        for (let mappedRoute of globalThis.mappedRoutes) {

            //Look up matching bean
            let controllerBean = container.get(mappedRoute.controllerClass)

            routes.push( {
                path: mappedRoute.path,
                async: async (routeTo, routeFrom, resolve, reject) => {
                    try {
                        await this._doResolve(routeTo, routeFrom, resolve, reject, controllerBean[mappedRoute.action]())
                    } catch (ex) {
                        this.uiService.showExceptionPopup(ex)
                    }
                }
            })
        }


        //Needs to be last
        routes.push({
            path: '(.*)',
            // url: 'pages/404.html',
            async async(routeTo, routeFrom, resolve, reject) {
                // this.uiService.showPopup("Page was not found")
                console.log(`404 error: ${routeTo.path}`)
            }
        })
        console.log(routes)
        return routes

    }

    private async _doResolve(routeTo, routeFrom, resolve, reject, controller_promise: Promise<ModelView>) {

        //TODO: this should probably be optional somehow. 
        this.uiService.showSpinner()

        let modelView: ModelView = await controller_promise;
        if (!modelView) return

        let model:Function = await modelView.model

        let contextData:string 

        //If we get passed a component it means we're trying to resolve back to the same one.
        //See SpaceComponent.formSubmit. Still a work in progress. 
        if (routeTo.context?.component) {
            contextData = routeTo.context?.data
        } else {
            contextData = routeTo.context
        }


        let updatedState = await model(contextData)

        if (modelView.view) {

            //Load the new component if it's given to us. 
            resolve({
                component: modelView.view
            },
                {
                    context: updatedState
                })    

        } else {

            //Otherwise update the state of the current component.
            if (routeTo.context) {
                let component = routeTo.context.component
                component.$setState(updatedState)
            }


        }

        this.uiService.hideSpinner()

    }


}

interface Route {
    path: string 
    method: string 
}


export {
    RoutingService, Route
}