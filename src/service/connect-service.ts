import { IpfsService } from "./ipfs-service";
import { ConnectInfo } from "../dto/connect-info";
import service from "../decorators/service";

@service()
class ConnectService {

    constructor(
        private ipfsService:IpfsService
    ) {}

    async getConnectInfo() : Promise<ConnectInfo> {

        let peers = await this.ipfsService.ipfs.swarm.peers()
            
        peers = peers.map(e => e.addr.toString())

        let subscribed = await this.ipfsService.ipfs.pubsub.ls()

        let id = await this.ipfsService.ipfs.id()

        let ipfsInfo = {
            addresses: id.addresses.map(e => e.toString()),
            agentVersion: id.agentVersion,
            id: id.id,
            publicKey: id.publicKey 
        }


        return {
            peers: peers,
            ipfsInfo: ipfsInfo,
            peerCount: peers.length,
            subscribed: subscribed
        }

    }

    async addPeer(peerAddress:string) {
        return this.ipfsService.ipfs.swarm.connect(peerAddress)
    }


}



export {
    ConnectService
}