import { Container } from "inversify"
import service from "../decorators/service"

@service()
class BindingService {

    public async initBindings(container:Container) {

        let initilizableBindings = this._getInitializableBindings(container)
        console.log(initilizableBindings)
        for (let bean of initilizableBindings) {
            await bean.init()
        }
    }

    private _getInitializableBindings(container:Container) : any[] {

        let bindings:any[] = []

        //TODO: probably this shouldn't be a hardcoded list

        let serviceIdentifiers = this._getServiceIdentifiers(container)
        serviceIdentifiers = serviceIdentifiers.filter(id => {
            if (this._isSkipBinding(container, id)) return false 
            if (id == "framework7") return false 
            if (this._isInitializable(container, id)) return true 

            return false
        })

        for (let si of serviceIdentifiers) {
            bindings.push(container.get(si))
        }

        return bindings
    }

    private _getServiceIdentifiers(container:Container) : any[] {

        //@ts-ignore
        let original = container._bindingDictionary

        let serviceIdentifiers = []

        original.traverse((key, value) => {
            value.forEach((binding) => {
                serviceIdentifiers.push(binding.serviceIdentifier)
            })
        })

        return serviceIdentifiers
    }

    private _isInitializable(container:Container, obj): boolean { 
        let bean:any = container.get(obj)
        return bean.init != undefined
    }

    private _isSkipBinding(container:Container, obj): boolean { 
        let bean:any = container.get(obj)
        return bean.skipBinding == true
    }

}


//TODO:this probably sucks
interface BindingInfo {
    getClassName():string 
}

export {
    BindingService, BindingInfo
}