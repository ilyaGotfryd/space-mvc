import { inject } from "inversify";
import service from "../decorators/service";
import { BindingInfo } from "./binding-service";

const IPFS = require('ipfs')

@service()
class IpfsService implements BindingInfo {

    public skipBinding = true
    public ipfs 

    constructor(
        @inject("ipfsOptions") private ipfsOptions
    ){
    }

    getClassName(): string {
        return "IpfsService"
    }

    async init() {
        this.ipfs = await IPFS.create(this.ipfsOptions)
    }

}


export {
    IpfsService
}