const { ethers, Wallet, providers } = require('ethers')
import { WalletDao } from "../dao/wallet-dao";
import service from "../decorators/service";
import { UiService } from "./ui-service";

@service()
class WalletService {

  public skipBinding = true

  public provider: any
  public wallet: any


  constructor(
    private walletDao: WalletDao,
    private uiService: UiService
  ) {
  }

  getClassName(): string {
    return "WalletService"
  }

  async init() {
    //TODO: Probably break this out into different implementation depending on the scenario. 
    //Then just inject the right scenario at startup. Maybe built to a profile or whatever inversify calls them. 

    if (window['ethereum']) {

      // Request account access
      await window['ethereum'].enable()

      //@ts-ignore
      window.web3Provider = window.ethereum

      //@ts-ignore
      web3 = new Web3(window.web3Provider)

      //@ts-ignore
      this.provider = new providers.Web3Provider(web3.currentProvider)
      this.wallet = this.provider.getSigner()

    } else {

      //We need our own provider.
      this.provider = ethers.getDefaultProvider("homestead")

    }

  }


  async createWallet(password): Promise<string> {

    this.uiService.showSpinner("Creating wallet. Please wait.")

    //Generate random words
    let wallet = await Wallet.createRandom()

    this.uiService.showSpinner("Encrypting wallet...")

    //Encrypt with entered password
    let encryptedJsonWallet = await wallet.encrypt(password)

    await this.walletDao.saveWallet(encryptedJsonWallet)

    await this.connectWallet(wallet)

    this.uiService.hideSpinner()

    return this.wallet.mnemonic.phrase.split(" ")


  }

  async connectWallet(wallet) {
    this.wallet = wallet
    this.wallet = await this.wallet.connect(this.provider)
  }



  async unlockWallet(password: string) {

    let savedWallet = await this.walletDao.loadWallet()

    if (!savedWallet) throw new Error("No wallet to unlock")

    let wallet = await Wallet.fromEncryptedJson(savedWallet, password)

    return this.connectWallet(wallet)

  }

  async restoreWallet(recoverySeed: string, password: string) {

    let wallet = await Wallet.fromMnemonic(recoverySeed)
    if (!wallet) return

    //Encrypt with entered password
    let encryptedJsonWallet = await wallet.encrypt(password)
    await this.walletDao.saveWallet(encryptedJsonWallet)


    await this.connectWallet(wallet)

    return wallet

  }

  getWallet() {
    return this.walletDao.loadWallet()
  }

  logout() {
    this.wallet = undefined
  }



}



export {
  WalletService
}