import { StoreDefinition } from "./store-definition";

interface Schema {
    cid?: string
    owner?: string

    addresses?: any
    storeDefinitions?:StoreDefinition[]
}


interface LoadedSchema {
    schema?: Schema
    stores?: any
}

export {
    Schema, LoadedSchema
}
