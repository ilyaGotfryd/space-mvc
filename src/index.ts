import "reflect-metadata"


import { ModelView } from "./util/model-view"
import { PromiseView } from "./util/promise-view"
import { QueueService } from "./service/queue_service"
import { SpaceMVC } from "./space-mvc";
import { UiService } from "./service/ui-service";
import { PagingService, PagingViewModel } from "./service/paging-service";
import { WalletService } from "./service/wallet-service";
import { SchemaService } from "./service/schema-service";
import { StoreDefinition } from "./dto/store-definition";
import { Schema } from "./dto/schema";



import { inject, Container } from "inversify";
import { IpfsService } from "./service/ipfs-service";
import { InvalidWalletException } from "./service/exception/invalid-wallet-exception";
import { WalletController } from "./controller/wallet-controller";
import { RoutingService } from "./service/routing-service";
import controller from "./decorators/controller";
import service from "./decorators/service";
import { routeMap } from "./decorators/route-map";
import dao from "./decorators/dao";
import { SpaceComponent } from "./util/space-component";
import { OrbitService } from "./service/orbit-service";
import { initTestContainer } from "../test/test-inversify.config";
import { Initializable } from "./interface/initializable";

export {
    QueueService,
    PagingService,
    PagingViewModel,
    UiService,
    WalletService,
    InvalidWalletException,
    SchemaService,
    IpfsService,
    OrbitService,
    RoutingService,
    WalletController,
    StoreDefinition,
    ModelView,
    PromiseView,
    Schema,
    Container,
    SpaceComponent,
    inject,
    service,
    controller,
    dao,
    Initializable,
    routeMap,
    initTestContainer
}




export default SpaceMVC