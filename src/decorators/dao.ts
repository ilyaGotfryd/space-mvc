import { injectable } from "inversify"

function dao(args: any = {}): (cls: any) => any {

    const injectableDecorator = injectable()

    return function (controllerType: any) {
        injectableDecorator(controllerType)
    }
}

export default dao