import { SpaceMVC } from "../space-mvc";

function routeMap(value: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {

        if (!globalThis.mappedRoutes) globalThis.mappedRoutes = []

        globalThis.mappedRoutes.push({
            path: value,
            controllerClass: target.constructor,
            action: propertyKey
        })

    }
}

export {
    routeMap
}