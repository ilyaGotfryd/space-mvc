import { Component } from 'framework7';
import { SpaceMVC } from '../space-mvc';
import { Router } from 'framework7/modules/router/router';
import { RoutingService } from '../service/routing-service';
import { UiService } from '../service/ui-service';

class SpaceComponent extends Component {

    public getApp() {
        return SpaceMVC.app()
    }

    private getRoutingService(): RoutingService {
        return SpaceMVC.getContainer().get<RoutingService>(RoutingService)
    }


    navigate(navigateParams:Router.NavigateParameters, routeOptions?: Router.RouteOptions, viewName?:string) {
        this.getRoutingService().navigate(navigateParams, routeOptions, viewName)
    }

    submitForm(e: Event, formId: string) {

        e.preventDefault()


        let data = SpaceMVC.app().form.convertToData(formId)

        //@ts-ignore
        let form = this.$$(formId)[0]

        //@ts-ignore
        if (!form.checkValidity()) return
        
        //@ts-ignore
        let action: string = this.$$(form).attr('action')

        this.getRoutingService().navigate({
            path: action
        },
            {
                ignoreCache: true,
                context: {
                    data: data,
                    component: this
                }
            }
        )


    }


}


export {
    SpaceComponent
}