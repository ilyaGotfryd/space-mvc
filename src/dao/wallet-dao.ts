import { injectable } from "inversify"

const levelup = require('levelup')
const leveljs = require('level-js')


@injectable()
class WalletDao {

  public skipBinding = true

  db: any

  constructor() {
    this.db = levelup(leveljs('spacemvc-wallet'))
  }


  async saveWallet(wallet) {
    return this.db.put('wallet', Buffer.from(JSON.stringify(wallet)))
  }

  async loadWallet() {

    try {
      const value = await this.db.get('wallet')

      if (value) {
        return JSON.parse(value.toString())
      }

    } catch (ex) {
      // console.log(ex)
    }

  }




}

export {
  WalletDao
}