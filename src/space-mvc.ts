import { UiService } from "./service/ui-service";
import { PagingService } from "./service/paging-service";
import { IdentityService } from "./service/identity-service";
import { WalletService } from "./service/wallet-service";
import { EventEmitter } from "events";
import { SchemaService } from "./service/schema-service";
import { StoreDefinition } from "./dto/store-definition";
import { Container, interfaces } from "inversify";

import { OrbitService } from "./service/orbit-service";
import { IpfsService } from "./service/ipfs-service";
import { InvalidWalletException } from "./service/exception/invalid-wallet-exception";
import { buildContainer } from "./inversify.config";
import { RoutingService } from "./service/routing-service";
import { QueueService } from "./service/queue_service";
import { BindingService } from "./service/binding-service";



let appName: string
let eventEmitter: EventEmitter = new EventEmitter()


export namespace SpaceMVC {

    export function getEventEmitter() {
        return eventEmitter
    }

    export function getContainer() : Container {
        return globalThis.container
    }

    export function name() : string {
        return appName
    }

    export function app(): any {
        return getContainer().get("framework7")
    }

    export function getWallet() {
        return _walletService().wallet
    }


    export async function init(container:Container, f7Config, ownerAddress:string, storeDefinitions: StoreDefinition[]) {

        container.bind("ownerAddress").toConstantValue(ownerAddress)
        container.bind('storeDefinitions').toConstantValue(storeDefinitions)
        
        await SpaceMVC.initWallet(container, f7Config)
    
        if (SpaceMVC.getWallet()) {

            SpaceMVC.initMainView()
        
            await SpaceMVC.initFinish(ownerAddress, storeDefinitions)    
        
        } else {

            SpaceMVC.initMainView()
            _routingService().navigate({
                path: "/wallet"
            })
        }
    }


    export async function initWallet(container: Container, f7Config) {

        console.log("Initializing SpaceMVC wallet")

        appName = f7Config.name


        globalThis.container = buildContainer(container, f7Config) 
        

        //Include default routing
        let routes = _routingService().buildRoutesForContainer(SpaceMVC.app().routes, globalThis.container)
        SpaceMVC.app().routes = routes


        //Get IPFS options and initialize
        await _ipfsService().init()

        //Initialize wallet
        await _walletService().init()

    }


    export async function initSchema(ownerAddress: string, storeDefinitions: StoreDefinition[]) {

        if (!SpaceMVC.getWallet()) {
            throw new InvalidWalletException("No wallet found. Unable to initialize.")
        }

        let walletAddress = await SpaceMVC.getWallet().getAddress()

        console.log(`Initializing ${appName} for wallet: ${walletAddress}`)

        _uiService().showSpinner('Initializing wallet')

        await _orbitService().init(SpaceMVC.getWallet())

        let schema = await _schemaService().create(await SpaceMVC.getWallet().getAddress(), storeDefinitions)
        await _schemaService().load(schema)


        //Init all the Initializable bindings in the container 
        await _bindingService().initBindings(getContainer())

        _uiService().hideSpinner()
    }



    export function initMainView() {

        //Create main view
        SpaceMVC.app().views.create('.view-main', {
            pushState: true,
            loadInitialPage: false
        })
    }


    export async function initFinish(ownerAddress: string, storeDefinitions: StoreDefinition[]) {

        await SpaceMVC.initSchema(ownerAddress, storeDefinitions)    
        
        _routingService().navigate({
            path: "/"
        })

        eventEmitter.emit('spacemvc:initFinish')

    }


    function _ipfsService(): IpfsService {
        return getContainer().get<IpfsService>(IpfsService)
    }

    function _orbitService(): OrbitService {
        return getContainer().get<OrbitService>(OrbitService)
    }

    function _queueService(): QueueService {
        return getContainer().get<QueueService>(QueueService)
    }

    function _pagingService(): PagingService {
        return getContainer().get<PagingService>(PagingService)
    }

    function _uiService(): UiService {
        return getContainer().get<UiService>(UiService)
    }

    function _identityService(): IdentityService {
        return getContainer().get<IdentityService>(IdentityService)
    }

    function _walletService(): WalletService {
        return getContainer().get<WalletService>(WalletService)
    }

    function _schemaService(): SchemaService {
        return getContainer().get<SchemaService>(SchemaService)
    }

    function _routingService(): RoutingService {
        return getContainer().get<RoutingService>(RoutingService)
    }

    function _bindingService(): BindingService {
        return getContainer().get<BindingService>(BindingService)
    }



}



