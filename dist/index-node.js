(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(global, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/components/connect/index.f7.html":
/*!**********************************************!*\
  !*** ./src/components/connect/index.f7.html ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! template7 */ "template7");
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(template7__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/space-component */ "./src/util/space-component.ts");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var Template7Helpers = template7__WEBPACK_IMPORTED_MODULE_0___default.a.helpers;
template7__WEBPACK_IMPORTED_MODULE_0___default.a.registerPartial('bars', '<a href="#" class="link icon-only panel-open small-only" data-panel=".panel-left"><i class="icon f7-icons">bars</i></a>'); // first we import super class

 // we need to export extended class

var F7PageComponent = /*#__PURE__*/function (_SpaceComponent) {
  _inherits(F7PageComponent, _SpaceComponent);

  var _super = _createSuper(F7PageComponent);

  function F7PageComponent() {
    _classCallCheck(this, F7PageComponent);

    return _super.apply(this, arguments);
  }

  _createClass(F7PageComponent, [{
    key: "render",
    value: function render() {
      return function (ctx_1, data_1, root) {
        function isArray(arr) {
          return Array.isArray(arr);
        }

        function isFunction(func) {
          return typeof func === 'function';
        }

        function c(val, ctx) {
          if (typeof val !== "undefined" && val !== null) {
            if (isFunction(val)) {
              return val.call(ctx);
            } else return val;
          } else return "";
        }

        root = root || ctx_1 || {};
        var r = '';
        r += '<div class=page data-name=connect><div class=navbar><div class=navbar-bg></div><div class=navbar-inner><div class=left>';
        r += Template7Helpers._partial.call(ctx_1, "bars", {
          hash: {},
          data: data_1 || {},
          fn: function empty() {
            return '';
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += '</div><div class=title>Connect</div></div></div><div class=page-content><div class=row><div class="col-100 tablet-50 center">';
        r += Template7Helpers["with"].call(ctx_1, ctx_1.connect, {
          hash: {},
          data: data_1 || {},
          fn: function fn(ctx_2, data_2) {
            var r = '';
            r += '<div class=block-title>IPFS Peers <span class="badge peers-badge color-blue">';
            r += c(ctx_2.peerCount, ctx_2);
            r += '</span></div><div class="block list media-list peer-list"><ul>';
            r += Template7Helpers.each.call(ctx_2, ctx_2.peers, {
              hash: {},
              data: data_2 || {},
              fn: function fn(ctx_3, data_3) {
                var r = '';
                r += '<li><div class=item-content><div class=item-inner><div class="item-subtitle no-wrap">';
                r += c(ctx_3, ctx_3);
                r += '</div></div></div></li>';
                return r;
              },
              inverse: function empty() {
                return '';
              },
              root: root,
              parents: [ctx_1]
            });
            r += '</ul></div><div class=block-title>IPFS Info</div><div class="block list media-list"><ul>';
            r += Template7Helpers.each.call(ctx_2, ctx_2.ipfsInfo.addresses, {
              hash: {},
              data: data_2 || {},
              fn: function fn(ctx_3, data_3) {
                var r = '';
                r += '<li><div class=item-content><div class=item-inner><div class="item-subtitle no-wrap">';
                r += c(ctx_3, ctx_3);
                r += '</div></div></div></li>';
                return r;
              },
              inverse: function empty() {
                return '';
              },
              root: root,
              parents: [ctx_1]
            });
            r += '<li><div class=item-content><div class=item-inner><div class="item-title no-wrap"><div class=item-header>Agent Version</div>';
            r += c(ctx_2.ipfsInfo.agentVersion, ctx_2);
            r += '</div></div></div></li><li><div class=item-content><div class=item-inner><div class="item-title no-wrap"><div class=item-header>ID</div>';
            r += c(ctx_2.ipfsInfo.id, ctx_2);
            r += '</div></div></div></li></ul></div><div class=block-title>Add IPFS Peer</div><form class="block list" id=add-peer-form action=/connect/addPeer><ul><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">Peer Address</div><div class=item-input-wrap><input id=peerAddressInput type=text name=peerAddress placeholder="Enter peer address" required validate><div class=row><button class="button button-fill col-100 large-40" value="Add Peer" @click=addPeerClick>Add Peer</button></div></div></div></div></li></ul></form><div class=block-title>Monitored Feeds</div><div class="block list media-list peer-list"><ul>';
            r += Template7Helpers.each.call(ctx_2, ctx_2.subscribed, {
              hash: {},
              data: data_2 || {},
              fn: function fn(ctx_3, data_3) {
                var r = '';
                r += '<li><div class=item-content><div class=item-inner><div class="item-subtitle no-wrap">';
                r += c(ctx_3, ctx_3);
                r += '</div></div></div></li>';
                return r;
              },
              inverse: function empty() {
                return '';
              },
              root: root,
              parents: [ctx_1]
            });
            r += '</ul></div>';
            return r;
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += ' ';
        r += Template7Helpers["with"].call(ctx_1, ctx_1.schema, {
          hash: {},
          data: data_1 || {},
          fn: function fn(ctx_2, data_2) {
            var r = '';
            r += '<div class=block-title>Schema</div><div class=block><div class="list media-list"><ul><li><div class=item-content><div class=item-inner><div class=item-title-row><div class=item-title>Global Schema CID</div></div><div class=item-text>';
            r += c(ctx_2.globalSchema.cid, ctx_2);
            r += '</div></div></div></li><li><div class=item-content><div class=item-inner><div class=item-title-row><div class=item-title>Global Schema Owner</div></div><div class=item-text>';
            r += c(ctx_2.globalSchema.owner, ctx_2);
            r += '</div></div></div></li>';
            r += Template7Helpers.each.call(ctx_2, ctx_2.stores, {
              hash: {},
              data: data_2 || {},
              fn: function fn(ctx_3, data_3) {
                var r = '';
                r += '<li><div class=item-content><div class=item-inner><div class=item-title-row><div class=item-title>';
                r += c(ctx_3.name, ctx_3);
                r += '</div><div class=item-after>Loaded: ';
                r += c(ctx_3.loaded, ctx_3);
                r += '</div></div><div class=item-text>';
                r += c(ctx_3.address, ctx_3);
                r += '</div></div></div></li>';
                return r;
              },
              inverse: function empty() {
                return '';
              },
              root: root,
              parents: [ctx_1]
            });
            r += '</ul></div></div>';
            return r;
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += '</div></div></div></div>';
        return r;
      }(this);
    } // hooks

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {
      // Bind event handlers to component context
      this.addPeerClick = this.addPeerClick.bind(this);
    }
  }, {
    key: "mounted",
    value: function mounted() {}
  }, {
    key: "beforeDestroy",
    value: function beforeDestroy() {} // methods

  }, {
    key: "addPeerClick",
    value: function addPeerClick(e) {
      this.submitForm(e, "#add-peer-form");
    }
  }]);

  return F7PageComponent;
}(_util_space_component__WEBPACK_IMPORTED_MODULE_1__["SpaceComponent"]);

F7PageComponent.id = 'a9788e5f92';
F7PageComponent.styleScoped = false;
/* harmony default export */ __webpack_exports__["default"] = (F7PageComponent);

/***/ }),

/***/ "./src/components/wallet/create-wallet-success.f7.html":
/*!*************************************************************!*\
  !*** ./src/components/wallet/create-wallet-success.f7.html ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! template7 */ "template7");
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(template7__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/space-component */ "./src/util/space-component.ts");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var Template7Helpers = template7__WEBPACK_IMPORTED_MODULE_0___default.a.helpers; // first we import super class

 // we need to export extended class

var F7PageComponent = /*#__PURE__*/function (_SpaceComponent) {
  _inherits(F7PageComponent, _SpaceComponent);

  var _super = _createSuper(F7PageComponent);

  function F7PageComponent() {
    _classCallCheck(this, F7PageComponent);

    return _super.apply(this, arguments);
  }

  _createClass(F7PageComponent, [{
    key: "render",
    value: function render() {
      return function (ctx_1, data_1, root) {
        function isArray(arr) {
          return Array.isArray(arr);
        }

        function isFunction(func) {
          return typeof func === 'function';
        }

        function c(val, ctx) {
          if (typeof val !== "undefined" && val !== null) {
            if (isFunction(val)) {
              return val.call(ctx);
            } else return val;
          } else return "";
        }

        root = root || ctx_1 || {};
        var r = '';
        r += '<div class=page data-name=create-wallet-success><div class=page-content><div class=row><div class="col-100 large-50 center"><div class=block-title><i class="icon f7-icons">lock_shield_fill</i> This is your key phrase.</div><div class="block block-strong inset"><div class=row>';
        r += Template7Helpers.each.call(ctx_1, ctx_1.mnemonic, {
          hash: {},
          data: data_1 || {},
          fn: function fn(ctx_2, data_2) {
            var r = '';
            r += '<div class="col-33 mnemonic"><div class=number>';
            r += Template7Helpers.js.call(ctx_2, "@index + 1", {
              hash: {},
              data: data_2 || {},
              fn: function empty() {
                return '';
              },
              inverse: function empty() {
                return '';
              },
              root: root,
              parents: [ctx_1]
            });
            r += '.</div><div class=word>';
            r += c(ctx_2, ctx_2);
            r += '</div></div>';
            return r;
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += '</div></div><div class="block block-strong inset">';
        r += Template7Helpers.each.call(ctx_1, ctx_1.mnemonic, {
          hash: {},
          data: data_1 || {},
          fn: function fn(ctx_2, data_2) {
            var r = '';
            r += ' ';
            r += c(ctx_2, ctx_2);
            r += ' ';
            return r;
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += '</div><div class=block-title>Important</div><div class="block block-strong inset"><p>This 12 word secret phrase is how your account is accessed. It\'s important to keep this phrase secret.</p><p><strong>Write this phrase on a piece of paper</strong> and keep it in a secure location.</p><p><strong>WARNING:</strong> Don\'t ever tell anyone your secret phrase. Anyone with this phrase can use it to access your account and make permanent changes. There is no way to recover lost key phrases.</p></div><div class="block row"><a class="button button-fill col-100 large-40" href=/createWalletDone>Got it</a></div></div></div></div></div>';
        return r;
      }(this);
    } // hooks

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {}
  }, {
    key: "mounted",
    value: function mounted() {}
  }, {
    key: "beforeDestroy",
    value: function beforeDestroy() {}
  }]);

  return F7PageComponent;
}(_util_space_component__WEBPACK_IMPORTED_MODULE_1__["SpaceComponent"]);

F7PageComponent.id = '295040aacf';
F7PageComponent.styleScoped = false;
/* harmony default export */ __webpack_exports__["default"] = (F7PageComponent);

/***/ }),

/***/ "./src/components/wallet/create-wallet.f7.html":
/*!*****************************************************!*\
  !*** ./src/components/wallet/create-wallet.f7.html ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../util/space-component */ "./src/util/space-component.ts");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// first we import super class
 // we need to export extended class

var F7PageComponent = /*#__PURE__*/function (_SpaceComponent) {
  _inherits(F7PageComponent, _SpaceComponent);

  var _super = _createSuper(F7PageComponent);

  function F7PageComponent() {
    _classCallCheck(this, F7PageComponent);

    return _super.apply(this, arguments);
  }

  _createClass(F7PageComponent, [{
    key: "render",
    value: function render() {
      return function (ctx_1, data_1, root) {
        function isArray(arr) {
          return Array.isArray(arr);
        }

        function isFunction(func) {
          return typeof func === 'function';
        }

        function c(val, ctx) {
          if (typeof val !== "undefined" && val !== null) {
            if (isFunction(val)) {
              return val.call(ctx);
            } else return val;
          } else return "";
        }

        root = root || ctx_1 || {};
        var r = '';
        r += '<div class=page data-name=create-wallet><div class=page-content><div class=row><div class="col-100 large-50 center"><form id=create-wallet-form action=/createWallet><div class=block-title>Create Password</div><div class=block>This password will used to encrypt your wallet on this device. Choose a secure password.</div><div class="block list"><ul><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">New Password (min 8 chars)</div><div class=item-input-wrap><input type=password id=newPassword name=newPassword placeholder="Enter new password" minlength=8 required validate></div></div></div></li><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">Confirm Password</div><div class=item-input-wrap><input type=password id=confirmPassword name=confirmPassword placeholder="Confirm Password" minlength=8 required validate></div></div></div></li></ul></div><div class="block row"><button class="button button-fill col-100 large-40" @click=createClick>Create Wallet</button></div></form></div></div></div></div>';
        return r;
      }(this);
    } // hooks

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {
      // Bind event handlers to component context
      this.createClick = this.createClick.bind(this);
    }
  }, {
    key: "mounted",
    value: function mounted() {
      var self = this;

      function validateMatchingPasswords(e) {
        var password = self.$$('#newPassword').val();
        var confirmPassword = self.$$('#confirmPassword').val();
        self.$$('#newPassword')[0].setCustomValidity('');

        if (password != confirmPassword) {
          self.$$('#newPassword')[0].setCustomValidity("Passwords must match");
        }
      }

      this.$$('#newPassword').on('change blur', validateMatchingPasswords);
      this.$$('#confirmPassword').on('change blur', validateMatchingPasswords);
    }
  }, {
    key: "beforeDestroy",
    value: function beforeDestroy() {} // methods

  }, {
    key: "createClick",
    value: function createClick(e) {
      this.submitForm(e, "#create-wallet-form");
    }
  }]);

  return F7PageComponent;
}(_util_space_component__WEBPACK_IMPORTED_MODULE_0__["SpaceComponent"]);

F7PageComponent.id = '716d8610eb';
F7PageComponent.styleScoped = false;
/* harmony default export */ __webpack_exports__["default"] = (F7PageComponent);

/***/ }),

/***/ "./src/components/wallet/enter-recovery.f7.html":
/*!******************************************************!*\
  !*** ./src/components/wallet/enter-recovery.f7.html ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../util/space-component */ "./src/util/space-component.ts");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

// first we import super class
 // we need to export extended class

var F7PageComponent = /*#__PURE__*/function (_SpaceComponent) {
  _inherits(F7PageComponent, _SpaceComponent);

  var _super = _createSuper(F7PageComponent);

  function F7PageComponent() {
    _classCallCheck(this, F7PageComponent);

    return _super.apply(this, arguments);
  }

  _createClass(F7PageComponent, [{
    key: "render",
    value: function render() {
      return function (ctx_1, data_1, root) {
        function isArray(arr) {
          return Array.isArray(arr);
        }

        function isFunction(func) {
          return typeof func === 'function';
        }

        function c(val, ctx) {
          if (typeof val !== "undefined" && val !== null) {
            if (isFunction(val)) {
              return val.call(ctx);
            } else return val;
          } else return "";
        }

        root = root || ctx_1 || {};
        var r = '';
        r += '<div class=page data-name=enter-recovery><div class=page-content><div class=row><div class="col-100 large-50 center"><div class=block-title>Restore Your Account</div><div class=block><p>Enter your secret twelve word phrase to restore your account.</p><form id=restore-account-form action=/restoreAccount><div class=list><ul><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">Recovery Seed (12 words)</div><div class=item-input-wrap><textarea name=recoverySeed required validate></textarea></div></div></div></li><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">New Password (min 8 chars)</div><div class=item-input-wrap><input type=password id=newPassword name=newPassword placeholder="Enter new password" minlength=8 required validate></div></div></div></li><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">Confirm Password</div><div class=item-input-wrap><input type=password id=confirmPassword name=confirmPassword minlength=8 placeholder="Confirm Password" required validate></div></div></div></li></ul></div><div class=row><button class="button button-fill col-40" @click=restoreClick>Restore Wallet</button></div></form></div></div></div></div></div>';
        return r;
      }(this);
    } // hooks

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {
      this.restoreClick = this.restoreClick.bind(this);
    }
  }, {
    key: "mounted",
    value: function mounted() {
      var self = this;

      function validateMatchingPasswords(e) {
        var password = self.$$('#newPassword').val();
        var confirmPassword = self.$$('#confirmPassword').val();
        self.$$('#newPassword')[0].setCustomValidity('');

        if (password != confirmPassword) {
          self.$$('#newPassword')[0].setCustomValidity("Passwords must match");
        }
      }

      this.$$('#newPassword').on('change blur', validateMatchingPasswords);
      this.$$('#confirmPassword').on('change blur', validateMatchingPasswords);
    } // methods

  }, {
    key: "restoreClick",
    value: function restoreClick(e) {
      this.submitForm(e, "#restore-account-form");
    }
  }]);

  return F7PageComponent;
}(_util_space_component__WEBPACK_IMPORTED_MODULE_0__["SpaceComponent"]);

F7PageComponent.id = '327f33c906';
F7PageComponent.styleScoped = false;
/* harmony default export */ __webpack_exports__["default"] = (F7PageComponent);

/***/ }),

/***/ "./src/components/wallet/landing.f7.html":
/*!***********************************************!*\
  !*** ./src/components/wallet/landing.f7.html ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! template7 */ "template7");
/* harmony import */ var template7__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(template7__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../util/space-component */ "./src/util/space-component.ts");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var Template7Helpers = template7__WEBPACK_IMPORTED_MODULE_0___default.a.helpers; // first we import super class

 // we need to export extended class

var F7PageComponent = /*#__PURE__*/function (_SpaceComponent) {
  _inherits(F7PageComponent, _SpaceComponent);

  var _super = _createSuper(F7PageComponent);

  function F7PageComponent() {
    _classCallCheck(this, F7PageComponent);

    return _super.apply(this, arguments);
  }

  _createClass(F7PageComponent, [{
    key: "render",
    value: function render() {
      return function (ctx_1, data_1, root) {
        function isArray(arr) {
          return Array.isArray(arr);
        }

        function isFunction(func) {
          return typeof func === 'function';
        }

        function c(val, ctx) {
          if (typeof val !== "undefined" && val !== null) {
            if (isFunction(val)) {
              return val.call(ctx);
            } else return val;
          } else return "";
        }

        root = root || ctx_1 || {};
        var r = '';
        r += '<div class=page data-name=wallet-landing><div class=page-content><div class=row><div class="col-100 large-50 center"><div class=block-title>New to ';
        r += c(ctx_1.appName, ctx_1);
        r += '?</div><div class="block list media-list"><ul><li><a href=/showCreateWallet><div class=item-content><div class=item-inner><div class=item-title-row><div class=item-title>I\'m new, let\'s get set up!</div></div><div class=item-text>Create a new wallet with a new seed phrase.</div></div></div></a></li><li><a href=/enterRecovery><div class=item-content><div class=item-inner><div class=item-title-row><div class=item-title>I have a recovery seed phrase.</div></div><div class=item-text>Import your existing wallet using the 12 word recovery seed phrase.</div></div></div></a></li></ul></div>';
        r += Template7Helpers["if"].call(ctx_1, ctx_1.showUnlock, {
          hash: {},
          data: data_1 || {},
          fn: function fn(ctx_2, data_2) {
            var r = '';
            r += '<form id=unlock-wallet-form action=/unlockWallet><div class=block-title>Unlock Wallet</div><div class="block list"><ul><li><div class="item-content item-input"><div class=item-inner><div class="item-title item-label">Password</div><div class=item-input-wrap><input type=password id=password name=password placeholder="Enter password for stored wallet" required validate></div></div></div></li></ul></div><div class="block row"><button class="button button-fill col-100 large-40" @click=unlockClick>Unlock</button></div></form>';
            return r;
          },
          inverse: function empty() {
            return '';
          },
          root: root,
          parents: [ctx_1]
        });
        r += '</div></div></div></div>';
        return r;
      }(this);
    } // hooks

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {
      // Bind event handlers to component context
      this.unlockClick = this.unlockClick.bind(this);
    }
  }, {
    key: "mounted",
    value: function mounted() {}
  }, {
    key: "beforeDestroy",
    value: function beforeDestroy() {} // methods

  }, {
    key: "unlockClick",
    value: function unlockClick(e) {
      this.submitForm(e, "#unlock-wallet-form");
    }
  }]);

  return F7PageComponent;
}(_util_space_component__WEBPACK_IMPORTED_MODULE_1__["SpaceComponent"]);

F7PageComponent.id = 'c79f72e4fd';
F7PageComponent.styleScoped = false;
/* harmony default export */ __webpack_exports__["default"] = (F7PageComponent);

/***/ }),

/***/ "./src/controller/connect-controller.ts":
/*!**********************************************!*\
  !*** ./src/controller/connect-controller.ts ***!
  \**********************************************/
/*! exports provided: ConnectController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectController", function() { return ConnectController; });
/* harmony import */ var _service_ui_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../service/ui-service */ "./src/service/ui-service.ts");
/* harmony import */ var _service_connect_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/connect-service */ "./src/service/connect-service.ts");
/* harmony import */ var _decorators_route_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../decorators/route-map */ "./src/decorators/route-map.ts");
/* harmony import */ var _util_model_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../util/model-view */ "./src/util/model-view.ts");
/* harmony import */ var _components_connect_index_f7_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/connect/index.f7.html */ "./src/components/connect/index.f7.html");
/* harmony import */ var _decorators_controller__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../decorators/controller */ "./src/decorators/controller.ts");
/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/routing-service */ "./src/service/routing-service.ts");
/* harmony import */ var _service_schema_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/schema-service */ "./src/service/schema-service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








let ConnectController = class ConnectController {
    constructor(connectService, uiService, routingService, schemaService) {
        this.connectService = connectService;
        this.uiService = uiService;
        this.routingService = routingService;
        this.schemaService = schemaService;
    }
    async index() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async () => {
            let vm = {
                connect: await this.connectService.getConnectInfo(),
                schema: await this.schemaService.getSchemaInfo()
            };
            return vm;
        }, _components_connect_index_f7_html__WEBPACK_IMPORTED_MODULE_4__["default"]);
    }
    async addPeer() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async (data) => {
            try {
                await this.connectService.addPeer(data.peerAddress);
            }
            catch (ex) {
                this.uiService.showPopup(ex);
            }
            this.routingService.navigate({
                path: "/connect"
            });
        }, _components_connect_index_f7_html__WEBPACK_IMPORTED_MODULE_4__["default"]);
    }
};
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_2__["routeMap"])("/connect"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ConnectController.prototype, "index", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_2__["routeMap"])("/connect/addPeer"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ConnectController.prototype, "addPeer", null);
ConnectController = __decorate([
    Object(_decorators_controller__WEBPACK_IMPORTED_MODULE_5__["default"])(),
    __metadata("design:paramtypes", [_service_connect_service__WEBPACK_IMPORTED_MODULE_1__["ConnectService"],
        _service_ui_service__WEBPACK_IMPORTED_MODULE_0__["UiService"],
        _service_routing_service__WEBPACK_IMPORTED_MODULE_6__["RoutingService"],
        _service_schema_service__WEBPACK_IMPORTED_MODULE_7__["SchemaService"]])
], ConnectController);



/***/ }),

/***/ "./src/controller/wallet-controller.ts":
/*!*********************************************!*\
  !*** ./src/controller/wallet-controller.ts ***!
  \*********************************************/
/*! exports provided: WalletController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletController", function() { return WalletController; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _service_wallet_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/wallet-service */ "./src/service/wallet-service.ts");
/* harmony import */ var _service_ui_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/ui-service */ "./src/service/ui-service.ts");
/* harmony import */ var _util_model_view__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../util/model-view */ "./src/util/model-view.ts");
/* harmony import */ var _components_wallet_landing_f7_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/wallet/landing.f7.html */ "./src/components/wallet/landing.f7.html");
/* harmony import */ var _components_wallet_enter_recovery_f7_html__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/wallet/enter-recovery.f7.html */ "./src/components/wallet/enter-recovery.f7.html");
/* harmony import */ var _components_wallet_create_wallet_f7_html__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/wallet/create-wallet.f7.html */ "./src/components/wallet/create-wallet.f7.html");
/* harmony import */ var _components_wallet_create_wallet_success_f7_html__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/wallet/create-wallet-success.f7.html */ "./src/components/wallet/create-wallet-success.f7.html");
/* harmony import */ var _decorators_route_map__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../decorators/route-map */ "./src/decorators/route-map.ts");
/* harmony import */ var _decorators_controller__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../decorators/controller */ "./src/decorators/controller.ts");
/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/routing-service */ "./src/service/routing-service.ts");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! .. */ "./src/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};












let WalletController = class WalletController {
    constructor(walletService, uiService, routingService, ownerAddress, storeDefinitions) {
        this.walletService = walletService;
        this.uiService = uiService;
        this.routingService = routingService;
        this.ownerAddress = ownerAddress;
        this.storeDefinitions = storeDefinitions;
    }
    async _initApp() {
        this.uiService.showSpinner();
        await ___WEBPACK_IMPORTED_MODULE_11__["default"].initFinish(this.ownerAddress, this.storeDefinitions);
        this.uiService.hideSpinner();
    }
    async showLanding() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async () => {
            let existingWallet = await this.walletService.getWallet();
            return {
                showUnlock: (existingWallet),
                appName: ___WEBPACK_IMPORTED_MODULE_11__["default"].name()
            };
        }, _components_wallet_landing_f7_html__WEBPACK_IMPORTED_MODULE_4__["default"]);
    }
    async showCreateWallet() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async () => {
        }, _components_wallet_create_wallet_f7_html__WEBPACK_IMPORTED_MODULE_6__["default"]);
    }
    async createWallet() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async (formData) => {
            //Create new wallet
            let mnemonic = await this.walletService.createWallet(formData.newPassword);
            this.routingService.navigate({
                path: "/createWalletSuccess"
            }, {
                context: {
                    mnemonic: mnemonic
                }
            });
        });
    }
    async createWalletSuccess() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async (data) => {
            return data;
        }, _components_wallet_create_wallet_success_f7_html__WEBPACK_IMPORTED_MODULE_7__["default"]);
    }
    async createWalletDone() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async () => {
            try {
                await this._initApp();
            }
            catch (ex) {
                console.log(ex);
                this.uiService.hideSpinner();
                this.uiService.showPopup("Unable to initialize app");
            }
        });
    }
    async unlockWallet() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async (formData) => {
            this.uiService.showSpinner('Unlocking wallet...');
            try {
                await this.walletService.unlockWallet(formData.password);
                await this._initApp();
            }
            catch (ex) {
                console.log(ex);
                this.uiService.hideSpinner();
                this.uiService.showPopup("Incorrect password please try again.");
            }
        });
    }
    async showEnterRecovery() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async () => {
            return {};
        }, _components_wallet_enter_recovery_f7_html__WEBPACK_IMPORTED_MODULE_5__["default"]);
    }
    async restoreButtonClick() {
        return new _util_model_view__WEBPACK_IMPORTED_MODULE_3__["ModelView"](async (formData) => {
            this.uiService.showSpinner('Restoring wallet...');
            let wallet = await this.walletService.restoreWallet(formData.recoverySeed, formData.newPassword);
            if (wallet) {
                await this._initApp();
            }
            else {
                this.uiService.showPopup("Error restoring wallet. Please try again.");
                this.uiService.hideSpinner();
            }
        });
    }
};
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/wallet"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "showLanding", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/showCreateWallet"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "showCreateWallet", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/createWallet"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "createWallet", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/createWalletSuccess"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "createWalletSuccess", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/createWalletDone"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "createWalletDone", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/unlockWallet"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "unlockWallet", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/enterRecovery"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "showEnterRecovery", null);
__decorate([
    Object(_decorators_route_map__WEBPACK_IMPORTED_MODULE_8__["routeMap"])("/restoreAccount"),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WalletController.prototype, "restoreButtonClick", null);
WalletController = __decorate([
    Object(_decorators_controller__WEBPACK_IMPORTED_MODULE_9__["default"])(),
    __param(3, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("ownerAddress")),
    __param(4, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("storeDefinitions")),
    __metadata("design:paramtypes", [_service_wallet_service__WEBPACK_IMPORTED_MODULE_1__["WalletService"],
        _service_ui_service__WEBPACK_IMPORTED_MODULE_2__["UiService"],
        _service_routing_service__WEBPACK_IMPORTED_MODULE_10__["RoutingService"], String, Array])
], WalletController);



/***/ }),

/***/ "./src/dao/wallet-dao.ts":
/*!*******************************!*\
  !*** ./src/dao/wallet-dao.ts ***!
  \*******************************/
/*! exports provided: WalletDao */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletDao", function() { return WalletDao; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

const levelup = __webpack_require__(/*! levelup */ "levelup");
const leveljs = __webpack_require__(/*! level-js */ "level-js");
let WalletDao = class WalletDao {
    constructor() {
        this.skipBinding = true;
        this.db = levelup(leveljs('spacemvc-wallet'));
    }
    async saveWallet(wallet) {
        return this.db.put('wallet', Buffer.from(JSON.stringify(wallet)));
    }
    async loadWallet() {
        try {
            const value = await this.db.get('wallet');
            if (value) {
                return JSON.parse(value.toString());
            }
        }
        catch (ex) {
            // console.log(ex)
        }
    }
};
WalletDao = __decorate([
    Object(inversify__WEBPACK_IMPORTED_MODULE_0__["injectable"])(),
    __metadata("design:paramtypes", [])
], WalletDao);



/***/ }),

/***/ "./src/decorators/controller.ts":
/*!**************************************!*\
  !*** ./src/decorators/controller.ts ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);

function controller() {
    const injectableDecorator = Object(inversify__WEBPACK_IMPORTED_MODULE_0__["injectable"])();
    return function (controllerType) {
        injectableDecorator(controllerType);
    };
}
/* harmony default export */ __webpack_exports__["default"] = (controller);


/***/ }),

/***/ "./src/decorators/dao.ts":
/*!*******************************!*\
  !*** ./src/decorators/dao.ts ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);

function dao(args = {}) {
    const injectableDecorator = Object(inversify__WEBPACK_IMPORTED_MODULE_0__["injectable"])();
    return function (controllerType) {
        injectableDecorator(controllerType);
    };
}
/* harmony default export */ __webpack_exports__["default"] = (dao);


/***/ }),

/***/ "./src/decorators/route-map.ts":
/*!*************************************!*\
  !*** ./src/decorators/route-map.ts ***!
  \*************************************/
/*! exports provided: routeMap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routeMap", function() { return routeMap; });
function routeMap(value) {
    return function (target, propertyKey, descriptor) {
        if (!globalThis.mappedRoutes)
            globalThis.mappedRoutes = [];
        globalThis.mappedRoutes.push({
            path: value,
            controllerClass: target.constructor,
            action: propertyKey
        });
    };
}



/***/ }),

/***/ "./src/decorators/service.ts":
/*!***********************************!*\
  !*** ./src/decorators/service.ts ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);

function service(args = {}) {
    const injectableDecorator = Object(inversify__WEBPACK_IMPORTED_MODULE_0__["injectable"])();
    return function (controllerType) {
        injectableDecorator(controllerType);
    };
}
/* harmony default export */ __webpack_exports__["default"] = (service);


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! exports provided: QueueService, PagingService, PagingViewModel, UiService, WalletService, InvalidWalletException, SchemaService, IpfsService, OrbitService, RoutingService, WalletController, ModelView, PromiseView, Container, SpaceComponent, inject, service, controller, dao, routeMap, initTestContainer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reflect-metadata */ "reflect-metadata");
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reflect_metadata__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_model_view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util/model-view */ "./src/util/model-view.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ModelView", function() { return _util_model_view__WEBPACK_IMPORTED_MODULE_1__["ModelView"]; });

/* harmony import */ var _util_promise_view__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./util/promise-view */ "./src/util/promise-view.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PromiseView", function() { return _util_promise_view__WEBPACK_IMPORTED_MODULE_2__["PromiseView"]; });

/* harmony import */ var _service_queue_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/queue_service */ "./src/service/queue_service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "QueueService", function() { return _service_queue_service__WEBPACK_IMPORTED_MODULE_3__["QueueService"]; });

/* harmony import */ var _space_mvc__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./space-mvc */ "./src/space-mvc.ts");
/* harmony import */ var _service_ui_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service/ui-service */ "./src/service/ui-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UiService", function() { return _service_ui_service__WEBPACK_IMPORTED_MODULE_5__["UiService"]; });

/* harmony import */ var _service_paging_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/paging-service */ "./src/service/paging-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PagingService", function() { return _service_paging_service__WEBPACK_IMPORTED_MODULE_6__["PagingService"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PagingViewModel", function() { return _service_paging_service__WEBPACK_IMPORTED_MODULE_6__["PagingViewModel"]; });

/* harmony import */ var _service_wallet_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./service/wallet-service */ "./src/service/wallet-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WalletService", function() { return _service_wallet_service__WEBPACK_IMPORTED_MODULE_7__["WalletService"]; });

/* harmony import */ var _service_schema_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./service/schema-service */ "./src/service/schema-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SchemaService", function() { return _service_schema_service__WEBPACK_IMPORTED_MODULE_8__["SchemaService"]; });

/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_9__);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Container", function() { return inversify__WEBPACK_IMPORTED_MODULE_9__["Container"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "inject", function() { return inversify__WEBPACK_IMPORTED_MODULE_9__["inject"]; });

/* harmony import */ var _service_ipfs_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./service/ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "IpfsService", function() { return _service_ipfs_service__WEBPACK_IMPORTED_MODULE_10__["IpfsService"]; });

/* harmony import */ var _service_exception_invalid_wallet_exception__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./service/exception/invalid-wallet-exception */ "./src/service/exception/invalid-wallet-exception.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InvalidWalletException", function() { return _service_exception_invalid_wallet_exception__WEBPACK_IMPORTED_MODULE_11__["InvalidWalletException"]; });

/* harmony import */ var _controller_wallet_controller__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./controller/wallet-controller */ "./src/controller/wallet-controller.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WalletController", function() { return _controller_wallet_controller__WEBPACK_IMPORTED_MODULE_12__["WalletController"]; });

/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./service/routing-service */ "./src/service/routing-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return _service_routing_service__WEBPACK_IMPORTED_MODULE_13__["RoutingService"]; });

/* harmony import */ var _decorators_controller__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./decorators/controller */ "./src/decorators/controller.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "controller", function() { return _decorators_controller__WEBPACK_IMPORTED_MODULE_14__["default"]; });

/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./decorators/service */ "./src/decorators/service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "service", function() { return _decorators_service__WEBPACK_IMPORTED_MODULE_15__["default"]; });

/* harmony import */ var _decorators_route_map__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./decorators/route-map */ "./src/decorators/route-map.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "routeMap", function() { return _decorators_route_map__WEBPACK_IMPORTED_MODULE_16__["routeMap"]; });

/* harmony import */ var _decorators_dao__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./decorators/dao */ "./src/decorators/dao.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dao", function() { return _decorators_dao__WEBPACK_IMPORTED_MODULE_17__["default"]; });

/* harmony import */ var _util_space_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./util/space-component */ "./src/util/space-component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SpaceComponent", function() { return _util_space_component__WEBPACK_IMPORTED_MODULE_18__["SpaceComponent"]; });

/* harmony import */ var _service_orbit_service__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./service/orbit-service */ "./src/service/orbit-service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "OrbitService", function() { return _service_orbit_service__WEBPACK_IMPORTED_MODULE_19__["OrbitService"]; });

/* harmony import */ var _test_test_inversify_config__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../test/test-inversify.config */ "./test/test-inversify.config.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "initTestContainer", function() { return _test_test_inversify_config__WEBPACK_IMPORTED_MODULE_20__["initTestContainer"]; });























/* harmony default export */ __webpack_exports__["default"] = (_space_mvc__WEBPACK_IMPORTED_MODULE_4__["SpaceMVC"]);


/***/ }),

/***/ "./src/inversify.config.ts":
/*!*********************************!*\
  !*** ./src/inversify.config.ts ***!
  \*********************************/
/*! exports provided: buildContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buildContainer", function() { return buildContainer; });
/* harmony import */ var _service_ui_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service/ui-service */ "./src/service/ui-service.ts");
/* harmony import */ var _service_paging_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service/paging-service */ "./src/service/paging-service.ts");
/* harmony import */ var _service_identity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/identity-service */ "./src/service/identity-service.ts");
/* harmony import */ var _service_wallet_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/wallet-service */ "./src/service/wallet-service.ts");
/* harmony import */ var _service_schema_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./service/schema-service */ "./src/service/schema-service.ts");
/* harmony import */ var _service_orbit_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service/orbit-service */ "./src/service/orbit-service.ts");
/* harmony import */ var _service_ipfs_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _dao_wallet_dao__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dao/wallet-dao */ "./src/dao/wallet-dao.ts");
/* harmony import */ var _controller_wallet_controller__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./controller/wallet-controller */ "./src/controller/wallet-controller.ts");
/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./service/routing-service */ "./src/service/routing-service.ts");
/* harmony import */ var _service_queue_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./service/queue_service */ "./src/service/queue_service.ts");
/* harmony import */ var _controller_connect_controller__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./controller/connect-controller */ "./src/controller/connect-controller.ts");
/* harmony import */ var _service_connect_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./service/connect-service */ "./src/service/connect-service.ts");
/* harmony import */ var _service_binding_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./service/binding-service */ "./src/service/binding-service.ts");














function buildContainer(container, config) {
    function framework7() {
        const Framework7 = __webpack_require__(/*! framework7/js/framework7.bundle */ "framework7/js/framework7.bundle");
        return new Framework7(config);
    }
    container.bind("framework7").toConstantValue(framework7());
    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {
        container.bind("ipfsOptions").toConstantValue({
            repo: `space-mvc-default`,
            EXPERIMENTAL: {
                ipnsPubsub: true
            },
            preload: {
                enabled: false
            },
            relay: {
                enabled: true,
                hop: {
                    enabled: true // enable circuit relay HOP (make this node a relay)
                }
            },
            config: {
                Addresses: {
                    Swarm: []
                }
            }
        });
    }
    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({});
    }
    container.bind(_service_identity_service__WEBPACK_IMPORTED_MODULE_2__["IdentityService"]).toSelf().inSingletonScope();
    container.bind(_dao_wallet_dao__WEBPACK_IMPORTED_MODULE_7__["WalletDao"]).toSelf().inSingletonScope();
    container.bind(_service_ui_service__WEBPACK_IMPORTED_MODULE_0__["UiService"]).toSelf().inSingletonScope();
    container.bind(_service_queue_service__WEBPACK_IMPORTED_MODULE_10__["QueueService"]).toSelf().inSingletonScope();
    container.bind(_service_paging_service__WEBPACK_IMPORTED_MODULE_1__["PagingService"]).toSelf().inSingletonScope();
    container.bind(_service_schema_service__WEBPACK_IMPORTED_MODULE_4__["SchemaService"]).toSelf().inSingletonScope();
    container.bind(_service_orbit_service__WEBPACK_IMPORTED_MODULE_5__["OrbitService"]).toSelf().inSingletonScope();
    container.bind(_service_ipfs_service__WEBPACK_IMPORTED_MODULE_6__["IpfsService"]).toSelf().inSingletonScope();
    container.bind(_service_wallet_service__WEBPACK_IMPORTED_MODULE_3__["WalletService"]).toSelf().inSingletonScope();
    container.bind(_controller_wallet_controller__WEBPACK_IMPORTED_MODULE_8__["WalletController"]).toSelf().inSingletonScope();
    container.bind(_service_connect_service__WEBPACK_IMPORTED_MODULE_12__["ConnectService"]).toSelf().inSingletonScope();
    container.bind(_controller_connect_controller__WEBPACK_IMPORTED_MODULE_11__["ConnectController"]).toSelf().inSingletonScope();
    container.bind(_service_routing_service__WEBPACK_IMPORTED_MODULE_9__["RoutingService"]).toSelf().inSingletonScope();
    container.bind(_service_binding_service__WEBPACK_IMPORTED_MODULE_13__["BindingService"]).toSelf().inSingletonScope();
    return container;
}


/***/ }),

/***/ "./src/service/binding-service.ts":
/*!****************************************!*\
  !*** ./src/service/binding-service.ts ***!
  \****************************************/
/*! exports provided: BindingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BindingService", function() { return BindingService; });
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

let BindingService = class BindingService {
    async initBindings(container) {
        let initilizableBindings = this._getInitializableBindings(container);
        console.log(initilizableBindings);
        for (let bean of initilizableBindings) {
            await bean.init();
        }
    }
    _getInitializableBindings(container) {
        let bindings = [];
        //TODO: probably this shouldn't be a hardcoded list
        let serviceIdentifiers = this._getServiceIdentifiers(container);
        serviceIdentifiers = serviceIdentifiers.filter(id => {
            if (this._isSkipBinding(container, id))
                return false;
            if (id == "framework7")
                return false;
            if (this._isInitializable(container, id))
                return true;
            return false;
        });
        for (let si of serviceIdentifiers) {
            bindings.push(container.get(si));
        }
        return bindings;
    }
    _getServiceIdentifiers(container) {
        //@ts-ignore
        let original = container._bindingDictionary;
        let serviceIdentifiers = [];
        original.traverse((key, value) => {
            value.forEach((binding) => {
                serviceIdentifiers.push(binding.serviceIdentifier);
            });
        });
        return serviceIdentifiers;
    }
    _isInitializable(container, obj) {
        let bean = container.get(obj);
        return bean.init != undefined;
    }
    _isSkipBinding(container, obj) {
        let bean = container.get(obj);
        return bean.skipBinding == true;
    }
};
BindingService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_0__["default"])()
], BindingService);



/***/ }),

/***/ "./src/service/connect-service.ts":
/*!****************************************!*\
  !*** ./src/service/connect-service.ts ***!
  \****************************************/
/*! exports provided: ConnectService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectService", function() { return ConnectService; });
/* harmony import */ var _ipfs_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let ConnectService = class ConnectService {
    constructor(ipfsService) {
        this.ipfsService = ipfsService;
    }
    async getConnectInfo() {
        let peers = await this.ipfsService.ipfs.swarm.peers();
        peers = peers.map(e => e.addr.toString());
        let subscribed = await this.ipfsService.ipfs.pubsub.ls();
        let id = await this.ipfsService.ipfs.id();
        let ipfsInfo = {
            addresses: id.addresses.map(e => e.toString()),
            agentVersion: id.agentVersion,
            id: id.id,
            publicKey: id.publicKey
        };
        return {
            peers: peers,
            ipfsInfo: ipfsInfo,
            peerCount: peers.length,
            subscribed: subscribed
        };
    }
    async addPeer(peerAddress) {
        return this.ipfsService.ipfs.swarm.connect(peerAddress);
    }
};
ConnectService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_1__["default"])(),
    __metadata("design:paramtypes", [_ipfs_service__WEBPACK_IMPORTED_MODULE_0__["IpfsService"]])
], ConnectService);



/***/ }),

/***/ "./src/service/exception/invalid-wallet-exception.ts":
/*!***********************************************************!*\
  !*** ./src/service/exception/invalid-wallet-exception.ts ***!
  \***********************************************************/
/*! exports provided: InvalidWalletException */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidWalletException", function() { return InvalidWalletException; });
class InvalidWalletException extends Error {
}



/***/ }),

/***/ "./src/service/identity-service.ts":
/*!*****************************************!*\
  !*** ./src/service/identity-service.ts ***!
  \*****************************************/
/*! exports provided: IdentityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdentityService", function() { return IdentityService; });
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

const EthIdentityProvider = __webpack_require__(/*! orbit-db-identity-provider/src/ethereum-identity-provider */ "orbit-db-identity-provider/src/ethereum-identity-provider");
const Identities = __webpack_require__(/*! orbit-db-identity-provider */ "orbit-db-identity-provider");
let IdentityService = class IdentityService {
    constructor() {
        Identities.addIdentityProvider(EthIdentityProvider);
    }
    async getIdentity(keystore, wallet) {
        if (this.identity)
            return this.identity;
        const type = EthIdentityProvider.type;
        const options = {
            type: type,
            keystore: keystore,
            wallet: wallet
        };
        this.identity = await Identities.createIdentity(options);
        return this.identity;
    }
    getAccessController(orbitdb) {
        return {
            write: ['*'] //[orbitdb.identity.id]
        };
    }
};
IdentityService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_0__["default"])(),
    __metadata("design:paramtypes", [])
], IdentityService);



/***/ }),

/***/ "./src/service/ipfs-service.ts":
/*!*************************************!*\
  !*** ./src/service/ipfs-service.ts ***!
  \*************************************/
/*! exports provided: IpfsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IpfsService", function() { return IpfsService; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


const IPFS = __webpack_require__(/*! ipfs */ "ipfs");
let IpfsService = class IpfsService {
    constructor(ipfsOptions) {
        this.ipfsOptions = ipfsOptions;
        this.skipBinding = true;
    }
    getClassName() {
        return "IpfsService";
    }
    async init() {
        this.ipfs = await IPFS.create(this.ipfsOptions);
    }
};
IpfsService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_1__["default"])(),
    __param(0, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("ipfsOptions")),
    __metadata("design:paramtypes", [Object])
], IpfsService);



/***/ }),

/***/ "./src/service/orbit-service.ts":
/*!**************************************!*\
  !*** ./src/service/orbit-service.ts ***!
  \**************************************/
/*! exports provided: OrbitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrbitService", function() { return OrbitService; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _identity_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./identity-service */ "./src/service/identity-service.ts");
/* harmony import */ var _ipfs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




let OrbitDB = __webpack_require__(/*! orbit-db */ "orbit-db");
let Keystore = __webpack_require__(/*! orbit-db-keystore */ "orbit-db-keystore");
let MfsStore = __webpack_require__(/*! orbit-db-mfsstore */ "orbit-db-mfsstore");
let OrbitService = class OrbitService {
    constructor(ipfsService, identityService, orbitDbOptions) {
        this.ipfsService = ipfsService;
        this.identityService = identityService;
        this.orbitDbOptions = orbitDbOptions;
        this.skipBinding = true;
    }
    getClassName() {
        return "OrbitService";
    }
    async init(wallet) {
        OrbitDB.addDatabaseType("mfsstore", MfsStore);
        if (wallet) {
            let keystore = new Keystore();
            let identity = await this.identityService.getIdentity(keystore, wallet);
            this.orbitDbOptions['identity'] = identity;
        }
        this.orbitDb = await OrbitDB.createInstance(this.ipfsService.ipfs, this.orbitDbOptions);
    }
};
OrbitService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_3__["default"])(),
    __param(2, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("orbitDbOptions")),
    __metadata("design:paramtypes", [_ipfs_service__WEBPACK_IMPORTED_MODULE_2__["IpfsService"],
        _identity_service__WEBPACK_IMPORTED_MODULE_1__["IdentityService"], Object])
], OrbitService);



/***/ }),

/***/ "./src/service/paging-service.ts":
/*!***************************************!*\
  !*** ./src/service/paging-service.ts ***!
  \***************************************/
/*! exports provided: PagingService, PagingViewModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagingService", function() { return PagingService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagingViewModel", function() { return PagingViewModel; });
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let PagingService = class PagingService {
    constructor() { }
    buildPagingViewModel(offset, limit, count) {
        let viewModel = new PagingViewModel();
        viewModel.offset = offset ? offset : 0;
        viewModel.limit = limit;
        viewModel.count = count;
        viewModel.start = viewModel.offset + 1;
        viewModel.end = Math.min(viewModel.offset + limit, count);
        viewModel.previousOffset = Math.max(viewModel.offset - limit, 0);
        if ((viewModel.offset + limit) < count - 1) {
            viewModel.nextOffset = viewModel.offset + limit;
        }
        return viewModel;
    }
};
PagingService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_0__["default"])(),
    __metadata("design:paramtypes", [])
], PagingService);
class PagingViewModel {
}



/***/ }),

/***/ "./src/service/queue_service.ts":
/*!**************************************!*\
  !*** ./src/service/queue_service.ts ***!
  \**************************************/
/*! exports provided: QueueService, QueueItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueueService", function() { return QueueService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueueItem", function() { return QueueItem; });
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let QueueService = class QueueService {
    constructor() { }
    async queuePromiseView(promiseView) {
        const self = this;
        let queueItem = new QueueItem(Guid.newGuid(), promiseView.icon, promiseView.title, promiseView.view, promiseView.context);
        let before = async function () {
            return new Promise((resolve, reject) => {
                self.beforeSaveAction(queueItem);
                resolve();
            });
        };
        let during = async function () {
            return promiseView.promise;
        };
        let after = async function (result) {
            if (result) {
                queueItem.context = result;
            }
            return new Promise((resolve, reject) => {
                self.afterSaveAction(queueItem);
                resolve();
            });
        };
        return before()
            .then(during)
            .then(after);
    }
    beforeSaveAction(queueItem) {
        // queueItem.title = this._parseTitle(queueItem.titleTemplate, queueItem.context)
        // Create toast with close button
        // queueItem.toast = Global.app.toast.create({
        //   text: queueItem.title,
        //   closeButton: true
        // })
        queueItem.toast.open();
    }
    afterSaveAction(queueItem) {
        queueItem.toast.close();
        // queueItem.link = this._parseLink(queueItem.linkTemplate, queueItem.context)
        // console.log(queueItem.context)
        // console.log(queueItem.link)
        // Global.app.toast.create({
        //   text: "Save Complete",
        //   closeButton: true,
        //   closeButtonText: "View",
        //   closeTimeout: 5000,
        //   on: {
        //     toastCloseButtonClick: function() {
        //       // Global.navigate(queueItem.link)
        //     }
        //   }
        // }).open()
    }
};
QueueService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_0__["default"])(),
    __metadata("design:paramtypes", [])
], QueueService);
class QueueItem {
    constructor(id, icon, titleTemplate, linkTemplate, context) {
        this.id = id;
        this.icon = icon;
        this.titleTemplate = titleTemplate;
        this.linkTemplate = linkTemplate;
        this.context = context;
    }
}
//from https://stackoverflow.com/questions/26501688/a-typescript-guid-class
class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}



/***/ }),

/***/ "./src/service/routing-service.ts":
/*!****************************************!*\
  !*** ./src/service/routing-service.ts ***!
  \****************************************/
/*! exports provided: RoutingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutingService", function() { return RoutingService; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ui_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ui-service */ "./src/service/ui-service.ts");
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



let RoutingService = class RoutingService {
    constructor(uiService, app) {
        this.uiService = uiService;
        this.app = app;
    }
    navigate(navigateParams, routeOptions, viewName = 'main') {
        console.log(`${viewName}: navigating to ${JSON.stringify(navigateParams)}`);
        if (!routeOptions)
            routeOptions = {
                reloadCurrent: true,
                ignoreCache: false
            };
        let view = this.app.view[viewName];
        if (view) {
            view.router.navigate(navigateParams, routeOptions);
        }
        else {
            console.log(`Could not find view ${viewName}`);
        }
    }
    buildRoutesForContainer(existing, container) {
        let routes = [];
        routes = routes.concat(existing);
        //Look up requestMappings 
        for (let mappedRoute of globalThis.mappedRoutes) {
            //Look up matching bean
            let controllerBean = container.get(mappedRoute.controllerClass);
            routes.push({
                path: mappedRoute.path,
                async: async (routeTo, routeFrom, resolve, reject) => {
                    try {
                        await this._doResolve(routeTo, routeFrom, resolve, reject, controllerBean[mappedRoute.action]());
                    }
                    catch (ex) {
                        this.uiService.showExceptionPopup(ex);
                    }
                }
            });
        }
        //Needs to be last
        routes.push({
            path: '(.*)',
            // url: 'pages/404.html',
            async async(routeTo, routeFrom, resolve, reject) {
                // this.uiService.showPopup("Page was not found")
                console.log(`404 error: ${routeTo.path}`);
            }
        });
        console.log(routes);
        return routes;
    }
    async _doResolve(routeTo, routeFrom, resolve, reject, controller_promise) {
        var _a, _b;
        //TODO: this should probably be optional somehow. 
        this.uiService.showSpinner();
        let modelView = await controller_promise;
        if (!modelView)
            return;
        let model = await modelView.model;
        let contextData;
        //If we get passed a component it means we're trying to resolve back to the same one.
        //See SpaceComponent.formSubmit. Still a work in progress. 
        if ((_a = routeTo.context) === null || _a === void 0 ? void 0 : _a.component) {
            contextData = (_b = routeTo.context) === null || _b === void 0 ? void 0 : _b.data;
        }
        else {
            contextData = routeTo.context;
        }
        let updatedState = await model(contextData);
        if (modelView.view) {
            //Load the new component if it's given to us. 
            resolve({
                component: modelView.view
            }, {
                context: updatedState
            });
        }
        else {
            //Otherwise update the state of the current component.
            if (routeTo.context) {
                let component = routeTo.context.component;
                component.$setState(updatedState);
            }
        }
        this.uiService.hideSpinner();
    }
};
RoutingService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_2__["default"])(),
    __param(1, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("framework7")),
    __metadata("design:paramtypes", [_ui_service__WEBPACK_IMPORTED_MODULE_1__["UiService"], Object])
], RoutingService);



/***/ }),

/***/ "./src/service/schema-service.ts":
/*!***************************************!*\
  !*** ./src/service/schema-service.ts ***!
  \***************************************/
/*! exports provided: SchemaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SchemaService", function() { return SchemaService; });
/* harmony import */ var _orbit_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./orbit-service */ "./src/service/orbit-service.ts");
/* harmony import */ var _ipfs_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



const WALLET_SCHEMA = "WALLET_SCHEMA";
let SchemaService = class SchemaService {
    constructor(ipfsService, orbitService) {
        this.ipfsService = ipfsService;
        this.orbitService = orbitService;
        this.skipBinding = true;
        this.loadedSchemas = {};
    }
    /**
     * Creates a Schema object from the passed parameters and stores it in IPFS. Returns the Schema.
     *
     * @param ownerAddress
     * @param storeDefinitions
     */
    async create(ownerAddress, storeDefinitions) {
        let schema = {
            addresses: {},
            storeDefinitions: storeDefinitions
        };
        let defaultOptions = {
            create: true,
            accessController: {
                write: [ownerAddress]
            }
        };
        if (storeDefinitions) {
            for (let store of storeDefinitions) {
                // let createdStore
                let storeName = `${store.name}-${ownerAddress.toLowerCase()}`;
                let createdStore;
                switch (store.type) {
                    case "kvstore":
                        createdStore = await this.orbitService.orbitDb.kvstore(storeName, defaultOptions);
                        break;
                    case "docs":
                        createdStore = await this.orbitService.orbitDb.docs(storeName, defaultOptions);
                        break;
                    case "mfsstore":
                        let options = JSON.parse(JSON.stringify(defaultOptions));
                        options.type = "mfsstore";
                        options.schema = store.schema;
                        createdStore = await this.orbitService.orbitDb.open(storeName, options);
                        break;
                }
                schema.addresses[store.name] = createdStore.address.toString();
            }
        }
        schema.owner = ownerAddress;
        //Write global schema to IPFS
        let buffer = Buffer.from(JSON.stringify(schema));
        let cid = await this.ipfsService.ipfs.object.put(buffer);
        schema.cid = cid.toString();
        return schema;
    }
    /**
     * Loads Schema from IPFS based on cid
     * @param cid
     */
    async read(cid) {
        let loaded = await this.ipfsService.ipfs.object.get(cid);
        let data = loaded.Data ? loaded.Data.toString() : loaded.data.toString();
        let schema = JSON.parse(data);
        schema.cid = cid;
        return schema;
    }
    /**
     * Takes a Schema object and then loads all the orbit stores associated with it.
     *
     * Reads the storeDefinitions from the schema and then calls load() on the orbit store.
     *
     * @param key
     * @param schema
     */
    async load(schema) {
        let loadedSchema = {
            stores: {},
            schema: schema
        };
        for (let field in schema.addresses) {
            loadedSchema.stores[field] = await this.orbitService.orbitDb.open(schema.addresses[field]);
            let definition = schema.storeDefinitions.filter(def => def.name == field)[0];
            if (definition.load) {
                console.log(`Loading ${field}`);
                await loadedSchema.stores[field].load(definition.load);
            }
            else {
                console.log(`Skipping ${field}`);
            }
        }
        this.loadedSchemas[WALLET_SCHEMA] = loadedSchema;
    }
    getStore(storeName) {
        return this.loadedSchemas[WALLET_SCHEMA].stores[storeName];
    }
    async getSchemaInfo() {
        let stores = this.loadedSchemas[WALLET_SCHEMA].stores;
        let translated = [];
        for (let key in stores) {
            let store = stores[key];
            let count = await store.count();
            translated.push({
                name: key,
                address: store.address.toString(),
                loaded: count
            });
        }
        //Get a copy of the global schema
        let globalSchema = await this.loadedSchemas[WALLET_SCHEMA].schema;
        return {
            stores: translated,
            globalSchema: globalSchema
        };
    }
};
SchemaService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_2__["default"])(),
    __metadata("design:paramtypes", [_ipfs_service__WEBPACK_IMPORTED_MODULE_1__["IpfsService"],
        _orbit_service__WEBPACK_IMPORTED_MODULE_0__["OrbitService"]])
], SchemaService);



/***/ }),

/***/ "./src/service/ui-service.ts":
/*!***********************************!*\
  !*** ./src/service/ui-service.ts ***!
  \***********************************/
/*! exports provided: UiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiService", function() { return UiService; });
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


let UiService = class UiService {
    constructor(app) {
        this.app = app;
    }
    showExceptionPopup(ex) {
        console.log(ex);
        this.app.dialog.alert(ex.message, "There was an error");
    }
    showPopup(message) {
        this.app.dialog.alert(message);
    }
    showSpinner(message) {
        if (this.spinnerDialog)
            this.hideSpinner();
        this.spinnerDialog = this.app.dialog.preloader(message ? message : "Loading");
    }
    hideSpinner() {
        if (this.spinnerDialog) {
            this.spinnerDialog.close();
            this.spinnerDialog = null;
        }
    }
};
UiService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_1__["default"])(),
    __param(0, Object(inversify__WEBPACK_IMPORTED_MODULE_0__["inject"])("framework7")),
    __metadata("design:paramtypes", [Object])
], UiService);



/***/ }),

/***/ "./src/service/wallet-service.ts":
/*!***************************************!*\
  !*** ./src/service/wallet-service.ts ***!
  \***************************************/
/*! exports provided: WalletService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletService", function() { return WalletService; });
/* harmony import */ var _dao_wallet_dao__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../dao/wallet-dao */ "./src/dao/wallet-dao.ts");
/* harmony import */ var _decorators_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../decorators/service */ "./src/decorators/service.ts");
/* harmony import */ var _ui_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui-service */ "./src/service/ui-service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const { ethers, Wallet, providers } = __webpack_require__(/*! ethers */ "ethers");



let WalletService = class WalletService {
    constructor(walletDao, uiService) {
        this.walletDao = walletDao;
        this.uiService = uiService;
        this.skipBinding = true;
    }
    getClassName() {
        return "WalletService";
    }
    async init() {
        //TODO: Probably break this out into different implementation depending on the scenario. 
        //Then just inject the right scenario at startup. Maybe built to a profile or whatever inversify calls them. 
        if (window['ethereum']) {
            // Request account access
            await window['ethereum'].enable();
            //@ts-ignore
            window.web3Provider = window.ethereum;
            //@ts-ignore
            web3 = new Web3(window.web3Provider);
            //@ts-ignore
            this.provider = new providers.Web3Provider(web3.currentProvider);
            this.wallet = this.provider.getSigner();
        }
        else {
            //We need our own provider.
            this.provider = ethers.getDefaultProvider("homestead");
        }
    }
    async createWallet(password) {
        this.uiService.showSpinner("Creating wallet. Please wait.");
        //Generate random words
        let wallet = await Wallet.createRandom();
        this.uiService.showSpinner("Encrypting wallet...");
        //Encrypt with entered password
        let encryptedJsonWallet = await wallet.encrypt(password);
        await this.walletDao.saveWallet(encryptedJsonWallet);
        await this.connectWallet(wallet);
        this.uiService.hideSpinner();
        return this.wallet.mnemonic.phrase.split(" ");
    }
    async connectWallet(wallet) {
        this.wallet = wallet;
        this.wallet = await this.wallet.connect(this.provider);
    }
    async unlockWallet(password) {
        let savedWallet = await this.walletDao.loadWallet();
        if (!savedWallet)
            throw new Error("No wallet to unlock");
        let wallet = await Wallet.fromEncryptedJson(savedWallet, password);
        return this.connectWallet(wallet);
    }
    async restoreWallet(recoverySeed, password) {
        let wallet = await Wallet.fromMnemonic(recoverySeed);
        if (!wallet)
            return;
        //Encrypt with entered password
        let encryptedJsonWallet = await wallet.encrypt(password);
        await this.walletDao.saveWallet(encryptedJsonWallet);
        await this.connectWallet(wallet);
        return wallet;
    }
    getWallet() {
        return this.walletDao.loadWallet();
    }
    logout() {
        this.wallet = undefined;
    }
};
WalletService = __decorate([
    Object(_decorators_service__WEBPACK_IMPORTED_MODULE_1__["default"])(),
    __metadata("design:paramtypes", [_dao_wallet_dao__WEBPACK_IMPORTED_MODULE_0__["WalletDao"],
        _ui_service__WEBPACK_IMPORTED_MODULE_2__["UiService"]])
], WalletService);



/***/ }),

/***/ "./src/space-mvc.ts":
/*!**************************!*\
  !*** ./src/space-mvc.ts ***!
  \**************************/
/*! exports provided: SpaceMVC */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpaceMVC", function() { return SpaceMVC; });
/* harmony import */ var _service_ui_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./service/ui-service */ "./src/service/ui-service.ts");
/* harmony import */ var _service_paging_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./service/paging-service */ "./src/service/paging-service.ts");
/* harmony import */ var _service_identity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/identity-service */ "./src/service/identity-service.ts");
/* harmony import */ var _service_wallet_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./service/wallet-service */ "./src/service/wallet-service.ts");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! events */ "events");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _service_schema_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./service/schema-service */ "./src/service/schema-service.ts");
/* harmony import */ var _service_orbit_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./service/orbit-service */ "./src/service/orbit-service.ts");
/* harmony import */ var _service_ipfs_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./service/ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _service_exception_invalid_wallet_exception__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./service/exception/invalid-wallet-exception */ "./src/service/exception/invalid-wallet-exception.ts");
/* harmony import */ var _inversify_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./inversify.config */ "./src/inversify.config.ts");
/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./service/routing-service */ "./src/service/routing-service.ts");
/* harmony import */ var _service_queue_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./service/queue_service */ "./src/service/queue_service.ts");
/* harmony import */ var _service_binding_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./service/binding-service */ "./src/service/binding-service.ts");













let appName;
let eventEmitter = new events__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
var SpaceMVC;
(function (SpaceMVC) {
    function getEventEmitter() {
        return eventEmitter;
    }
    SpaceMVC.getEventEmitter = getEventEmitter;
    function getContainer() {
        return globalThis.container;
    }
    SpaceMVC.getContainer = getContainer;
    function name() {
        return appName;
    }
    SpaceMVC.name = name;
    function app() {
        return getContainer().get("framework7");
    }
    SpaceMVC.app = app;
    function getWallet() {
        return _walletService().wallet;
    }
    SpaceMVC.getWallet = getWallet;
    async function init(container, f7Config, ownerAddress, storeDefinitions) {
        container.bind("ownerAddress").toConstantValue(ownerAddress);
        container.bind('storeDefinitions').toConstantValue(storeDefinitions);
        await SpaceMVC.initWallet(container, f7Config);
        if (SpaceMVC.getWallet()) {
            SpaceMVC.initMainView();
            await SpaceMVC.initFinish(ownerAddress, storeDefinitions);
        }
        else {
            SpaceMVC.initMainView();
            _routingService().navigate({
                path: "/wallet"
            });
        }
    }
    SpaceMVC.init = init;
    async function initWallet(container, f7Config) {
        console.log("Initializing SpaceMVC wallet");
        appName = f7Config.name;
        globalThis.container = Object(_inversify_config__WEBPACK_IMPORTED_MODULE_9__["buildContainer"])(container, f7Config);
        //Include default routing
        let routes = _routingService().buildRoutesForContainer(SpaceMVC.app().routes, globalThis.container);
        SpaceMVC.app().routes = routes;
        //Get IPFS options and initialize
        await _ipfsService().init();
        //Initialize wallet
        await _walletService().init();
    }
    SpaceMVC.initWallet = initWallet;
    async function initSchema(ownerAddress, storeDefinitions) {
        if (!SpaceMVC.getWallet()) {
            throw new _service_exception_invalid_wallet_exception__WEBPACK_IMPORTED_MODULE_8__["InvalidWalletException"]("No wallet found. Unable to initialize.");
        }
        let walletAddress = await SpaceMVC.getWallet().getAddress();
        console.log(`Initializing ${appName} for wallet: ${walletAddress}`);
        _uiService().showSpinner('Initializing wallet');
        await _orbitService().init(SpaceMVC.getWallet());
        let schema = await _schemaService().create(await SpaceMVC.getWallet().getAddress(), storeDefinitions);
        await _schemaService().load(schema);
        //Init all the Initializable bindings in the container 
        await _bindingService().initBindings(getContainer());
        _uiService().hideSpinner();
    }
    SpaceMVC.initSchema = initSchema;
    function initMainView() {
        //Create main view
        SpaceMVC.app().views.create('.view-main', {
            pushState: true,
            loadInitialPage: false
        });
    }
    SpaceMVC.initMainView = initMainView;
    async function initFinish(ownerAddress, storeDefinitions) {
        await SpaceMVC.initSchema(ownerAddress, storeDefinitions);
        _routingService().navigate({
            path: "/"
        });
        eventEmitter.emit('spacemvc:initFinish');
    }
    SpaceMVC.initFinish = initFinish;
    function _ipfsService() {
        return getContainer().get(_service_ipfs_service__WEBPACK_IMPORTED_MODULE_7__["IpfsService"]);
    }
    function _orbitService() {
        return getContainer().get(_service_orbit_service__WEBPACK_IMPORTED_MODULE_6__["OrbitService"]);
    }
    function _queueService() {
        return getContainer().get(_service_queue_service__WEBPACK_IMPORTED_MODULE_11__["QueueService"]);
    }
    function _pagingService() {
        return getContainer().get(_service_paging_service__WEBPACK_IMPORTED_MODULE_1__["PagingService"]);
    }
    function _uiService() {
        return getContainer().get(_service_ui_service__WEBPACK_IMPORTED_MODULE_0__["UiService"]);
    }
    function _identityService() {
        return getContainer().get(_service_identity_service__WEBPACK_IMPORTED_MODULE_2__["IdentityService"]);
    }
    function _walletService() {
        return getContainer().get(_service_wallet_service__WEBPACK_IMPORTED_MODULE_3__["WalletService"]);
    }
    function _schemaService() {
        return getContainer().get(_service_schema_service__WEBPACK_IMPORTED_MODULE_5__["SchemaService"]);
    }
    function _routingService() {
        return getContainer().get(_service_routing_service__WEBPACK_IMPORTED_MODULE_10__["RoutingService"]);
    }
    function _bindingService() {
        return getContainer().get(_service_binding_service__WEBPACK_IMPORTED_MODULE_12__["BindingService"]);
    }
})(SpaceMVC || (SpaceMVC = {}));


/***/ }),

/***/ "./src/util/model-view.ts":
/*!********************************!*\
  !*** ./src/util/model-view.ts ***!
  \********************************/
/*! exports provided: ModelView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModelView", function() { return ModelView; });
class ModelView {
    constructor(model, view) {
        this.model = model;
        this.view = view;
    }
}



/***/ }),

/***/ "./src/util/promise-view.ts":
/*!**********************************!*\
  !*** ./src/util/promise-view.ts ***!
  \**********************************/
/*! exports provided: PromiseView */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromiseView", function() { return PromiseView; });
class PromiseView {
    constructor(promise, title, icon, context, view) {
        this.promise = promise;
        this.title = title;
        this.icon = icon;
        this.context = context;
        this.view = view;
    }
}



/***/ }),

/***/ "./src/util/space-component.ts":
/*!*************************************!*\
  !*** ./src/util/space-component.ts ***!
  \*************************************/
/*! exports provided: SpaceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpaceComponent", function() { return SpaceComponent; });
/* harmony import */ var framework7__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! framework7 */ "framework7");
/* harmony import */ var framework7__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(framework7__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _space_mvc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../space-mvc */ "./src/space-mvc.ts");
/* harmony import */ var _service_routing_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/routing-service */ "./src/service/routing-service.ts");



class SpaceComponent extends framework7__WEBPACK_IMPORTED_MODULE_0__["Component"] {
    getApp() {
        return _space_mvc__WEBPACK_IMPORTED_MODULE_1__["SpaceMVC"].app();
    }
    getRoutingService() {
        return _space_mvc__WEBPACK_IMPORTED_MODULE_1__["SpaceMVC"].getContainer().get(_service_routing_service__WEBPACK_IMPORTED_MODULE_2__["RoutingService"]);
    }
    navigate(navigateParams, routeOptions, viewName) {
        this.getRoutingService().navigate(navigateParams, routeOptions, viewName);
    }
    submitForm(e, formId) {
        e.preventDefault();
        let data = _space_mvc__WEBPACK_IMPORTED_MODULE_1__["SpaceMVC"].app().form.convertToData(formId);
        //@ts-ignore
        let form = this.$$(formId)[0];
        //@ts-ignore
        if (!form.checkValidity())
            return;
        //@ts-ignore
        let action = this.$$(form).attr('action');
        this.getRoutingService().navigate({
            path: action
        }, {
            ignoreCache: true,
            context: {
                data: data,
                component: this
            }
        });
    }
}



/***/ }),

/***/ "./test/test-inversify.config.ts":
/*!***************************************!*\
  !*** ./test/test-inversify.config.ts ***!
  \***************************************/
/*! exports provided: initTestContainer, getTestContainer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initTestContainer", function() { return initTestContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTestContainer", function() { return getTestContainer; });
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reflect-metadata */ "reflect-metadata");
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reflect_metadata__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! inversify */ "inversify");
/* harmony import */ var inversify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(inversify__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _src_service_ipfs_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../src/service/ipfs-service */ "./src/service/ipfs-service.ts");
/* harmony import */ var _src_service_orbit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../src/service/orbit-service */ "./src/service/orbit-service.ts");
/* harmony import */ var _src_service_identity_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../src/service/identity-service */ "./src/service/identity-service.ts");
/* harmony import */ var _src_service_binding_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../src/service/binding-service */ "./src/service/binding-service.ts");
/* harmony import */ var _src_service_schema_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../src/service/schema-service */ "./src/service/schema-service.ts");







let container;
async function initTestContainer(container, storeDefinitions) {
    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {
        container.bind("ipfsOptions").toConstantValue({
            repo: './test/.tmp/test-repos/' + Math.random().toString()
        });
    }
    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({
            directory: "./test/.tmp/orbitdb/" + Math.random().toString()
        });
    }
    container.bind(_src_service_identity_service__WEBPACK_IMPORTED_MODULE_4__["IdentityService"]).toSelf().inSingletonScope();
    container.bind(_src_service_orbit_service__WEBPACK_IMPORTED_MODULE_3__["OrbitService"]).toSelf().inSingletonScope();
    container.bind(_src_service_ipfs_service__WEBPACK_IMPORTED_MODULE_2__["IpfsService"]).toSelf().inSingletonScope();
    container.bind(_src_service_schema_service__WEBPACK_IMPORTED_MODULE_6__["SchemaService"]).toSelf().inSingletonScope();
    container.bind(_src_service_binding_service__WEBPACK_IMPORTED_MODULE_5__["BindingService"]).toSelf().inSingletonScope();
    let ipfsService = container.get(_src_service_ipfs_service__WEBPACK_IMPORTED_MODULE_2__["IpfsService"]);
    let orbitService = container.get(_src_service_orbit_service__WEBPACK_IMPORTED_MODULE_3__["OrbitService"]);
    let schemaService = container.get(_src_service_schema_service__WEBPACK_IMPORTED_MODULE_6__["SchemaService"]);
    let bindingService = container.get(_src_service_binding_service__WEBPACK_IMPORTED_MODULE_5__["BindingService"]);
    await ipfsService.init();
    await orbitService.init();
    let schema = await schemaService.create(orbitService.orbitDb.identity.id, storeDefinitions);
    await schemaService.load(schema);
    await bindingService.initBindings(container);
    return container;
}
async function getTestContainer() {
    if (container)
        return container;
    container = new inversify__WEBPACK_IMPORTED_MODULE_1__["Container"]();
    return initTestContainer(container, undefined);
}


/***/ }),

/***/ "ethers":
/*!*************************!*\
  !*** external "ethers" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("ethers");

/***/ }),

/***/ "events":
/*!*************************!*\
  !*** external "events" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("events");

/***/ }),

/***/ "framework7":
/*!*****************************!*\
  !*** external "framework7" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("framework7");

/***/ }),

/***/ "framework7/js/framework7.bundle":
/*!**************************************************!*\
  !*** external "framework7/js/framework7.bundle" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("framework7/js/framework7.bundle");

/***/ }),

/***/ "inversify":
/*!****************************!*\
  !*** external "inversify" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("inversify");

/***/ }),

/***/ "ipfs":
/*!***********************!*\
  !*** external "ipfs" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("ipfs");

/***/ }),

/***/ "level-js":
/*!***************************!*\
  !*** external "level-js" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("level-js");

/***/ }),

/***/ "levelup":
/*!**************************!*\
  !*** external "levelup" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("levelup");

/***/ }),

/***/ "orbit-db":
/*!***************************!*\
  !*** external "orbit-db" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("orbit-db");

/***/ }),

/***/ "orbit-db-identity-provider":
/*!*********************************************!*\
  !*** external "orbit-db-identity-provider" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("orbit-db-identity-provider");

/***/ }),

/***/ "orbit-db-identity-provider/src/ethereum-identity-provider":
/*!****************************************************************************!*\
  !*** external "orbit-db-identity-provider/src/ethereum-identity-provider" ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("orbit-db-identity-provider/src/ethereum-identity-provider");

/***/ }),

/***/ "orbit-db-keystore":
/*!************************************!*\
  !*** external "orbit-db-keystore" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("orbit-db-keystore");

/***/ }),

/***/ "orbit-db-mfsstore":
/*!************************************!*\
  !*** external "orbit-db-mfsstore" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("orbit-db-mfsstore");

/***/ }),

/***/ "reflect-metadata":
/*!***********************************!*\
  !*** external "reflect-metadata" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("reflect-metadata");

/***/ }),

/***/ "template7":
/*!****************************!*\
  !*** external "template7" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("template7");

/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9jb25uZWN0L2luZGV4LmY3Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvd2FsbGV0L2NyZWF0ZS13YWxsZXQtc3VjY2Vzcy5mNy5odG1sIiwid2VicGFjazovLy8uL3NyYy9jb21wb25lbnRzL3dhbGxldC9jcmVhdGUtd2FsbGV0LmY3Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvd2FsbGV0L2VudGVyLXJlY292ZXJ5LmY3Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvd2FsbGV0L2xhbmRpbmcuZjcuaHRtbCIsIndlYnBhY2s6Ly8vLi9zcmMvY29udHJvbGxlci9jb25uZWN0LWNvbnRyb2xsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbnRyb2xsZXIvd2FsbGV0LWNvbnRyb2xsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2Rhby93YWxsZXQtZGFvLnRzIiwid2VicGFjazovLy8uL3NyYy9kZWNvcmF0b3JzL2NvbnRyb2xsZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2RlY29yYXRvcnMvZGFvLnRzIiwid2VicGFjazovLy8uL3NyYy9kZWNvcmF0b3JzL3JvdXRlLW1hcC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvZGVjb3JhdG9ycy9zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NyYy9pbmRleC50cyIsIndlYnBhY2s6Ly8vLi9zcmMvaW52ZXJzaWZ5LmNvbmZpZy50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS9iaW5kaW5nLXNlcnZpY2UudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZpY2UvY29ubmVjdC1zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2aWNlL2V4Y2VwdGlvbi9pbnZhbGlkLXdhbGxldC1leGNlcHRpb24udHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZpY2UvaWRlbnRpdHktc2VydmljZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS9pcGZzLXNlcnZpY2UudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZpY2Uvb3JiaXQtc2VydmljZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS9wYWdpbmctc2VydmljZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS9xdWV1ZV9zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2aWNlL3JvdXRpbmctc2VydmljZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS9zY2hlbWEtc2VydmljZS50cyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmljZS91aS1zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2aWNlL3dhbGxldC1zZXJ2aWNlLnRzIiwid2VicGFjazovLy8uL3NyYy9zcGFjZS1tdmMudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWwvbW9kZWwtdmlldy50cyIsIndlYnBhY2s6Ly8vLi9zcmMvdXRpbC9wcm9taXNlLXZpZXcudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWwvc3BhY2UtY29tcG9uZW50LnRzIiwid2VicGFjazovLy8uL3Rlc3QvdGVzdC1pbnZlcnNpZnkuY29uZmlnLnRzIiwid2VicGFjazovLy9leHRlcm5hbCBcImV0aGVyc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImV2ZW50c1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImZyYW1ld29yazdcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmcmFtZXdvcms3L2pzL2ZyYW1ld29yazcuYnVuZGxlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiaW52ZXJzaWZ5XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiaXBmc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImxldmVsLWpzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibGV2ZWx1cFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm9yYml0LWRiXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwib3JiaXQtZGItaWRlbnRpdHktcHJvdmlkZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJvcmJpdC1kYi1pZGVudGl0eS1wcm92aWRlci9zcmMvZXRoZXJldW0taWRlbnRpdHktcHJvdmlkZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJvcmJpdC1kYi1rZXlzdG9yZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm9yYml0LWRiLW1mc3N0b3JlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVmbGVjdC1tZXRhZGF0YVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInRlbXBsYXRlN1wiIl0sIm5hbWVzIjpbIlRlbXBsYXRlN0hlbHBlcnMiLCJUZW1wbGF0ZTciLCJoZWxwZXJzIiwicmVnaXN0ZXJQYXJ0aWFsIiwiRjdQYWdlQ29tcG9uZW50IiwiY3R4XzEiLCJkYXRhXzEiLCJyb290IiwiaXNBcnJheSIsImFyciIsIkFycmF5IiwiaXNGdW5jdGlvbiIsImZ1bmMiLCJjIiwidmFsIiwiY3R4IiwiY2FsbCIsInIiLCJfcGFydGlhbCIsImhhc2giLCJkYXRhIiwiZm4iLCJlbXB0eSIsImludmVyc2UiLCJwYXJlbnRzIiwiY29ubmVjdCIsImN0eF8yIiwiZGF0YV8yIiwicGVlckNvdW50IiwiZWFjaCIsInBlZXJzIiwiY3R4XzMiLCJkYXRhXzMiLCJpcGZzSW5mbyIsImFkZHJlc3NlcyIsImFnZW50VmVyc2lvbiIsImlkIiwic3Vic2NyaWJlZCIsInNjaGVtYSIsImdsb2JhbFNjaGVtYSIsImNpZCIsIm93bmVyIiwic3RvcmVzIiwibmFtZSIsImxvYWRlZCIsImFkZHJlc3MiLCJhZGRQZWVyQ2xpY2siLCJiaW5kIiwiZSIsInN1Ym1pdEZvcm0iLCJTcGFjZUNvbXBvbmVudCIsInN0eWxlU2NvcGVkIiwibW5lbW9uaWMiLCJqcyIsImNyZWF0ZUNsaWNrIiwic2VsZiIsInZhbGlkYXRlTWF0Y2hpbmdQYXNzd29yZHMiLCJwYXNzd29yZCIsIiQkIiwiY29uZmlybVBhc3N3b3JkIiwic2V0Q3VzdG9tVmFsaWRpdHkiLCJvbiIsInJlc3RvcmVDbGljayIsImFwcE5hbWUiLCJzaG93VW5sb2NrIiwidW5sb2NrQ2xpY2siXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO1FDVkE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRk07QUFDQSxJQUFNQSxnQkFBZ0IsR0FBR0MsZ0RBQVMsQ0FBQ0MsT0FBbkM7QUFFQUQsZ0RBQVMsQ0FBQ0UsZUFBVixDQUEwQixNQUExQixFQUFrQyx5SEFBbEMsRSxDQUVBOztDQUN1RDs7SUFFdkRDLGU7Ozs7Ozs7Ozs7Ozs7NkJBQ0s7QUFDUCxhQUFPLFVBQVVDLEtBQVYsRUFBaUJDLE1BQWpCLEVBQXlCQyxJQUF6QixFQUErQjtBQUNwQyxpQkFBU0MsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEIsaUJBQU9DLEtBQUssQ0FBQ0YsT0FBTixDQUFjQyxHQUFkLENBQVA7QUFDRDs7QUFFRCxpQkFBU0UsVUFBVCxDQUFvQkMsSUFBcEIsRUFBMEI7QUFDeEIsaUJBQU8sT0FBT0EsSUFBUCxLQUFnQixVQUF2QjtBQUNEOztBQUVELGlCQUFTQyxDQUFULENBQVdDLEdBQVgsRUFBZ0JDLEdBQWhCLEVBQXFCO0FBQ25CLGNBQUksT0FBT0QsR0FBUCxLQUFlLFdBQWYsSUFBOEJBLEdBQUcsS0FBSyxJQUExQyxFQUFnRDtBQUM5QyxnQkFBSUgsVUFBVSxDQUFDRyxHQUFELENBQWQsRUFBcUI7QUFDbkIscUJBQU9BLEdBQUcsQ0FBQ0UsSUFBSixDQUFTRCxHQUFULENBQVA7QUFDRCxhQUZELE1BRU8sT0FBT0QsR0FBUDtBQUNSLFdBSkQsTUFJTyxPQUFPLEVBQVA7QUFDUjs7QUFFRFAsWUFBSSxHQUFHQSxJQUFJLElBQUlGLEtBQVIsSUFBaUIsRUFBeEI7QUFDQSxZQUFJWSxDQUFDLEdBQUcsRUFBUjtBQUNBQSxTQUFDLElBQUkseUhBQUw7QUFDQUEsU0FBQyxJQUFJakIsZ0JBQWdCLENBQUNrQixRQUFqQixDQUEwQkYsSUFBMUIsQ0FBK0JYLEtBQS9CLEVBQXNDLE1BQXRDLEVBQThDO0FBQ2pEYyxjQUFJLEVBQUUsRUFEMkM7QUFFakRDLGNBQUksRUFBRWQsTUFBTSxJQUFJLEVBRmlDO0FBR2pEZSxZQUFFLEVBQUUsU0FBU0MsS0FBVCxHQUFpQjtBQUNuQixtQkFBTyxFQUFQO0FBQ0QsV0FMZ0Q7QUFNakRDLGlCQUFPLEVBQUUsU0FBU0QsS0FBVCxHQUFpQjtBQUN4QixtQkFBTyxFQUFQO0FBQ0QsV0FSZ0Q7QUFTakRmLGNBQUksRUFBRUEsSUFUMkM7QUFVakRpQixpQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBVndDLFNBQTlDLENBQUw7QUFZQVksU0FBQyxJQUFJLCtIQUFMO0FBQ0FBLFNBQUMsSUFBSWpCLGdCQUFnQixRQUFoQixDQUFzQmdCLElBQXRCLENBQTJCWCxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDb0IsT0FBeEMsRUFBaUQ7QUFDcEROLGNBQUksRUFBRSxFQUQ4QztBQUVwREMsY0FBSSxFQUFFZCxNQUFNLElBQUksRUFGb0M7QUFHcERlLFlBQUUsRUFBRSxZQUFVSyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMzQixnQkFBSVYsQ0FBQyxHQUFHLEVBQVI7QUFDQUEsYUFBQyxJQUFJLCtFQUFMO0FBQ0FBLGFBQUMsSUFBSUosQ0FBQyxDQUFDYSxLQUFLLENBQUNFLFNBQVAsRUFBa0JGLEtBQWxCLENBQU47QUFDQVQsYUFBQyxJQUFJLGdFQUFMO0FBQ0FBLGFBQUMsSUFBSWpCLGdCQUFnQixDQUFDNkIsSUFBakIsQ0FBc0JiLElBQXRCLENBQTJCVSxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDSSxLQUF4QyxFQUErQztBQUNsRFgsa0JBQUksRUFBRSxFQUQ0QztBQUVsREMsa0JBQUksRUFBRU8sTUFBTSxJQUFJLEVBRmtDO0FBR2xETixnQkFBRSxFQUFFLFlBQVVVLEtBQVYsRUFBaUJDLE1BQWpCLEVBQXlCO0FBQzNCLG9CQUFJZixDQUFDLEdBQUcsRUFBUjtBQUNBQSxpQkFBQyxJQUFJLHVGQUFMO0FBQ0FBLGlCQUFDLElBQUlKLENBQUMsQ0FBQ2tCLEtBQUQsRUFBUUEsS0FBUixDQUFOO0FBQ0FkLGlCQUFDLElBQUkseUJBQUw7QUFDQSx1QkFBT0EsQ0FBUDtBQUNELGVBVGlEO0FBVWxETSxxQkFBTyxFQUFFLFNBQVNELEtBQVQsR0FBaUI7QUFDeEIsdUJBQU8sRUFBUDtBQUNELGVBWmlEO0FBYWxEZixrQkFBSSxFQUFFQSxJQWI0QztBQWNsRGlCLHFCQUFPLEVBQUUsQ0FBQ25CLEtBQUQ7QUFkeUMsYUFBL0MsQ0FBTDtBQWdCQVksYUFBQyxJQUFJLDBGQUFMO0FBQ0FBLGFBQUMsSUFBSWpCLGdCQUFnQixDQUFDNkIsSUFBakIsQ0FBc0JiLElBQXRCLENBQTJCVSxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDTyxRQUFOLENBQWVDLFNBQWpELEVBQTREO0FBQy9EZixrQkFBSSxFQUFFLEVBRHlEO0FBRS9EQyxrQkFBSSxFQUFFTyxNQUFNLElBQUksRUFGK0M7QUFHL0ROLGdCQUFFLEVBQUUsWUFBVVUsS0FBVixFQUFpQkMsTUFBakIsRUFBeUI7QUFDM0Isb0JBQUlmLENBQUMsR0FBRyxFQUFSO0FBQ0FBLGlCQUFDLElBQUksdUZBQUw7QUFDQUEsaUJBQUMsSUFBSUosQ0FBQyxDQUFDa0IsS0FBRCxFQUFRQSxLQUFSLENBQU47QUFDQWQsaUJBQUMsSUFBSSx5QkFBTDtBQUNBLHVCQUFPQSxDQUFQO0FBQ0QsZUFUOEQ7QUFVL0RNLHFCQUFPLEVBQUUsU0FBU0QsS0FBVCxHQUFpQjtBQUN4Qix1QkFBTyxFQUFQO0FBQ0QsZUFaOEQ7QUFhL0RmLGtCQUFJLEVBQUVBLElBYnlEO0FBYy9EaUIscUJBQU8sRUFBRSxDQUFDbkIsS0FBRDtBQWRzRCxhQUE1RCxDQUFMO0FBZ0JBWSxhQUFDLElBQUksOEhBQUw7QUFDQUEsYUFBQyxJQUFJSixDQUFDLENBQUNhLEtBQUssQ0FBQ08sUUFBTixDQUFlRSxZQUFoQixFQUE4QlQsS0FBOUIsQ0FBTjtBQUNBVCxhQUFDLElBQUksMElBQUw7QUFDQUEsYUFBQyxJQUFJSixDQUFDLENBQUNhLEtBQUssQ0FBQ08sUUFBTixDQUFlRyxFQUFoQixFQUFvQlYsS0FBcEIsQ0FBTjtBQUNBVCxhQUFDLElBQUksMm9CQUFMO0FBQ0FBLGFBQUMsSUFBSWpCLGdCQUFnQixDQUFDNkIsSUFBakIsQ0FBc0JiLElBQXRCLENBQTJCVSxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDVyxVQUF4QyxFQUFvRDtBQUN2RGxCLGtCQUFJLEVBQUUsRUFEaUQ7QUFFdkRDLGtCQUFJLEVBQUVPLE1BQU0sSUFBSSxFQUZ1QztBQUd2RE4sZ0JBQUUsRUFBRSxZQUFVVSxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMzQixvQkFBSWYsQ0FBQyxHQUFHLEVBQVI7QUFDQUEsaUJBQUMsSUFBSSx1RkFBTDtBQUNBQSxpQkFBQyxJQUFJSixDQUFDLENBQUNrQixLQUFELEVBQVFBLEtBQVIsQ0FBTjtBQUNBZCxpQkFBQyxJQUFJLHlCQUFMO0FBQ0EsdUJBQU9BLENBQVA7QUFDRCxlQVRzRDtBQVV2RE0scUJBQU8sRUFBRSxTQUFTRCxLQUFULEdBQWlCO0FBQ3hCLHVCQUFPLEVBQVA7QUFDRCxlQVpzRDtBQWF2RGYsa0JBQUksRUFBRUEsSUFiaUQ7QUFjdkRpQixxQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBZDhDLGFBQXBELENBQUw7QUFnQkFZLGFBQUMsSUFBSSxhQUFMO0FBQ0EsbUJBQU9BLENBQVA7QUFDRCxXQWhFbUQ7QUFpRXBETSxpQkFBTyxFQUFFLFNBQVNELEtBQVQsR0FBaUI7QUFDeEIsbUJBQU8sRUFBUDtBQUNELFdBbkVtRDtBQW9FcERmLGNBQUksRUFBRUEsSUFwRThDO0FBcUVwRGlCLGlCQUFPLEVBQUUsQ0FBQ25CLEtBQUQ7QUFyRTJDLFNBQWpELENBQUw7QUF1RUFZLFNBQUMsSUFBSSxHQUFMO0FBQ0FBLFNBQUMsSUFBSWpCLGdCQUFnQixRQUFoQixDQUFzQmdCLElBQXRCLENBQTJCWCxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDaUMsTUFBeEMsRUFBZ0Q7QUFDbkRuQixjQUFJLEVBQUUsRUFENkM7QUFFbkRDLGNBQUksRUFBRWQsTUFBTSxJQUFJLEVBRm1DO0FBR25EZSxZQUFFLEVBQUUsWUFBVUssS0FBVixFQUFpQkMsTUFBakIsRUFBeUI7QUFDM0IsZ0JBQUlWLENBQUMsR0FBRyxFQUFSO0FBQ0FBLGFBQUMsSUFBSSwyT0FBTDtBQUNBQSxhQUFDLElBQUlKLENBQUMsQ0FBQ2EsS0FBSyxDQUFDYSxZQUFOLENBQW1CQyxHQUFwQixFQUF5QmQsS0FBekIsQ0FBTjtBQUNBVCxhQUFDLElBQUksK0tBQUw7QUFDQUEsYUFBQyxJQUFJSixDQUFDLENBQUNhLEtBQUssQ0FBQ2EsWUFBTixDQUFtQkUsS0FBcEIsRUFBMkJmLEtBQTNCLENBQU47QUFDQVQsYUFBQyxJQUFJLHlCQUFMO0FBQ0FBLGFBQUMsSUFBSWpCLGdCQUFnQixDQUFDNkIsSUFBakIsQ0FBc0JiLElBQXRCLENBQTJCVSxLQUEzQixFQUFrQ0EsS0FBSyxDQUFDZ0IsTUFBeEMsRUFBZ0Q7QUFDbkR2QixrQkFBSSxFQUFFLEVBRDZDO0FBRW5EQyxrQkFBSSxFQUFFTyxNQUFNLElBQUksRUFGbUM7QUFHbkROLGdCQUFFLEVBQUUsWUFBVVUsS0FBVixFQUFpQkMsTUFBakIsRUFBeUI7QUFDM0Isb0JBQUlmLENBQUMsR0FBRyxFQUFSO0FBQ0FBLGlCQUFDLElBQUksb0dBQUw7QUFDQUEsaUJBQUMsSUFBSUosQ0FBQyxDQUFDa0IsS0FBSyxDQUFDWSxJQUFQLEVBQWFaLEtBQWIsQ0FBTjtBQUNBZCxpQkFBQyxJQUFJLHNDQUFMO0FBQ0FBLGlCQUFDLElBQUlKLENBQUMsQ0FBQ2tCLEtBQUssQ0FBQ2EsTUFBUCxFQUFlYixLQUFmLENBQU47QUFDQWQsaUJBQUMsSUFBSSxtQ0FBTDtBQUNBQSxpQkFBQyxJQUFJSixDQUFDLENBQUNrQixLQUFLLENBQUNjLE9BQVAsRUFBZ0JkLEtBQWhCLENBQU47QUFDQWQsaUJBQUMsSUFBSSx5QkFBTDtBQUNBLHVCQUFPQSxDQUFQO0FBQ0QsZUFia0Q7QUFjbkRNLHFCQUFPLEVBQUUsU0FBU0QsS0FBVCxHQUFpQjtBQUN4Qix1QkFBTyxFQUFQO0FBQ0QsZUFoQmtEO0FBaUJuRGYsa0JBQUksRUFBRUEsSUFqQjZDO0FBa0JuRGlCLHFCQUFPLEVBQUUsQ0FBQ25CLEtBQUQ7QUFsQjBDLGFBQWhELENBQUw7QUFvQkFZLGFBQUMsSUFBSSxtQkFBTDtBQUNBLG1CQUFPQSxDQUFQO0FBQ0QsV0FoQ2tEO0FBaUNuRE0saUJBQU8sRUFBRSxTQUFTRCxLQUFULEdBQWlCO0FBQ3hCLG1CQUFPLEVBQVA7QUFDRCxXQW5Da0Q7QUFvQ25EZixjQUFJLEVBQUVBLElBcEM2QztBQXFDbkRpQixpQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBckMwQyxTQUFoRCxDQUFMO0FBdUNBWSxTQUFDLElBQUksMEJBQUw7QUFDQSxlQUFPQSxDQUFQO0FBQ0QsT0FsSk0sQ0FrSkwsSUFsSkssQ0FBUDtBQW1KRCxLLENBRUQ7Ozs7bUNBQ2U7QUFDYjtBQUNBLFdBQUs2QixZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBQXBCO0FBQ0Q7Ozs4QkFFUyxDQUFFOzs7b0NBRUksQ0FBRSxDLENBQUM7Ozs7aUNBR05DLEMsRUFBRztBQUNkLFdBQUtDLFVBQUwsQ0FBZ0JELENBQWhCLEVBQW1CLGdCQUFuQjtBQUNEOzs7O0VBcEsyQkUsb0U7O0FBd0s5QjlDLGVBQWUsQ0FBQ2dDLEVBQWhCLEdBQXFCLFlBQXJCO0FBQ0FoQyxlQUFlLENBQUMrQyxXQUFoQixHQUE4QixLQUE5QjtBQUNlL0MsOEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsTE07QUFDQSxJQUFNSixnQkFBZ0IsR0FBR0MsZ0RBQVMsQ0FBQ0MsT0FBbkMsQyxDQUlBOztDQUN1RDs7SUFFdkRFLGU7Ozs7Ozs7Ozs7Ozs7NkJBQ0s7QUFDUCxhQUFPLFVBQVVDLEtBQVYsRUFBaUJDLE1BQWpCLEVBQXlCQyxJQUF6QixFQUErQjtBQUNwQyxpQkFBU0MsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEIsaUJBQU9DLEtBQUssQ0FBQ0YsT0FBTixDQUFjQyxHQUFkLENBQVA7QUFDRDs7QUFFRCxpQkFBU0UsVUFBVCxDQUFvQkMsSUFBcEIsRUFBMEI7QUFDeEIsaUJBQU8sT0FBT0EsSUFBUCxLQUFnQixVQUF2QjtBQUNEOztBQUVELGlCQUFTQyxDQUFULENBQVdDLEdBQVgsRUFBZ0JDLEdBQWhCLEVBQXFCO0FBQ25CLGNBQUksT0FBT0QsR0FBUCxLQUFlLFdBQWYsSUFBOEJBLEdBQUcsS0FBSyxJQUExQyxFQUFnRDtBQUM5QyxnQkFBSUgsVUFBVSxDQUFDRyxHQUFELENBQWQsRUFBcUI7QUFDbkIscUJBQU9BLEdBQUcsQ0FBQ0UsSUFBSixDQUFTRCxHQUFULENBQVA7QUFDRCxhQUZELE1BRU8sT0FBT0QsR0FBUDtBQUNSLFdBSkQsTUFJTyxPQUFPLEVBQVA7QUFDUjs7QUFFRFAsWUFBSSxHQUFHQSxJQUFJLElBQUlGLEtBQVIsSUFBaUIsRUFBeEI7QUFDQSxZQUFJWSxDQUFDLEdBQUcsRUFBUjtBQUNBQSxTQUFDLElBQUksc1JBQUw7QUFDQUEsU0FBQyxJQUFJakIsZ0JBQWdCLENBQUM2QixJQUFqQixDQUFzQmIsSUFBdEIsQ0FBMkJYLEtBQTNCLEVBQWtDQSxLQUFLLENBQUMrQyxRQUF4QyxFQUFrRDtBQUNyRGpDLGNBQUksRUFBRSxFQUQrQztBQUVyREMsY0FBSSxFQUFFZCxNQUFNLElBQUksRUFGcUM7QUFHckRlLFlBQUUsRUFBRSxZQUFVSyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMzQixnQkFBSVYsQ0FBQyxHQUFHLEVBQVI7QUFDQUEsYUFBQyxJQUFJLGlEQUFMO0FBQ0FBLGFBQUMsSUFBSWpCLGdCQUFnQixDQUFDcUQsRUFBakIsQ0FBb0JyQyxJQUFwQixDQUF5QlUsS0FBekIsRUFBZ0MsWUFBaEMsRUFBOEM7QUFDakRQLGtCQUFJLEVBQUUsRUFEMkM7QUFFakRDLGtCQUFJLEVBQUVPLE1BQU0sSUFBSSxFQUZpQztBQUdqRE4sZ0JBQUUsRUFBRSxTQUFTQyxLQUFULEdBQWlCO0FBQ25CLHVCQUFPLEVBQVA7QUFDRCxlQUxnRDtBQU1qREMscUJBQU8sRUFBRSxTQUFTRCxLQUFULEdBQWlCO0FBQ3hCLHVCQUFPLEVBQVA7QUFDRCxlQVJnRDtBQVNqRGYsa0JBQUksRUFBRUEsSUFUMkM7QUFVakRpQixxQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBVndDLGFBQTlDLENBQUw7QUFZQVksYUFBQyxJQUFJLHlCQUFMO0FBQ0FBLGFBQUMsSUFBSUosQ0FBQyxDQUFDYSxLQUFELEVBQVFBLEtBQVIsQ0FBTjtBQUNBVCxhQUFDLElBQUksY0FBTDtBQUNBLG1CQUFPQSxDQUFQO0FBQ0QsV0F0Qm9EO0FBdUJyRE0saUJBQU8sRUFBRSxTQUFTRCxLQUFULEdBQWlCO0FBQ3hCLG1CQUFPLEVBQVA7QUFDRCxXQXpCb0Q7QUEwQnJEZixjQUFJLEVBQUVBLElBMUIrQztBQTJCckRpQixpQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBM0I0QyxTQUFsRCxDQUFMO0FBNkJBWSxTQUFDLElBQUksb0RBQUw7QUFDQUEsU0FBQyxJQUFJakIsZ0JBQWdCLENBQUM2QixJQUFqQixDQUFzQmIsSUFBdEIsQ0FBMkJYLEtBQTNCLEVBQWtDQSxLQUFLLENBQUMrQyxRQUF4QyxFQUFrRDtBQUNyRGpDLGNBQUksRUFBRSxFQUQrQztBQUVyREMsY0FBSSxFQUFFZCxNQUFNLElBQUksRUFGcUM7QUFHckRlLFlBQUUsRUFBRSxZQUFVSyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMzQixnQkFBSVYsQ0FBQyxHQUFHLEVBQVI7QUFDQUEsYUFBQyxJQUFJLEdBQUw7QUFDQUEsYUFBQyxJQUFJSixDQUFDLENBQUNhLEtBQUQsRUFBUUEsS0FBUixDQUFOO0FBQ0FULGFBQUMsSUFBSSxHQUFMO0FBQ0EsbUJBQU9BLENBQVA7QUFDRCxXQVRvRDtBQVVyRE0saUJBQU8sRUFBRSxTQUFTRCxLQUFULEdBQWlCO0FBQ3hCLG1CQUFPLEVBQVA7QUFDRCxXQVpvRDtBQWFyRGYsY0FBSSxFQUFFQSxJQWIrQztBQWNyRGlCLGlCQUFPLEVBQUUsQ0FBQ25CLEtBQUQ7QUFkNEMsU0FBbEQsQ0FBTDtBQWdCQVksU0FBQyxJQUFJLDJuQkFBTDtBQUNBLGVBQU9BLENBQVA7QUFDRCxPQXBFTSxDQW9FTCxJQXBFSyxDQUFQO0FBcUVELEssQ0FFRDs7OzttQ0FDZSxDQUFFOzs7OEJBRVAsQ0FBRTs7O29DQUVJLENBQUU7Ozs7RUE5RVVpQyxvRTs7QUFrRjlCOUMsZUFBZSxDQUFDZ0MsRUFBaEIsR0FBcUIsWUFBckI7QUFDQWhDLGVBQWUsQ0FBQytDLFdBQWhCLEdBQThCLEtBQTlCO0FBQ2UvQyw4RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3RkE7Q0FDNkQ7O0lBRXZEQSxlOzs7Ozs7Ozs7Ozs7OzZCQUNLO0FBQ1AsYUFBTyxVQUFVQyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QkMsSUFBekIsRUFBK0I7QUFDcEMsaUJBQVNDLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3BCLGlCQUFPQyxLQUFLLENBQUNGLE9BQU4sQ0FBY0MsR0FBZCxDQUFQO0FBQ0Q7O0FBRUQsaUJBQVNFLFVBQVQsQ0FBb0JDLElBQXBCLEVBQTBCO0FBQ3hCLGlCQUFPLE9BQU9BLElBQVAsS0FBZ0IsVUFBdkI7QUFDRDs7QUFFRCxpQkFBU0MsQ0FBVCxDQUFXQyxHQUFYLEVBQWdCQyxHQUFoQixFQUFxQjtBQUNuQixjQUFJLE9BQU9ELEdBQVAsS0FBZSxXQUFmLElBQThCQSxHQUFHLEtBQUssSUFBMUMsRUFBZ0Q7QUFDOUMsZ0JBQUlILFVBQVUsQ0FBQ0csR0FBRCxDQUFkLEVBQXFCO0FBQ25CLHFCQUFPQSxHQUFHLENBQUNFLElBQUosQ0FBU0QsR0FBVCxDQUFQO0FBQ0QsYUFGRCxNQUVPLE9BQU9ELEdBQVA7QUFDUixXQUpELE1BSU8sT0FBTyxFQUFQO0FBQ1I7O0FBRURQLFlBQUksR0FBR0EsSUFBSSxJQUFJRixLQUFSLElBQWlCLEVBQXhCO0FBQ0EsWUFBSVksQ0FBQyxHQUFHLEVBQVI7QUFDQUEsU0FBQyxJQUFJLDhrQ0FBTDtBQUNBLGVBQU9BLENBQVA7QUFDRCxPQXJCTSxDQXFCTCxJQXJCSyxDQUFQO0FBc0JELEssQ0FFRDs7OzttQ0FDZTtBQUNiO0FBQ0EsV0FBS3FDLFdBQUwsR0FBbUIsS0FBS0EsV0FBTCxDQUFpQlAsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbkI7QUFDRDs7OzhCQUVTO0FBQ1IsVUFBTVEsSUFBSSxHQUFHLElBQWI7O0FBRUEsZUFBU0MseUJBQVQsQ0FBbUNSLENBQW5DLEVBQXNDO0FBQ3BDLFlBQUlTLFFBQVEsR0FBR0YsSUFBSSxDQUFDRyxFQUFMLENBQVEsY0FBUixFQUF3QjVDLEdBQXhCLEVBQWY7QUFDQSxZQUFJNkMsZUFBZSxHQUFHSixJQUFJLENBQUNHLEVBQUwsQ0FBUSxrQkFBUixFQUE0QjVDLEdBQTVCLEVBQXRCO0FBQ0F5QyxZQUFJLENBQUNHLEVBQUwsQ0FBUSxjQUFSLEVBQXdCLENBQXhCLEVBQTJCRSxpQkFBM0IsQ0FBNkMsRUFBN0M7O0FBRUEsWUFBSUgsUUFBUSxJQUFJRSxlQUFoQixFQUFpQztBQUMvQkosY0FBSSxDQUFDRyxFQUFMLENBQVEsY0FBUixFQUF3QixDQUF4QixFQUEyQkUsaUJBQTNCLENBQTZDLHNCQUE3QztBQUNEO0FBQ0Y7O0FBRUQsV0FBS0YsRUFBTCxDQUFRLGNBQVIsRUFBd0JHLEVBQXhCLENBQTJCLGFBQTNCLEVBQTBDTCx5QkFBMUM7QUFDQSxXQUFLRSxFQUFMLENBQVEsa0JBQVIsRUFBNEJHLEVBQTVCLENBQStCLGFBQS9CLEVBQThDTCx5QkFBOUM7QUFDRDs7O29DQUVlLENBQUUsQyxDQUFDOzs7O2dDQUdQUixDLEVBQUc7QUFDYixXQUFLQyxVQUFMLENBQWdCRCxDQUFoQixFQUFtQixxQkFBbkI7QUFDRDs7OztFQXREMkJFLG9FOztBQTBEOUI5QyxlQUFlLENBQUNnQyxFQUFoQixHQUFxQixZQUFyQjtBQUNBaEMsZUFBZSxDQUFDK0MsV0FBaEIsR0FBOEIsS0FBOUI7QUFDZS9DLDhFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9EQTtDQUM2RDs7SUFFdkRBLGU7Ozs7Ozs7Ozs7Ozs7NkJBQ0s7QUFDUCxhQUFPLFVBQVVDLEtBQVYsRUFBaUJDLE1BQWpCLEVBQXlCQyxJQUF6QixFQUErQjtBQUNwQyxpQkFBU0MsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEIsaUJBQU9DLEtBQUssQ0FBQ0YsT0FBTixDQUFjQyxHQUFkLENBQVA7QUFDRDs7QUFFRCxpQkFBU0UsVUFBVCxDQUFvQkMsSUFBcEIsRUFBMEI7QUFDeEIsaUJBQU8sT0FBT0EsSUFBUCxLQUFnQixVQUF2QjtBQUNEOztBQUVELGlCQUFTQyxDQUFULENBQVdDLEdBQVgsRUFBZ0JDLEdBQWhCLEVBQXFCO0FBQ25CLGNBQUksT0FBT0QsR0FBUCxLQUFlLFdBQWYsSUFBOEJBLEdBQUcsS0FBSyxJQUExQyxFQUFnRDtBQUM5QyxnQkFBSUgsVUFBVSxDQUFDRyxHQUFELENBQWQsRUFBcUI7QUFDbkIscUJBQU9BLEdBQUcsQ0FBQ0UsSUFBSixDQUFTRCxHQUFULENBQVA7QUFDRCxhQUZELE1BRU8sT0FBT0QsR0FBUDtBQUNSLFdBSkQsTUFJTyxPQUFPLEVBQVA7QUFDUjs7QUFFRFAsWUFBSSxHQUFHQSxJQUFJLElBQUlGLEtBQVIsSUFBaUIsRUFBeEI7QUFDQSxZQUFJWSxDQUFDLEdBQUcsRUFBUjtBQUNBQSxTQUFDLElBQUksdXhDQUFMO0FBQ0EsZUFBT0EsQ0FBUDtBQUNELE9BckJNLENBcUJMLElBckJLLENBQVA7QUFzQkQsSyxDQUVEOzs7O21DQUNlO0FBQ2IsV0FBSzZDLFlBQUwsR0FBb0IsS0FBS0EsWUFBTCxDQUFrQmYsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEI7QUFDRDs7OzhCQUVTO0FBQ1IsVUFBTVEsSUFBSSxHQUFHLElBQWI7O0FBRUEsZUFBU0MseUJBQVQsQ0FBbUNSLENBQW5DLEVBQXNDO0FBQ3BDLFlBQUlTLFFBQVEsR0FBR0YsSUFBSSxDQUFDRyxFQUFMLENBQVEsY0FBUixFQUF3QjVDLEdBQXhCLEVBQWY7QUFDQSxZQUFJNkMsZUFBZSxHQUFHSixJQUFJLENBQUNHLEVBQUwsQ0FBUSxrQkFBUixFQUE0QjVDLEdBQTVCLEVBQXRCO0FBQ0F5QyxZQUFJLENBQUNHLEVBQUwsQ0FBUSxjQUFSLEVBQXdCLENBQXhCLEVBQTJCRSxpQkFBM0IsQ0FBNkMsRUFBN0M7O0FBRUEsWUFBSUgsUUFBUSxJQUFJRSxlQUFoQixFQUFpQztBQUMvQkosY0FBSSxDQUFDRyxFQUFMLENBQVEsY0FBUixFQUF3QixDQUF4QixFQUEyQkUsaUJBQTNCLENBQTZDLHNCQUE3QztBQUNEO0FBQ0Y7O0FBRUQsV0FBS0YsRUFBTCxDQUFRLGNBQVIsRUFBd0JHLEVBQXhCLENBQTJCLGFBQTNCLEVBQTBDTCx5QkFBMUM7QUFDQSxXQUFLRSxFQUFMLENBQVEsa0JBQVIsRUFBNEJHLEVBQTVCLENBQStCLGFBQS9CLEVBQThDTCx5QkFBOUM7QUFDRCxLLENBQUM7Ozs7aUNBR1dSLEMsRUFBRztBQUNkLFdBQUtDLFVBQUwsQ0FBZ0JELENBQWhCLEVBQW1CLHVCQUFuQjtBQUNEOzs7O0VBbkQyQkUsb0U7O0FBdUQ5QjlDLGVBQWUsQ0FBQ2dDLEVBQWhCLEdBQXFCLFlBQXJCO0FBQ0FoQyxlQUFlLENBQUMrQyxXQUFoQixHQUE4QixLQUE5QjtBQUNlL0MsOEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzRE07QUFDQSxJQUFNSixnQkFBZ0IsR0FBR0MsZ0RBQVMsQ0FBQ0MsT0FBbkMsQyxDQUlBOztDQUN1RDs7SUFFdkRFLGU7Ozs7Ozs7Ozs7Ozs7NkJBQ0s7QUFDUCxhQUFPLFVBQVVDLEtBQVYsRUFBaUJDLE1BQWpCLEVBQXlCQyxJQUF6QixFQUErQjtBQUNwQyxpQkFBU0MsT0FBVCxDQUFpQkMsR0FBakIsRUFBc0I7QUFDcEIsaUJBQU9DLEtBQUssQ0FBQ0YsT0FBTixDQUFjQyxHQUFkLENBQVA7QUFDRDs7QUFFRCxpQkFBU0UsVUFBVCxDQUFvQkMsSUFBcEIsRUFBMEI7QUFDeEIsaUJBQU8sT0FBT0EsSUFBUCxLQUFnQixVQUF2QjtBQUNEOztBQUVELGlCQUFTQyxDQUFULENBQVdDLEdBQVgsRUFBZ0JDLEdBQWhCLEVBQXFCO0FBQ25CLGNBQUksT0FBT0QsR0FBUCxLQUFlLFdBQWYsSUFBOEJBLEdBQUcsS0FBSyxJQUExQyxFQUFnRDtBQUM5QyxnQkFBSUgsVUFBVSxDQUFDRyxHQUFELENBQWQsRUFBcUI7QUFDbkIscUJBQU9BLEdBQUcsQ0FBQ0UsSUFBSixDQUFTRCxHQUFULENBQVA7QUFDRCxhQUZELE1BRU8sT0FBT0QsR0FBUDtBQUNSLFdBSkQsTUFJTyxPQUFPLEVBQVA7QUFDUjs7QUFFRFAsWUFBSSxHQUFHQSxJQUFJLElBQUlGLEtBQVIsSUFBaUIsRUFBeEI7QUFDQSxZQUFJWSxDQUFDLEdBQUcsRUFBUjtBQUNBQSxTQUFDLElBQUkscUpBQUw7QUFDQUEsU0FBQyxJQUFJSixDQUFDLENBQUNSLEtBQUssQ0FBQzBELE9BQVAsRUFBZ0IxRCxLQUFoQixDQUFOO0FBQ0FZLFNBQUMsSUFBSSxnbEJBQUw7QUFDQUEsU0FBQyxJQUFJakIsZ0JBQWdCLE1BQWhCLENBQW9CZ0IsSUFBcEIsQ0FBeUJYLEtBQXpCLEVBQWdDQSxLQUFLLENBQUMyRCxVQUF0QyxFQUFrRDtBQUNyRDdDLGNBQUksRUFBRSxFQUQrQztBQUVyREMsY0FBSSxFQUFFZCxNQUFNLElBQUksRUFGcUM7QUFHckRlLFlBQUUsRUFBRSxZQUFVSyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QjtBQUMzQixnQkFBSVYsQ0FBQyxHQUFHLEVBQVI7QUFDQUEsYUFBQyxJQUFJLGdoQkFBTDtBQUNBLG1CQUFPQSxDQUFQO0FBQ0QsV0FQb0Q7QUFRckRNLGlCQUFPLEVBQUUsU0FBU0QsS0FBVCxHQUFpQjtBQUN4QixtQkFBTyxFQUFQO0FBQ0QsV0FWb0Q7QUFXckRmLGNBQUksRUFBRUEsSUFYK0M7QUFZckRpQixpQkFBTyxFQUFFLENBQUNuQixLQUFEO0FBWjRDLFNBQWxELENBQUw7QUFjQVksU0FBQyxJQUFJLDBCQUFMO0FBQ0EsZUFBT0EsQ0FBUDtBQUNELE9BdENNLENBc0NMLElBdENLLENBQVA7QUF1Q0QsSyxDQUVEOzs7O21DQUNlO0FBQ2I7QUFDQSxXQUFLZ0QsV0FBTCxHQUFtQixLQUFLQSxXQUFMLENBQWlCbEIsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbkI7QUFDRDs7OzhCQUVTLENBQUU7OztvQ0FFSSxDQUFFLEMsQ0FBQzs7OztnQ0FHUEMsQyxFQUFHO0FBQ2IsV0FBS0MsVUFBTCxDQUFnQkQsQ0FBaEIsRUFBbUIscUJBQW5CO0FBQ0Q7Ozs7RUF4RDJCRSxvRTs7QUE0RDlCOUMsZUFBZSxDQUFDZ0MsRUFBaEIsR0FBcUIsWUFBckI7QUFDQWhDLGVBQWUsQ0FBQytDLFdBQWhCLEdBQThCLEtBQTlCO0FBQ2UvQyw4RUFBZixFOzs7Ozs7Ozs7Ozs7QUN2RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNrRDtBQUNVO0FBQ1Q7QUFDSjtBQUN5QjtBQUN0QjtBQUNVO0FBQ0Y7QUFDMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQiwwREFBUztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUyxFQUFFLHlFQUFxQjtBQUNoQztBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUyxFQUFFLHlFQUFxQjtBQUNoQztBQUNBO0FBQ0E7QUFDQSxJQUFJLHNFQUFRO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksc0VBQVE7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxzRUFBVTtBQUNkLHFDQUFxQyx1RUFBYztBQUNuRCxRQUFRLDZEQUFTO0FBQ2pCLFFBQVEsdUVBQWM7QUFDdEIsUUFBUSxxRUFBYTtBQUNyQjtBQUM2Qjs7Ozs7Ozs7Ozs7OztBQ2xFN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSw0Q0FBNEMsUUFBUTtBQUNwRDtBQUNBO0FBQ0Esa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSxlQUFlLFNBQUksSUFBSSxTQUFJO0FBQzNCLG1DQUFtQyxvQ0FBb0M7QUFDdkU7QUFDbUM7QUFDdUI7QUFDUjtBQUNIO0FBQ3FCO0FBQ2E7QUFDRjtBQUNlO0FBQzNDO0FBQ0Q7QUFDVTtBQUNsQztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsMENBQVE7QUFDdEI7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QiwwQ0FBUTtBQUNqQztBQUNBLFNBQVMsRUFBRSwwRUFBZ0I7QUFDM0I7QUFDQTtBQUNBLG1CQUFtQiwwREFBUztBQUM1QixTQUFTLEVBQUUsZ0ZBQXFCO0FBQ2hDO0FBQ0E7QUFDQSxtQkFBbUIsMERBQVM7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0EsU0FBUyxFQUFFLHdGQUE0QjtBQUN2QztBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLG1CQUFtQiwwREFBUztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0EsU0FBUyxFQUFFLGlGQUFzQjtBQUNqQztBQUNBO0FBQ0EsbUJBQW1CLDBEQUFTO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxJQUFJLHNFQUFRO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksc0VBQVE7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxzRUFBUTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLHNFQUFRO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksc0VBQVE7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxzRUFBUTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLHNFQUFRO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksc0VBQVE7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxzRUFBVTtBQUNkLGVBQWUsd0RBQU07QUFDckIsZUFBZSx3REFBTTtBQUNyQixxQ0FBcUMscUVBQWE7QUFDbEQsUUFBUSw2REFBUztBQUNqQixRQUFRLHdFQUFjO0FBQ3RCO0FBQzRCOzs7Ozs7Ozs7Ozs7O0FDeks1QjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTtBQUNBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ3VDO0FBQ3ZDLGdCQUFnQixtQkFBTyxDQUFDLHdCQUFTO0FBQ2pDLGdCQUFnQixtQkFBTyxDQUFDLDBCQUFVO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksNERBQVU7QUFDZDtBQUNBO0FBQ3FCOzs7Ozs7Ozs7Ozs7O0FDcENyQjtBQUFBO0FBQUE7QUFBdUM7QUFDdkM7QUFDQSxnQ0FBZ0MsNERBQVU7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDZSx5RUFBVSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDUDFCO0FBQUE7QUFBQTtBQUF1QztBQUN2QyxzQkFBc0I7QUFDdEIsZ0NBQWdDLDREQUFVO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ2Usa0VBQUcsRUFBQzs7Ozs7Ozs7Ozs7OztBQ1BuQjtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ29COzs7Ozs7Ozs7Ozs7O0FDWHBCO0FBQUE7QUFBQTtBQUF1QztBQUN2QywwQkFBMEI7QUFDMUIsZ0NBQWdDLDREQUFVO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ2Usc0VBQU8sRUFBQzs7Ozs7Ozs7Ozs7OztBQ1B2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMEI7QUFDb0I7QUFDSTtBQUNLO0FBQ2hCO0FBQ1U7QUFDeUI7QUFDakI7QUFDQTtBQUNYO0FBQ087QUFDaUM7QUFDcEI7QUFDUDtBQUNWO0FBQ047QUFDTztBQUNmO0FBQ3FCO0FBQ0Q7QUFDVztBQUNzTztBQUN6UixrSEFBUSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDdEJ4QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpRDtBQUNRO0FBQ0k7QUFDSjtBQUNBO0FBQ0Y7QUFDRjtBQUNSO0FBQ3FCO0FBQ1A7QUFDSjtBQUNhO0FBQ1Q7QUFDQTtBQUNwRDtBQUNQO0FBQ0EsMkJBQTJCLG1CQUFPLENBQUMsd0VBQWlDO0FBQ3BFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEO0FBQ0EsbUJBQW1CLHlFQUFlO0FBQ2xDLG1CQUFtQix5REFBUztBQUM1QixtQkFBbUIsNkRBQVM7QUFDNUIsbUJBQW1CLG9FQUFZO0FBQy9CLG1CQUFtQixxRUFBYTtBQUNoQyxtQkFBbUIscUVBQWE7QUFDaEMsbUJBQW1CLG1FQUFZO0FBQy9CLG1CQUFtQixpRUFBVztBQUM5QixtQkFBbUIscUVBQWE7QUFDaEMsbUJBQW1CLDhFQUFnQjtBQUNuQyxtQkFBbUIsd0VBQWM7QUFDakMsbUJBQW1CLGlGQUFpQjtBQUNwQyxtQkFBbUIsdUVBQWM7QUFDakMsbUJBQW1CLHdFQUFjO0FBQ2pDO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUM3REE7QUFBQTtBQUFBO0FBQUEsa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSw0Q0FBNEMsUUFBUTtBQUNwRDtBQUNBO0FBQzRDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1FQUFPO0FBQ1g7QUFDMEI7Ozs7Ozs7Ozs7Ozs7QUN4RDFCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSw0Q0FBNEMsUUFBUTtBQUNwRDtBQUNBO0FBQ0Esa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDNkM7QUFDRDtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1FQUFPO0FBQ1gscUNBQXFDLHlEQUFXO0FBQ2hEO0FBQzBCOzs7Ozs7Ozs7Ozs7O0FDekMxQjtBQUFBO0FBQUE7QUFDQTtBQUNrQzs7Ozs7Ozs7Ozs7OztBQ0ZsQztBQUFBO0FBQUE7QUFBQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUM0QztBQUM1Qyw0QkFBNEIsbUJBQU8sQ0FBQyw0SEFBMkQ7QUFDL0YsbUJBQW1CLG1CQUFPLENBQUMsOERBQTRCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1FQUFPO0FBQ1g7QUFDQTtBQUMyQjs7Ozs7Ozs7Ozs7OztBQ3RDM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTtBQUNBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsZUFBZSxTQUFJLElBQUksU0FBSTtBQUMzQixtQ0FBbUMsb0NBQW9DO0FBQ3ZFO0FBQ21DO0FBQ1M7QUFDNUMsYUFBYSxtQkFBTyxDQUFDLGtCQUFNO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxtRUFBTztBQUNYLGVBQWUsd0RBQU07QUFDckI7QUFDQTtBQUN1Qjs7Ozs7Ozs7Ozs7OztBQ2hDdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNBLGVBQWUsU0FBSSxJQUFJLFNBQUk7QUFDM0IsbUNBQW1DLG9DQUFvQztBQUN2RTtBQUNtQztBQUNrQjtBQUNSO0FBQ0Q7QUFDNUMsY0FBYyxtQkFBTyxDQUFDLDBCQUFVO0FBQ2hDLGVBQWUsbUJBQU8sQ0FBQyw0Q0FBbUI7QUFDMUMsZUFBZSxtQkFBTyxDQUFDLDRDQUFtQjtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1FQUFPO0FBQ1gsZUFBZSx3REFBTTtBQUNyQixxQ0FBcUMseURBQVc7QUFDaEQsUUFBUSxpRUFBZTtBQUN2QjtBQUN3Qjs7Ozs7Ozs7Ozs7OztBQzdDeEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQSxrQkFBa0IsU0FBSSxJQUFJLFNBQUk7QUFDOUI7QUFDQTtBQUM0QztBQUM1QztBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG1FQUFPO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDMEM7Ozs7Ozs7Ozs7Ozs7QUNoQzFDO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSw0Q0FBNEMsUUFBUTtBQUNwRDtBQUNBO0FBQ0Esa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDNEM7QUFDNUM7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0EsSUFBSSxtRUFBTztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNtQzs7Ozs7Ozs7Ozs7OztBQ3RGbkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSw0Q0FBNEMsUUFBUTtBQUNwRDtBQUNBO0FBQ0Esa0JBQWtCLFNBQUksSUFBSSxTQUFJO0FBQzlCO0FBQ0E7QUFDQSxlQUFlLFNBQUksSUFBSSxTQUFJO0FBQzNCLG1DQUFtQyxvQ0FBb0M7QUFDdkU7QUFDbUM7QUFDTTtBQUNHO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixTQUFTLGtCQUFrQiwrQkFBK0I7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0MsU0FBUztBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQyxhQUFhO0FBQ3ZEO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksbUVBQU87QUFDWCxlQUFlLHdEQUFNO0FBQ3JCLHFDQUFxQyxxREFBUztBQUM5QztBQUMwQjs7Ozs7Ozs7Ozs7OztBQzNHMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTtBQUNBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQytDO0FBQ0Y7QUFDRDtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLFdBQVcsR0FBRywyQkFBMkI7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLE1BQU07QUFDN0M7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLE1BQU07QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksbUVBQU87QUFDWCxxQ0FBcUMseURBQVc7QUFDaEQsUUFBUSwyREFBWTtBQUNwQjtBQUN5Qjs7Ozs7Ozs7Ozs7OztBQ25JekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTtBQUNBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsZUFBZSxTQUFJLElBQUksU0FBSTtBQUMzQixtQ0FBbUMsb0NBQW9DO0FBQ3ZFO0FBQ21DO0FBQ1M7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxtRUFBTztBQUNYLGVBQWUsd0RBQU07QUFDckI7QUFDQTtBQUNxQjs7Ozs7Ozs7Ozs7OztBQzFDckI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTtBQUNBLGtCQUFrQixTQUFJLElBQUksU0FBSTtBQUM5QjtBQUNBO0FBQ0EsT0FBTyw0QkFBNEIsR0FBRyxtQkFBTyxDQUFDLHNCQUFRO0FBQ1I7QUFDRjtBQUNIO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksbUVBQU87QUFDWCxxQ0FBcUMseURBQVM7QUFDOUMsUUFBUSxxREFBUztBQUNqQjtBQUN5Qjs7Ozs7Ozs7Ozs7OztBQ3RGekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUQ7QUFDUTtBQUNJO0FBQ0o7QUFDbkI7QUFDbUI7QUFDRjtBQUNGO0FBQ2lDO0FBQ2xDO0FBQ087QUFDSjtBQUNJO0FBQzNEO0FBQ0EsdUJBQXVCLG1EQUFZO0FBQzVCO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQix3RUFBYztBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLGtHQUFzQjtBQUM1QztBQUNBO0FBQ0Esb0NBQW9DLFFBQVEsZUFBZSxjQUFjO0FBQ3pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLGlFQUFXO0FBQzdDO0FBQ0E7QUFDQSxrQ0FBa0MsbUVBQVk7QUFDOUM7QUFDQTtBQUNBLGtDQUFrQyxvRUFBWTtBQUM5QztBQUNBO0FBQ0Esa0NBQWtDLHFFQUFhO0FBQy9DO0FBQ0E7QUFDQSxrQ0FBa0MsNkRBQVM7QUFDM0M7QUFDQTtBQUNBLGtDQUFrQyx5RUFBZTtBQUNqRDtBQUNBO0FBQ0Esa0NBQWtDLHFFQUFhO0FBQy9DO0FBQ0E7QUFDQSxrQ0FBa0MscUVBQWE7QUFDL0M7QUFDQTtBQUNBLGtDQUFrQyx3RUFBYztBQUNoRDtBQUNBO0FBQ0Esa0NBQWtDLHdFQUFjO0FBQ2hEO0FBQ0EsQ0FBQyw0QkFBNEI7Ozs7Ozs7Ozs7Ozs7QUMvSDdCO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDcUI7Ozs7Ozs7Ozs7Ozs7QUNOckI7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUN1Qjs7Ozs7Ozs7Ozs7OztBQ1R2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBdUM7QUFDQztBQUNvQjtBQUM1RCw2QkFBNkIsb0RBQVM7QUFDdEM7QUFDQSxlQUFlLG1EQUFRO0FBQ3ZCO0FBQ0E7QUFDQSxlQUFlLG1EQUFRLG9CQUFvQix1RUFBYztBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsbURBQVE7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUMwQjs7Ozs7Ozs7Ozs7OztBQ2xDMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTBCO0FBQ1k7QUFDb0I7QUFDRTtBQUNNO0FBQ0Y7QUFDRjtBQUM5RDtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxtQkFBbUIsNkVBQWU7QUFDbEMsbUJBQW1CLHVFQUFZO0FBQy9CLG1CQUFtQixxRUFBVztBQUM5QixtQkFBbUIseUVBQWE7QUFDaEMsbUJBQW1CLDJFQUFjO0FBQ2pDLG9DQUFvQyxxRUFBVztBQUMvQyxxQ0FBcUMsdUVBQVk7QUFDakQsc0NBQXNDLHlFQUFhO0FBQ25ELHVDQUF1QywyRUFBYztBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBLG9CQUFvQixtREFBUztBQUM3QjtBQUNBOzs7Ozs7Ozs7Ozs7QUN6Q0EsbUM7Ozs7Ozs7Ozs7O0FDQUEsbUM7Ozs7Ozs7Ozs7O0FDQUEsdUM7Ozs7Ozs7Ozs7O0FDQUEsNEQ7Ozs7Ozs7Ozs7O0FDQUEsc0M7Ozs7Ozs7Ozs7O0FDQUEsaUM7Ozs7Ozs7Ozs7O0FDQUEscUM7Ozs7Ozs7Ozs7O0FDQUEsb0M7Ozs7Ozs7Ozs7O0FDQUEscUM7Ozs7Ozs7Ozs7O0FDQUEsdUQ7Ozs7Ozs7Ozs7O0FDQUEsc0Y7Ozs7Ozs7Ozs7O0FDQUEsOEM7Ozs7Ozs7Ozs7O0FDQUEsOEM7Ozs7Ozs7Ozs7O0FDQUEsNkM7Ozs7Ozs7Ozs7O0FDQUEsc0MiLCJmaWxlIjoiaW5kZXgtbm9kZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSB7XG5cdFx0dmFyIGEgPSBmYWN0b3J5KCk7XG5cdFx0Zm9yKHZhciBpIGluIGEpICh0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgPyBleHBvcnRzIDogcm9vdClbaV0gPSBhW2ldO1xuXHR9XG59KShnbG9iYWwsIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LnRzXCIpO1xuIiwiXG4gICAgICBpbXBvcnQgVGVtcGxhdGU3IGZyb20gJ3RlbXBsYXRlNyc7XG4gICAgICBjb25zdCBUZW1wbGF0ZTdIZWxwZXJzID0gVGVtcGxhdGU3LmhlbHBlcnM7XG4gIFxuICAgICAgVGVtcGxhdGU3LnJlZ2lzdGVyUGFydGlhbCgnYmFycycsICc8YSBocmVmPVwiI1wiIGNsYXNzPVwibGluayBpY29uLW9ubHkgcGFuZWwtb3BlbiBzbWFsbC1vbmx5XCIgZGF0YS1wYW5lbD1cIi5wYW5lbC1sZWZ0XCI+PGkgY2xhc3M9XCJpY29uIGY3LWljb25zXCI+YmFyczwvaT48L2E+Jyk7XG4gIFxuICAgICAgLy8gZmlyc3Qgd2UgaW1wb3J0IHN1cGVyIGNsYXNzXG5pbXBvcnQgeyBTcGFjZUNvbXBvbmVudCB9IGZyb20gJy4uLy4uL3V0aWwvc3BhY2UtY29tcG9uZW50JzsgLy8gd2UgbmVlZCB0byBleHBvcnQgZXh0ZW5kZWQgY2xhc3NcblxuY2xhc3MgRjdQYWdlQ29tcG9uZW50IGV4dGVuZHMgU3BhY2VDb21wb25lbnQge1xuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjdHhfMSwgZGF0YV8xLCByb290KSB7XG4gICAgICBmdW5jdGlvbiBpc0FycmF5KGFycikge1xuICAgICAgICByZXR1cm4gQXJyYXkuaXNBcnJheShhcnIpO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBpc0Z1bmN0aW9uKGZ1bmMpIHtcbiAgICAgICAgcmV0dXJuIHR5cGVvZiBmdW5jID09PSAnZnVuY3Rpb24nO1xuICAgICAgfVxuXG4gICAgICBmdW5jdGlvbiBjKHZhbCwgY3R4KSB7XG4gICAgICAgIGlmICh0eXBlb2YgdmFsICE9PSBcInVuZGVmaW5lZFwiICYmIHZhbCAhPT0gbnVsbCkge1xuICAgICAgICAgIGlmIChpc0Z1bmN0aW9uKHZhbCkpIHtcbiAgICAgICAgICAgIHJldHVybiB2YWwuY2FsbChjdHgpO1xuICAgICAgICAgIH0gZWxzZSByZXR1cm4gdmFsO1xuICAgICAgICB9IGVsc2UgcmV0dXJuIFwiXCI7XG4gICAgICB9XG5cbiAgICAgIHJvb3QgPSByb290IHx8IGN0eF8xIHx8IHt9O1xuICAgICAgdmFyIHIgPSAnJztcbiAgICAgIHIgKz0gJzxkaXYgY2xhc3M9cGFnZSBkYXRhLW5hbWU9Y29ubmVjdD48ZGl2IGNsYXNzPW5hdmJhcj48ZGl2IGNsYXNzPW5hdmJhci1iZz48L2Rpdj48ZGl2IGNsYXNzPW5hdmJhci1pbm5lcj48ZGl2IGNsYXNzPWxlZnQ+JztcbiAgICAgIHIgKz0gVGVtcGxhdGU3SGVscGVycy5fcGFydGlhbC5jYWxsKGN0eF8xLCBcImJhcnNcIiwge1xuICAgICAgICBoYXNoOiB7fSxcbiAgICAgICAgZGF0YTogZGF0YV8xIHx8IHt9LFxuICAgICAgICBmbjogZnVuY3Rpb24gZW1wdHkoKSB7XG4gICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9LFxuICAgICAgICBpbnZlcnNlOiBmdW5jdGlvbiBlbXB0eSgpIHtcbiAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgIH0sXG4gICAgICAgIHJvb3Q6IHJvb3QsXG4gICAgICAgIHBhcmVudHM6IFtjdHhfMV1cbiAgICAgIH0pO1xuICAgICAgciArPSAnPC9kaXY+PGRpdiBjbGFzcz10aXRsZT5Db25uZWN0PC9kaXY+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1wYWdlLWNvbnRlbnQ+PGRpdiBjbGFzcz1yb3c+PGRpdiBjbGFzcz1cImNvbC0xMDAgdGFibGV0LTUwIGNlbnRlclwiPic7XG4gICAgICByICs9IFRlbXBsYXRlN0hlbHBlcnMud2l0aC5jYWxsKGN0eF8xLCBjdHhfMS5jb25uZWN0LCB7XG4gICAgICAgIGhhc2g6IHt9LFxuICAgICAgICBkYXRhOiBkYXRhXzEgfHwge30sXG4gICAgICAgIGZuOiBmdW5jdGlvbiAoY3R4XzIsIGRhdGFfMikge1xuICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgciArPSAnPGRpdiBjbGFzcz1ibG9jay10aXRsZT5JUEZTIFBlZXJzIDxzcGFuIGNsYXNzPVwiYmFkZ2UgcGVlcnMtYmFkZ2UgY29sb3ItYmx1ZVwiPic7XG4gICAgICAgICAgciArPSBjKGN0eF8yLnBlZXJDb3VudCwgY3R4XzIpO1xuICAgICAgICAgIHIgKz0gJzwvc3Bhbj48L2Rpdj48ZGl2IGNsYXNzPVwiYmxvY2sgbGlzdCBtZWRpYS1saXN0IHBlZXItbGlzdFwiPjx1bD4nO1xuICAgICAgICAgIHIgKz0gVGVtcGxhdGU3SGVscGVycy5lYWNoLmNhbGwoY3R4XzIsIGN0eF8yLnBlZXJzLCB7XG4gICAgICAgICAgICBoYXNoOiB7fSxcbiAgICAgICAgICAgIGRhdGE6IGRhdGFfMiB8fCB7fSxcbiAgICAgICAgICAgIGZuOiBmdW5jdGlvbiAoY3R4XzMsIGRhdGFfMykge1xuICAgICAgICAgICAgICB2YXIgciA9ICcnO1xuICAgICAgICAgICAgICByICs9ICc8bGk+PGRpdiBjbGFzcz1pdGVtLWNvbnRlbnQ+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9XCJpdGVtLXN1YnRpdGxlIG5vLXdyYXBcIj4nO1xuICAgICAgICAgICAgICByICs9IGMoY3R4XzMsIGN0eF8zKTtcbiAgICAgICAgICAgICAgciArPSAnPC9kaXY+PC9kaXY+PC9kaXY+PC9saT4nO1xuICAgICAgICAgICAgICByZXR1cm4gcjtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBpbnZlcnNlOiBmdW5jdGlvbiBlbXB0eSgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHJvb3Q6IHJvb3QsXG4gICAgICAgICAgICBwYXJlbnRzOiBbY3R4XzFdXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgciArPSAnPC91bD48L2Rpdj48ZGl2IGNsYXNzPWJsb2NrLXRpdGxlPklQRlMgSW5mbzwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayBsaXN0IG1lZGlhLWxpc3RcIj48dWw+JztcbiAgICAgICAgICByICs9IFRlbXBsYXRlN0hlbHBlcnMuZWFjaC5jYWxsKGN0eF8yLCBjdHhfMi5pcGZzSW5mby5hZGRyZXNzZXMsIHtcbiAgICAgICAgICAgIGhhc2g6IHt9LFxuICAgICAgICAgICAgZGF0YTogZGF0YV8yIHx8IHt9LFxuICAgICAgICAgICAgZm46IGZ1bmN0aW9uIChjdHhfMywgZGF0YV8zKSB7XG4gICAgICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgICAgIHIgKz0gJzxsaT48ZGl2IGNsYXNzPWl0ZW0tY29udGVudD48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1cIml0ZW0tc3VidGl0bGUgbm8td3JhcFwiPic7XG4gICAgICAgICAgICAgIHIgKz0gYyhjdHhfMywgY3R4XzMpO1xuICAgICAgICAgICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2xpPic7XG4gICAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGludmVyc2U6IGZ1bmN0aW9uIGVtcHR5KCkge1xuICAgICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcm9vdDogcm9vdCxcbiAgICAgICAgICAgIHBhcmVudHM6IFtjdHhfMV1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICByICs9ICc8bGk+PGRpdiBjbGFzcz1pdGVtLWNvbnRlbnQ+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9XCJpdGVtLXRpdGxlIG5vLXdyYXBcIj48ZGl2IGNsYXNzPWl0ZW0taGVhZGVyPkFnZW50IFZlcnNpb248L2Rpdj4nO1xuICAgICAgICAgIHIgKz0gYyhjdHhfMi5pcGZzSW5mby5hZ2VudFZlcnNpb24sIGN0eF8yKTtcbiAgICAgICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2xpPjxsaT48ZGl2IGNsYXNzPWl0ZW0tY29udGVudD48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1cIml0ZW0tdGl0bGUgbm8td3JhcFwiPjxkaXYgY2xhc3M9aXRlbS1oZWFkZXI+SUQ8L2Rpdj4nO1xuICAgICAgICAgIHIgKz0gYyhjdHhfMi5pcGZzSW5mby5pZCwgY3R4XzIpO1xuICAgICAgICAgIHIgKz0gJzwvZGl2PjwvZGl2PjwvZGl2PjwvbGk+PC91bD48L2Rpdj48ZGl2IGNsYXNzPWJsb2NrLXRpdGxlPkFkZCBJUEZTIFBlZXI8L2Rpdj48Zm9ybSBjbGFzcz1cImJsb2NrIGxpc3RcIiBpZD1hZGQtcGVlci1mb3JtIGFjdGlvbj0vY29ubmVjdC9hZGRQZWVyPjx1bD48bGk+PGRpdiBjbGFzcz1cIml0ZW0tY29udGVudCBpdGVtLWlucHV0XCI+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9XCJpdGVtLXRpdGxlIGl0ZW0tbGFiZWxcIj5QZWVyIEFkZHJlc3M8L2Rpdj48ZGl2IGNsYXNzPWl0ZW0taW5wdXQtd3JhcD48aW5wdXQgaWQ9cGVlckFkZHJlc3NJbnB1dCB0eXBlPXRleHQgbmFtZT1wZWVyQWRkcmVzcyBwbGFjZWhvbGRlcj1cIkVudGVyIHBlZXIgYWRkcmVzc1wiIHJlcXVpcmVkIHZhbGlkYXRlPjxkaXYgY2xhc3M9cm93PjxidXR0b24gY2xhc3M9XCJidXR0b24gYnV0dG9uLWZpbGwgY29sLTEwMCBsYXJnZS00MFwiIHZhbHVlPVwiQWRkIFBlZXJcIiBAY2xpY2s9YWRkUGVlckNsaWNrPkFkZCBQZWVyPC9idXR0b24+PC9kaXY+PC9kaXY+PC9kaXY+PC9kaXY+PC9saT48L3VsPjwvZm9ybT48ZGl2IGNsYXNzPWJsb2NrLXRpdGxlPk1vbml0b3JlZCBGZWVkczwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayBsaXN0IG1lZGlhLWxpc3QgcGVlci1saXN0XCI+PHVsPic7XG4gICAgICAgICAgciArPSBUZW1wbGF0ZTdIZWxwZXJzLmVhY2guY2FsbChjdHhfMiwgY3R4XzIuc3Vic2NyaWJlZCwge1xuICAgICAgICAgICAgaGFzaDoge30sXG4gICAgICAgICAgICBkYXRhOiBkYXRhXzIgfHwge30sXG4gICAgICAgICAgICBmbjogZnVuY3Rpb24gKGN0eF8zLCBkYXRhXzMpIHtcbiAgICAgICAgICAgICAgdmFyIHIgPSAnJztcbiAgICAgICAgICAgICAgciArPSAnPGxpPjxkaXYgY2xhc3M9aXRlbS1jb250ZW50PjxkaXYgY2xhc3M9aXRlbS1pbm5lcj48ZGl2IGNsYXNzPVwiaXRlbS1zdWJ0aXRsZSBuby13cmFwXCI+JztcbiAgICAgICAgICAgICAgciArPSBjKGN0eF8zLCBjdHhfMyk7XG4gICAgICAgICAgICAgIHIgKz0gJzwvZGl2PjwvZGl2PjwvZGl2PjwvbGk+JztcbiAgICAgICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgaW52ZXJzZTogZnVuY3Rpb24gZW1wdHkoKSB7XG4gICAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICByb290OiByb290LFxuICAgICAgICAgICAgcGFyZW50czogW2N0eF8xXVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHIgKz0gJzwvdWw+PC9kaXY+JztcbiAgICAgICAgICByZXR1cm4gcjtcbiAgICAgICAgfSxcbiAgICAgICAgaW52ZXJzZTogZnVuY3Rpb24gZW1wdHkoKSB7XG4gICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9LFxuICAgICAgICByb290OiByb290LFxuICAgICAgICBwYXJlbnRzOiBbY3R4XzFdXG4gICAgICB9KTtcbiAgICAgIHIgKz0gJyAnO1xuICAgICAgciArPSBUZW1wbGF0ZTdIZWxwZXJzLndpdGguY2FsbChjdHhfMSwgY3R4XzEuc2NoZW1hLCB7XG4gICAgICAgIGhhc2g6IHt9LFxuICAgICAgICBkYXRhOiBkYXRhXzEgfHwge30sXG4gICAgICAgIGZuOiBmdW5jdGlvbiAoY3R4XzIsIGRhdGFfMikge1xuICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgciArPSAnPGRpdiBjbGFzcz1ibG9jay10aXRsZT5TY2hlbWE8L2Rpdj48ZGl2IGNsYXNzPWJsb2NrPjxkaXYgY2xhc3M9XCJsaXN0IG1lZGlhLWxpc3RcIj48dWw+PGxpPjxkaXYgY2xhc3M9aXRlbS1jb250ZW50PjxkaXYgY2xhc3M9aXRlbS1pbm5lcj48ZGl2IGNsYXNzPWl0ZW0tdGl0bGUtcm93PjxkaXYgY2xhc3M9aXRlbS10aXRsZT5HbG9iYWwgU2NoZW1hIENJRDwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9aXRlbS10ZXh0Pic7XG4gICAgICAgICAgciArPSBjKGN0eF8yLmdsb2JhbFNjaGVtYS5jaWQsIGN0eF8yKTtcbiAgICAgICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2xpPjxsaT48ZGl2IGNsYXNzPWl0ZW0tY29udGVudD48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1pdGVtLXRpdGxlLXJvdz48ZGl2IGNsYXNzPWl0ZW0tdGl0bGU+R2xvYmFsIFNjaGVtYSBPd25lcjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9aXRlbS10ZXh0Pic7XG4gICAgICAgICAgciArPSBjKGN0eF8yLmdsb2JhbFNjaGVtYS5vd25lciwgY3R4XzIpO1xuICAgICAgICAgIHIgKz0gJzwvZGl2PjwvZGl2PjwvZGl2PjwvbGk+JztcbiAgICAgICAgICByICs9IFRlbXBsYXRlN0hlbHBlcnMuZWFjaC5jYWxsKGN0eF8yLCBjdHhfMi5zdG9yZXMsIHtcbiAgICAgICAgICAgIGhhc2g6IHt9LFxuICAgICAgICAgICAgZGF0YTogZGF0YV8yIHx8IHt9LFxuICAgICAgICAgICAgZm46IGZ1bmN0aW9uIChjdHhfMywgZGF0YV8zKSB7XG4gICAgICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgICAgIHIgKz0gJzxsaT48ZGl2IGNsYXNzPWl0ZW0tY29udGVudD48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1pdGVtLXRpdGxlLXJvdz48ZGl2IGNsYXNzPWl0ZW0tdGl0bGU+JztcbiAgICAgICAgICAgICAgciArPSBjKGN0eF8zLm5hbWUsIGN0eF8zKTtcbiAgICAgICAgICAgICAgciArPSAnPC9kaXY+PGRpdiBjbGFzcz1pdGVtLWFmdGVyPkxvYWRlZDogJztcbiAgICAgICAgICAgICAgciArPSBjKGN0eF8zLmxvYWRlZCwgY3R4XzMpO1xuICAgICAgICAgICAgICByICs9ICc8L2Rpdj48L2Rpdj48ZGl2IGNsYXNzPWl0ZW0tdGV4dD4nO1xuICAgICAgICAgICAgICByICs9IGMoY3R4XzMuYWRkcmVzcywgY3R4XzMpO1xuICAgICAgICAgICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2xpPic7XG4gICAgICAgICAgICAgIHJldHVybiByO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGludmVyc2U6IGZ1bmN0aW9uIGVtcHR5KCkge1xuICAgICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcm9vdDogcm9vdCxcbiAgICAgICAgICAgIHBhcmVudHM6IFtjdHhfMV1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICByICs9ICc8L3VsPjwvZGl2PjwvZGl2Pic7XG4gICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgIH0sXG4gICAgICAgIGludmVyc2U6IGZ1bmN0aW9uIGVtcHR5KCkge1xuICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgfSxcbiAgICAgICAgcm9vdDogcm9vdCxcbiAgICAgICAgcGFyZW50czogW2N0eF8xXVxuICAgICAgfSk7XG4gICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj4nO1xuICAgICAgcmV0dXJuIHI7XG4gICAgfSh0aGlzKTtcbiAgfVxuXG4gIC8vIGhvb2tzXG4gIGJlZm9yZUNyZWF0ZSgpIHtcbiAgICAvLyBCaW5kIGV2ZW50IGhhbmRsZXJzIHRvIGNvbXBvbmVudCBjb250ZXh0XG4gICAgdGhpcy5hZGRQZWVyQ2xpY2sgPSB0aGlzLmFkZFBlZXJDbGljay5iaW5kKHRoaXMpO1xuICB9XG5cbiAgbW91bnRlZCgpIHt9XG5cbiAgYmVmb3JlRGVzdHJveSgpIHt9IC8vIG1ldGhvZHNcblxuXG4gIGFkZFBlZXJDbGljayhlKSB7XG4gICAgdGhpcy5zdWJtaXRGb3JtKGUsIFwiI2FkZC1wZWVyLWZvcm1cIik7XG4gIH1cblxufVxuXG5GN1BhZ2VDb21wb25lbnQuaWQgPSAnYTk3ODhlNWY5Mic7XG5GN1BhZ2VDb21wb25lbnQuc3R5bGVTY29wZWQgPSBmYWxzZTtcbmV4cG9ydCBkZWZhdWx0IEY3UGFnZUNvbXBvbmVudDtcbiAgICAiLCJcbiAgICAgIGltcG9ydCBUZW1wbGF0ZTcgZnJvbSAndGVtcGxhdGU3JztcbiAgICAgIGNvbnN0IFRlbXBsYXRlN0hlbHBlcnMgPSBUZW1wbGF0ZTcuaGVscGVycztcbiAgXG4gICAgICBcbiAgXG4gICAgICAvLyBmaXJzdCB3ZSBpbXBvcnQgc3VwZXIgY2xhc3NcbmltcG9ydCB7IFNwYWNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbC9zcGFjZS1jb21wb25lbnQnOyAvLyB3ZSBuZWVkIHRvIGV4cG9ydCBleHRlbmRlZCBjbGFzc1xuXG5jbGFzcyBGN1BhZ2VDb21wb25lbnQgZXh0ZW5kcyBTcGFjZUNvbXBvbmVudCB7XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGN0eF8xLCBkYXRhXzEsIHJvb3QpIHtcbiAgICAgIGZ1bmN0aW9uIGlzQXJyYXkoYXJyKSB7XG4gICAgICAgIHJldHVybiBBcnJheS5pc0FycmF5KGFycik7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIGlzRnVuY3Rpb24oZnVuYykge1xuICAgICAgICByZXR1cm4gdHlwZW9mIGZ1bmMgPT09ICdmdW5jdGlvbic7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIGModmFsLCBjdHgpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWwgIT09IFwidW5kZWZpbmVkXCIgJiYgdmFsICE9PSBudWxsKSB7XG4gICAgICAgICAgaWYgKGlzRnVuY3Rpb24odmFsKSkge1xuICAgICAgICAgICAgcmV0dXJuIHZhbC5jYWxsKGN0eCk7XG4gICAgICAgICAgfSBlbHNlIHJldHVybiB2YWw7XG4gICAgICAgIH0gZWxzZSByZXR1cm4gXCJcIjtcbiAgICAgIH1cblxuICAgICAgcm9vdCA9IHJvb3QgfHwgY3R4XzEgfHwge307XG4gICAgICB2YXIgciA9ICcnO1xuICAgICAgciArPSAnPGRpdiBjbGFzcz1wYWdlIGRhdGEtbmFtZT1jcmVhdGUtd2FsbGV0LXN1Y2Nlc3M+PGRpdiBjbGFzcz1wYWdlLWNvbnRlbnQ+PGRpdiBjbGFzcz1yb3c+PGRpdiBjbGFzcz1cImNvbC0xMDAgbGFyZ2UtNTAgY2VudGVyXCI+PGRpdiBjbGFzcz1ibG9jay10aXRsZT48aSBjbGFzcz1cImljb24gZjctaWNvbnNcIj5sb2NrX3NoaWVsZF9maWxsPC9pPiBUaGlzIGlzIHlvdXIga2V5IHBocmFzZS48L2Rpdj48ZGl2IGNsYXNzPVwiYmxvY2sgYmxvY2stc3Ryb25nIGluc2V0XCI+PGRpdiBjbGFzcz1yb3c+JztcbiAgICAgIHIgKz0gVGVtcGxhdGU3SGVscGVycy5lYWNoLmNhbGwoY3R4XzEsIGN0eF8xLm1uZW1vbmljLCB7XG4gICAgICAgIGhhc2g6IHt9LFxuICAgICAgICBkYXRhOiBkYXRhXzEgfHwge30sXG4gICAgICAgIGZuOiBmdW5jdGlvbiAoY3R4XzIsIGRhdGFfMikge1xuICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgciArPSAnPGRpdiBjbGFzcz1cImNvbC0zMyBtbmVtb25pY1wiPjxkaXYgY2xhc3M9bnVtYmVyPic7XG4gICAgICAgICAgciArPSBUZW1wbGF0ZTdIZWxwZXJzLmpzLmNhbGwoY3R4XzIsIFwiQGluZGV4ICsgMVwiLCB7XG4gICAgICAgICAgICBoYXNoOiB7fSxcbiAgICAgICAgICAgIGRhdGE6IGRhdGFfMiB8fCB7fSxcbiAgICAgICAgICAgIGZuOiBmdW5jdGlvbiBlbXB0eSgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGludmVyc2U6IGZ1bmN0aW9uIGVtcHR5KCkge1xuICAgICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgcm9vdDogcm9vdCxcbiAgICAgICAgICAgIHBhcmVudHM6IFtjdHhfMV1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICByICs9ICcuPC9kaXY+PGRpdiBjbGFzcz13b3JkPic7XG4gICAgICAgICAgciArPSBjKGN0eF8yLCBjdHhfMik7XG4gICAgICAgICAgciArPSAnPC9kaXY+PC9kaXY+JztcbiAgICAgICAgICByZXR1cm4gcjtcbiAgICAgICAgfSxcbiAgICAgICAgaW52ZXJzZTogZnVuY3Rpb24gZW1wdHkoKSB7XG4gICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9LFxuICAgICAgICByb290OiByb290LFxuICAgICAgICBwYXJlbnRzOiBbY3R4XzFdXG4gICAgICB9KTtcbiAgICAgIHIgKz0gJzwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayBibG9jay1zdHJvbmcgaW5zZXRcIj4nO1xuICAgICAgciArPSBUZW1wbGF0ZTdIZWxwZXJzLmVhY2guY2FsbChjdHhfMSwgY3R4XzEubW5lbW9uaWMsIHtcbiAgICAgICAgaGFzaDoge30sXG4gICAgICAgIGRhdGE6IGRhdGFfMSB8fCB7fSxcbiAgICAgICAgZm46IGZ1bmN0aW9uIChjdHhfMiwgZGF0YV8yKSB7XG4gICAgICAgICAgdmFyIHIgPSAnJztcbiAgICAgICAgICByICs9ICcgJztcbiAgICAgICAgICByICs9IGMoY3R4XzIsIGN0eF8yKTtcbiAgICAgICAgICByICs9ICcgJztcbiAgICAgICAgICByZXR1cm4gcjtcbiAgICAgICAgfSxcbiAgICAgICAgaW52ZXJzZTogZnVuY3Rpb24gZW1wdHkoKSB7XG4gICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9LFxuICAgICAgICByb290OiByb290LFxuICAgICAgICBwYXJlbnRzOiBbY3R4XzFdXG4gICAgICB9KTtcbiAgICAgIHIgKz0gJzwvZGl2PjxkaXYgY2xhc3M9YmxvY2stdGl0bGU+SW1wb3J0YW50PC9kaXY+PGRpdiBjbGFzcz1cImJsb2NrIGJsb2NrLXN0cm9uZyBpbnNldFwiPjxwPlRoaXMgMTIgd29yZCBzZWNyZXQgcGhyYXNlIGlzIGhvdyB5b3VyIGFjY291bnQgaXMgYWNjZXNzZWQuIEl0XFwncyBpbXBvcnRhbnQgdG8ga2VlcCB0aGlzIHBocmFzZSBzZWNyZXQuPC9wPjxwPjxzdHJvbmc+V3JpdGUgdGhpcyBwaHJhc2Ugb24gYSBwaWVjZSBvZiBwYXBlcjwvc3Ryb25nPiBhbmQga2VlcCBpdCBpbiBhIHNlY3VyZSBsb2NhdGlvbi48L3A+PHA+PHN0cm9uZz5XQVJOSU5HOjwvc3Ryb25nPiBEb25cXCd0IGV2ZXIgdGVsbCBhbnlvbmUgeW91ciBzZWNyZXQgcGhyYXNlLiBBbnlvbmUgd2l0aCB0aGlzIHBocmFzZSBjYW4gdXNlIGl0IHRvIGFjY2VzcyB5b3VyIGFjY291bnQgYW5kIG1ha2UgcGVybWFuZW50IGNoYW5nZXMuIFRoZXJlIGlzIG5vIHdheSB0byByZWNvdmVyIGxvc3Qga2V5IHBocmFzZXMuPC9wPjwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayByb3dcIj48YSBjbGFzcz1cImJ1dHRvbiBidXR0b24tZmlsbCBjb2wtMTAwIGxhcmdlLTQwXCIgaHJlZj0vY3JlYXRlV2FsbGV0RG9uZT5Hb3QgaXQ8L2E+PC9kaXY+PC9kaXY+PC9kaXY+PC9kaXY+PC9kaXY+JztcbiAgICAgIHJldHVybiByO1xuICAgIH0odGhpcyk7XG4gIH1cblxuICAvLyBob29rc1xuICBiZWZvcmVDcmVhdGUoKSB7fVxuXG4gIG1vdW50ZWQoKSB7fVxuXG4gIGJlZm9yZURlc3Ryb3koKSB7fVxuXG59XG5cbkY3UGFnZUNvbXBvbmVudC5pZCA9ICcyOTUwNDBhYWNmJztcbkY3UGFnZUNvbXBvbmVudC5zdHlsZVNjb3BlZCA9IGZhbHNlO1xuZXhwb3J0IGRlZmF1bHQgRjdQYWdlQ29tcG9uZW50O1xuICAgICIsIi8vIGZpcnN0IHdlIGltcG9ydCBzdXBlciBjbGFzc1xuaW1wb3J0IHsgU3BhY2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi91dGlsL3NwYWNlLWNvbXBvbmVudCc7IC8vIHdlIG5lZWQgdG8gZXhwb3J0IGV4dGVuZGVkIGNsYXNzXG5cbmNsYXNzIEY3UGFnZUNvbXBvbmVudCBleHRlbmRzIFNwYWNlQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoY3R4XzEsIGRhdGFfMSwgcm9vdCkge1xuICAgICAgZnVuY3Rpb24gaXNBcnJheShhcnIpIHtcbiAgICAgICAgcmV0dXJuIEFycmF5LmlzQXJyYXkoYXJyKTtcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gaXNGdW5jdGlvbihmdW5jKSB7XG4gICAgICAgIHJldHVybiB0eXBlb2YgZnVuYyA9PT0gJ2Z1bmN0aW9uJztcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gYyh2YWwsIGN0eCkge1xuICAgICAgICBpZiAodHlwZW9mIHZhbCAhPT0gXCJ1bmRlZmluZWRcIiAmJiB2YWwgIT09IG51bGwpIHtcbiAgICAgICAgICBpZiAoaXNGdW5jdGlvbih2YWwpKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsLmNhbGwoY3R4KTtcbiAgICAgICAgICB9IGVsc2UgcmV0dXJuIHZhbDtcbiAgICAgICAgfSBlbHNlIHJldHVybiBcIlwiO1xuICAgICAgfVxuXG4gICAgICByb290ID0gcm9vdCB8fCBjdHhfMSB8fCB7fTtcbiAgICAgIHZhciByID0gJyc7XG4gICAgICByICs9ICc8ZGl2IGNsYXNzPXBhZ2UgZGF0YS1uYW1lPWNyZWF0ZS13YWxsZXQ+PGRpdiBjbGFzcz1wYWdlLWNvbnRlbnQ+PGRpdiBjbGFzcz1yb3c+PGRpdiBjbGFzcz1cImNvbC0xMDAgbGFyZ2UtNTAgY2VudGVyXCI+PGZvcm0gaWQ9Y3JlYXRlLXdhbGxldC1mb3JtIGFjdGlvbj0vY3JlYXRlV2FsbGV0PjxkaXYgY2xhc3M9YmxvY2stdGl0bGU+Q3JlYXRlIFBhc3N3b3JkPC9kaXY+PGRpdiBjbGFzcz1ibG9jaz5UaGlzIHBhc3N3b3JkIHdpbGwgdXNlZCB0byBlbmNyeXB0IHlvdXIgd2FsbGV0IG9uIHRoaXMgZGV2aWNlLiBDaG9vc2UgYSBzZWN1cmUgcGFzc3dvcmQuPC9kaXY+PGRpdiBjbGFzcz1cImJsb2NrIGxpc3RcIj48dWw+PGxpPjxkaXYgY2xhc3M9XCJpdGVtLWNvbnRlbnQgaXRlbS1pbnB1dFwiPjxkaXYgY2xhc3M9aXRlbS1pbm5lcj48ZGl2IGNsYXNzPVwiaXRlbS10aXRsZSBpdGVtLWxhYmVsXCI+TmV3IFBhc3N3b3JkIChtaW4gOCBjaGFycyk8L2Rpdj48ZGl2IGNsYXNzPWl0ZW0taW5wdXQtd3JhcD48aW5wdXQgdHlwZT1wYXNzd29yZCBpZD1uZXdQYXNzd29yZCBuYW1lPW5ld1Bhc3N3b3JkIHBsYWNlaG9sZGVyPVwiRW50ZXIgbmV3IHBhc3N3b3JkXCIgbWlubGVuZ3RoPTggcmVxdWlyZWQgdmFsaWRhdGU+PC9kaXY+PC9kaXY+PC9kaXY+PC9saT48bGk+PGRpdiBjbGFzcz1cIml0ZW0tY29udGVudCBpdGVtLWlucHV0XCI+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9XCJpdGVtLXRpdGxlIGl0ZW0tbGFiZWxcIj5Db25maXJtIFBhc3N3b3JkPC9kaXY+PGRpdiBjbGFzcz1pdGVtLWlucHV0LXdyYXA+PGlucHV0IHR5cGU9cGFzc3dvcmQgaWQ9Y29uZmlybVBhc3N3b3JkIG5hbWU9Y29uZmlybVBhc3N3b3JkIHBsYWNlaG9sZGVyPVwiQ29uZmlybSBQYXNzd29yZFwiIG1pbmxlbmd0aD04IHJlcXVpcmVkIHZhbGlkYXRlPjwvZGl2PjwvZGl2PjwvZGl2PjwvbGk+PC91bD48L2Rpdj48ZGl2IGNsYXNzPVwiYmxvY2sgcm93XCI+PGJ1dHRvbiBjbGFzcz1cImJ1dHRvbiBidXR0b24tZmlsbCBjb2wtMTAwIGxhcmdlLTQwXCIgQGNsaWNrPWNyZWF0ZUNsaWNrPkNyZWF0ZSBXYWxsZXQ8L2J1dHRvbj48L2Rpdj48L2Zvcm0+PC9kaXY+PC9kaXY+PC9kaXY+PC9kaXY+JztcbiAgICAgIHJldHVybiByO1xuICAgIH0odGhpcyk7XG4gIH1cblxuICAvLyBob29rc1xuICBiZWZvcmVDcmVhdGUoKSB7XG4gICAgLy8gQmluZCBldmVudCBoYW5kbGVycyB0byBjb21wb25lbnQgY29udGV4dFxuICAgIHRoaXMuY3JlYXRlQ2xpY2sgPSB0aGlzLmNyZWF0ZUNsaWNrLmJpbmQodGhpcyk7XG4gIH1cblxuICBtb3VudGVkKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgZnVuY3Rpb24gdmFsaWRhdGVNYXRjaGluZ1Bhc3N3b3JkcyhlKSB7XG4gICAgICBsZXQgcGFzc3dvcmQgPSBzZWxmLiQkKCcjbmV3UGFzc3dvcmQnKS52YWwoKTtcbiAgICAgIGxldCBjb25maXJtUGFzc3dvcmQgPSBzZWxmLiQkKCcjY29uZmlybVBhc3N3b3JkJykudmFsKCk7XG4gICAgICBzZWxmLiQkKCcjbmV3UGFzc3dvcmQnKVswXS5zZXRDdXN0b21WYWxpZGl0eSgnJyk7XG5cbiAgICAgIGlmIChwYXNzd29yZCAhPSBjb25maXJtUGFzc3dvcmQpIHtcbiAgICAgICAgc2VsZi4kJCgnI25ld1Bhc3N3b3JkJylbMF0uc2V0Q3VzdG9tVmFsaWRpdHkoXCJQYXNzd29yZHMgbXVzdCBtYXRjaFwiKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLiQkKCcjbmV3UGFzc3dvcmQnKS5vbignY2hhbmdlIGJsdXInLCB2YWxpZGF0ZU1hdGNoaW5nUGFzc3dvcmRzKTtcbiAgICB0aGlzLiQkKCcjY29uZmlybVBhc3N3b3JkJykub24oJ2NoYW5nZSBibHVyJywgdmFsaWRhdGVNYXRjaGluZ1Bhc3N3b3Jkcyk7XG4gIH1cblxuICBiZWZvcmVEZXN0cm95KCkge30gLy8gbWV0aG9kc1xuXG5cbiAgY3JlYXRlQ2xpY2soZSkge1xuICAgIHRoaXMuc3VibWl0Rm9ybShlLCBcIiNjcmVhdGUtd2FsbGV0LWZvcm1cIik7XG4gIH1cblxufVxuXG5GN1BhZ2VDb21wb25lbnQuaWQgPSAnNzE2ZDg2MTBlYic7XG5GN1BhZ2VDb21wb25lbnQuc3R5bGVTY29wZWQgPSBmYWxzZTtcbmV4cG9ydCBkZWZhdWx0IEY3UGFnZUNvbXBvbmVudDsiLCIvLyBmaXJzdCB3ZSBpbXBvcnQgc3VwZXIgY2xhc3NcbmltcG9ydCB7IFNwYWNlQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vdXRpbC9zcGFjZS1jb21wb25lbnQnOyAvLyB3ZSBuZWVkIHRvIGV4cG9ydCBleHRlbmRlZCBjbGFzc1xuXG5jbGFzcyBGN1BhZ2VDb21wb25lbnQgZXh0ZW5kcyBTcGFjZUNvbXBvbmVudCB7XG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGN0eF8xLCBkYXRhXzEsIHJvb3QpIHtcbiAgICAgIGZ1bmN0aW9uIGlzQXJyYXkoYXJyKSB7XG4gICAgICAgIHJldHVybiBBcnJheS5pc0FycmF5KGFycik7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIGlzRnVuY3Rpb24oZnVuYykge1xuICAgICAgICByZXR1cm4gdHlwZW9mIGZ1bmMgPT09ICdmdW5jdGlvbic7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIGModmFsLCBjdHgpIHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWwgIT09IFwidW5kZWZpbmVkXCIgJiYgdmFsICE9PSBudWxsKSB7XG4gICAgICAgICAgaWYgKGlzRnVuY3Rpb24odmFsKSkge1xuICAgICAgICAgICAgcmV0dXJuIHZhbC5jYWxsKGN0eCk7XG4gICAgICAgICAgfSBlbHNlIHJldHVybiB2YWw7XG4gICAgICAgIH0gZWxzZSByZXR1cm4gXCJcIjtcbiAgICAgIH1cblxuICAgICAgcm9vdCA9IHJvb3QgfHwgY3R4XzEgfHwge307XG4gICAgICB2YXIgciA9ICcnO1xuICAgICAgciArPSAnPGRpdiBjbGFzcz1wYWdlIGRhdGEtbmFtZT1lbnRlci1yZWNvdmVyeT48ZGl2IGNsYXNzPXBhZ2UtY29udGVudD48ZGl2IGNsYXNzPXJvdz48ZGl2IGNsYXNzPVwiY29sLTEwMCBsYXJnZS01MCBjZW50ZXJcIj48ZGl2IGNsYXNzPWJsb2NrLXRpdGxlPlJlc3RvcmUgWW91ciBBY2NvdW50PC9kaXY+PGRpdiBjbGFzcz1ibG9jaz48cD5FbnRlciB5b3VyIHNlY3JldCB0d2VsdmUgd29yZCBwaHJhc2UgdG8gcmVzdG9yZSB5b3VyIGFjY291bnQuPC9wPjxmb3JtIGlkPXJlc3RvcmUtYWNjb3VudC1mb3JtIGFjdGlvbj0vcmVzdG9yZUFjY291bnQ+PGRpdiBjbGFzcz1saXN0Pjx1bD48bGk+PGRpdiBjbGFzcz1cIml0ZW0tY29udGVudCBpdGVtLWlucHV0XCI+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9XCJpdGVtLXRpdGxlIGl0ZW0tbGFiZWxcIj5SZWNvdmVyeSBTZWVkICgxMiB3b3Jkcyk8L2Rpdj48ZGl2IGNsYXNzPWl0ZW0taW5wdXQtd3JhcD48dGV4dGFyZWEgbmFtZT1yZWNvdmVyeVNlZWQgcmVxdWlyZWQgdmFsaWRhdGU+PC90ZXh0YXJlYT48L2Rpdj48L2Rpdj48L2Rpdj48L2xpPjxsaT48ZGl2IGNsYXNzPVwiaXRlbS1jb250ZW50IGl0ZW0taW5wdXRcIj48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1cIml0ZW0tdGl0bGUgaXRlbS1sYWJlbFwiPk5ldyBQYXNzd29yZCAobWluIDggY2hhcnMpPC9kaXY+PGRpdiBjbGFzcz1pdGVtLWlucHV0LXdyYXA+PGlucHV0IHR5cGU9cGFzc3dvcmQgaWQ9bmV3UGFzc3dvcmQgbmFtZT1uZXdQYXNzd29yZCBwbGFjZWhvbGRlcj1cIkVudGVyIG5ldyBwYXNzd29yZFwiIG1pbmxlbmd0aD04IHJlcXVpcmVkIHZhbGlkYXRlPjwvZGl2PjwvZGl2PjwvZGl2PjwvbGk+PGxpPjxkaXYgY2xhc3M9XCJpdGVtLWNvbnRlbnQgaXRlbS1pbnB1dFwiPjxkaXYgY2xhc3M9aXRlbS1pbm5lcj48ZGl2IGNsYXNzPVwiaXRlbS10aXRsZSBpdGVtLWxhYmVsXCI+Q29uZmlybSBQYXNzd29yZDwvZGl2PjxkaXYgY2xhc3M9aXRlbS1pbnB1dC13cmFwPjxpbnB1dCB0eXBlPXBhc3N3b3JkIGlkPWNvbmZpcm1QYXNzd29yZCBuYW1lPWNvbmZpcm1QYXNzd29yZCBtaW5sZW5ndGg9OCBwbGFjZWhvbGRlcj1cIkNvbmZpcm0gUGFzc3dvcmRcIiByZXF1aXJlZCB2YWxpZGF0ZT48L2Rpdj48L2Rpdj48L2Rpdj48L2xpPjwvdWw+PC9kaXY+PGRpdiBjbGFzcz1yb3c+PGJ1dHRvbiBjbGFzcz1cImJ1dHRvbiBidXR0b24tZmlsbCBjb2wtNDBcIiBAY2xpY2s9cmVzdG9yZUNsaWNrPlJlc3RvcmUgV2FsbGV0PC9idXR0b24+PC9kaXY+PC9mb3JtPjwvZGl2PjwvZGl2PjwvZGl2PjwvZGl2PjwvZGl2Pic7XG4gICAgICByZXR1cm4gcjtcbiAgICB9KHRoaXMpO1xuICB9XG5cbiAgLy8gaG9va3NcbiAgYmVmb3JlQ3JlYXRlKCkge1xuICAgIHRoaXMucmVzdG9yZUNsaWNrID0gdGhpcy5yZXN0b3JlQ2xpY2suYmluZCh0aGlzKTtcbiAgfVxuXG4gIG1vdW50ZWQoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICBmdW5jdGlvbiB2YWxpZGF0ZU1hdGNoaW5nUGFzc3dvcmRzKGUpIHtcbiAgICAgIGxldCBwYXNzd29yZCA9IHNlbGYuJCQoJyNuZXdQYXNzd29yZCcpLnZhbCgpO1xuICAgICAgbGV0IGNvbmZpcm1QYXNzd29yZCA9IHNlbGYuJCQoJyNjb25maXJtUGFzc3dvcmQnKS52YWwoKTtcbiAgICAgIHNlbGYuJCQoJyNuZXdQYXNzd29yZCcpWzBdLnNldEN1c3RvbVZhbGlkaXR5KCcnKTtcblxuICAgICAgaWYgKHBhc3N3b3JkICE9IGNvbmZpcm1QYXNzd29yZCkge1xuICAgICAgICBzZWxmLiQkKCcjbmV3UGFzc3dvcmQnKVswXS5zZXRDdXN0b21WYWxpZGl0eShcIlBhc3N3b3JkcyBtdXN0IG1hdGNoXCIpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMuJCQoJyNuZXdQYXNzd29yZCcpLm9uKCdjaGFuZ2UgYmx1cicsIHZhbGlkYXRlTWF0Y2hpbmdQYXNzd29yZHMpO1xuICAgIHRoaXMuJCQoJyNjb25maXJtUGFzc3dvcmQnKS5vbignY2hhbmdlIGJsdXInLCB2YWxpZGF0ZU1hdGNoaW5nUGFzc3dvcmRzKTtcbiAgfSAvLyBtZXRob2RzXG5cblxuICByZXN0b3JlQ2xpY2soZSkge1xuICAgIHRoaXMuc3VibWl0Rm9ybShlLCBcIiNyZXN0b3JlLWFjY291bnQtZm9ybVwiKTtcbiAgfVxuXG59XG5cbkY3UGFnZUNvbXBvbmVudC5pZCA9ICczMjdmMzNjOTA2JztcbkY3UGFnZUNvbXBvbmVudC5zdHlsZVNjb3BlZCA9IGZhbHNlO1xuZXhwb3J0IGRlZmF1bHQgRjdQYWdlQ29tcG9uZW50OyIsIlxuICAgICAgaW1wb3J0IFRlbXBsYXRlNyBmcm9tICd0ZW1wbGF0ZTcnO1xuICAgICAgY29uc3QgVGVtcGxhdGU3SGVscGVycyA9IFRlbXBsYXRlNy5oZWxwZXJzO1xuICBcbiAgICAgIFxuICBcbiAgICAgIC8vIGZpcnN0IHdlIGltcG9ydCBzdXBlciBjbGFzc1xuaW1wb3J0IHsgU3BhY2VDb21wb25lbnQgfSBmcm9tICcuLi8uLi91dGlsL3NwYWNlLWNvbXBvbmVudCc7IC8vIHdlIG5lZWQgdG8gZXhwb3J0IGV4dGVuZGVkIGNsYXNzXG5cbmNsYXNzIEY3UGFnZUNvbXBvbmVudCBleHRlbmRzIFNwYWNlQ29tcG9uZW50IHtcbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiBmdW5jdGlvbiAoY3R4XzEsIGRhdGFfMSwgcm9vdCkge1xuICAgICAgZnVuY3Rpb24gaXNBcnJheShhcnIpIHtcbiAgICAgICAgcmV0dXJuIEFycmF5LmlzQXJyYXkoYXJyKTtcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gaXNGdW5jdGlvbihmdW5jKSB7XG4gICAgICAgIHJldHVybiB0eXBlb2YgZnVuYyA9PT0gJ2Z1bmN0aW9uJztcbiAgICAgIH1cblxuICAgICAgZnVuY3Rpb24gYyh2YWwsIGN0eCkge1xuICAgICAgICBpZiAodHlwZW9mIHZhbCAhPT0gXCJ1bmRlZmluZWRcIiAmJiB2YWwgIT09IG51bGwpIHtcbiAgICAgICAgICBpZiAoaXNGdW5jdGlvbih2YWwpKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsLmNhbGwoY3R4KTtcbiAgICAgICAgICB9IGVsc2UgcmV0dXJuIHZhbDtcbiAgICAgICAgfSBlbHNlIHJldHVybiBcIlwiO1xuICAgICAgfVxuXG4gICAgICByb290ID0gcm9vdCB8fCBjdHhfMSB8fCB7fTtcbiAgICAgIHZhciByID0gJyc7XG4gICAgICByICs9ICc8ZGl2IGNsYXNzPXBhZ2UgZGF0YS1uYW1lPXdhbGxldC1sYW5kaW5nPjxkaXYgY2xhc3M9cGFnZS1jb250ZW50PjxkaXYgY2xhc3M9cm93PjxkaXYgY2xhc3M9XCJjb2wtMTAwIGxhcmdlLTUwIGNlbnRlclwiPjxkaXYgY2xhc3M9YmxvY2stdGl0bGU+TmV3IHRvICc7XG4gICAgICByICs9IGMoY3R4XzEuYXBwTmFtZSwgY3R4XzEpO1xuICAgICAgciArPSAnPzwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayBsaXN0IG1lZGlhLWxpc3RcIj48dWw+PGxpPjxhIGhyZWY9L3Nob3dDcmVhdGVXYWxsZXQ+PGRpdiBjbGFzcz1pdGVtLWNvbnRlbnQ+PGRpdiBjbGFzcz1pdGVtLWlubmVyPjxkaXYgY2xhc3M9aXRlbS10aXRsZS1yb3c+PGRpdiBjbGFzcz1pdGVtLXRpdGxlPklcXCdtIG5ldywgbGV0XFwncyBnZXQgc2V0IHVwITwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9aXRlbS10ZXh0PkNyZWF0ZSBhIG5ldyB3YWxsZXQgd2l0aCBhIG5ldyBzZWVkIHBocmFzZS48L2Rpdj48L2Rpdj48L2Rpdj48L2E+PC9saT48bGk+PGEgaHJlZj0vZW50ZXJSZWNvdmVyeT48ZGl2IGNsYXNzPWl0ZW0tY29udGVudD48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1pdGVtLXRpdGxlLXJvdz48ZGl2IGNsYXNzPWl0ZW0tdGl0bGU+SSBoYXZlIGEgcmVjb3Zlcnkgc2VlZCBwaHJhc2UuPC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1pdGVtLXRleHQ+SW1wb3J0IHlvdXIgZXhpc3Rpbmcgd2FsbGV0IHVzaW5nIHRoZSAxMiB3b3JkIHJlY292ZXJ5IHNlZWQgcGhyYXNlLjwvZGl2PjwvZGl2PjwvZGl2PjwvYT48L2xpPjwvdWw+PC9kaXY+JztcbiAgICAgIHIgKz0gVGVtcGxhdGU3SGVscGVycy5pZi5jYWxsKGN0eF8xLCBjdHhfMS5zaG93VW5sb2NrLCB7XG4gICAgICAgIGhhc2g6IHt9LFxuICAgICAgICBkYXRhOiBkYXRhXzEgfHwge30sXG4gICAgICAgIGZuOiBmdW5jdGlvbiAoY3R4XzIsIGRhdGFfMikge1xuICAgICAgICAgIHZhciByID0gJyc7XG4gICAgICAgICAgciArPSAnPGZvcm0gaWQ9dW5sb2NrLXdhbGxldC1mb3JtIGFjdGlvbj0vdW5sb2NrV2FsbGV0PjxkaXYgY2xhc3M9YmxvY2stdGl0bGU+VW5sb2NrIFdhbGxldDwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayBsaXN0XCI+PHVsPjxsaT48ZGl2IGNsYXNzPVwiaXRlbS1jb250ZW50IGl0ZW0taW5wdXRcIj48ZGl2IGNsYXNzPWl0ZW0taW5uZXI+PGRpdiBjbGFzcz1cIml0ZW0tdGl0bGUgaXRlbS1sYWJlbFwiPlBhc3N3b3JkPC9kaXY+PGRpdiBjbGFzcz1pdGVtLWlucHV0LXdyYXA+PGlucHV0IHR5cGU9cGFzc3dvcmQgaWQ9cGFzc3dvcmQgbmFtZT1wYXNzd29yZCBwbGFjZWhvbGRlcj1cIkVudGVyIHBhc3N3b3JkIGZvciBzdG9yZWQgd2FsbGV0XCIgcmVxdWlyZWQgdmFsaWRhdGU+PC9kaXY+PC9kaXY+PC9kaXY+PC9saT48L3VsPjwvZGl2PjxkaXYgY2xhc3M9XCJibG9jayByb3dcIj48YnV0dG9uIGNsYXNzPVwiYnV0dG9uIGJ1dHRvbi1maWxsIGNvbC0xMDAgbGFyZ2UtNDBcIiBAY2xpY2s9dW5sb2NrQ2xpY2s+VW5sb2NrPC9idXR0b24+PC9kaXY+PC9mb3JtPic7XG4gICAgICAgICAgcmV0dXJuIHI7XG4gICAgICAgIH0sXG4gICAgICAgIGludmVyc2U6IGZ1bmN0aW9uIGVtcHR5KCkge1xuICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgfSxcbiAgICAgICAgcm9vdDogcm9vdCxcbiAgICAgICAgcGFyZW50czogW2N0eF8xXVxuICAgICAgfSk7XG4gICAgICByICs9ICc8L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj4nO1xuICAgICAgcmV0dXJuIHI7XG4gICAgfSh0aGlzKTtcbiAgfVxuXG4gIC8vIGhvb2tzXG4gIGJlZm9yZUNyZWF0ZSgpIHtcbiAgICAvLyBCaW5kIGV2ZW50IGhhbmRsZXJzIHRvIGNvbXBvbmVudCBjb250ZXh0XG4gICAgdGhpcy51bmxvY2tDbGljayA9IHRoaXMudW5sb2NrQ2xpY2suYmluZCh0aGlzKTtcbiAgfVxuXG4gIG1vdW50ZWQoKSB7fVxuXG4gIGJlZm9yZURlc3Ryb3koKSB7fSAvLyBtZXRob2RzXG5cblxuICB1bmxvY2tDbGljayhlKSB7XG4gICAgdGhpcy5zdWJtaXRGb3JtKGUsIFwiI3VubG9jay13YWxsZXQtZm9ybVwiKTtcbiAgfVxuXG59XG5cbkY3UGFnZUNvbXBvbmVudC5pZCA9ICdjNzlmNzJlNGZkJztcbkY3UGFnZUNvbXBvbmVudC5zdHlsZVNjb3BlZCA9IGZhbHNlO1xuZXhwb3J0IGRlZmF1bHQgRjdQYWdlQ29tcG9uZW50O1xuICAgICIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG5pbXBvcnQgeyBVaVNlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS91aS1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENvbm5lY3RTZXJ2aWNlIH0gZnJvbSBcIi4uL3NlcnZpY2UvY29ubmVjdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IHJvdXRlTWFwIH0gZnJvbSBcIi4uL2RlY29yYXRvcnMvcm91dGUtbWFwXCI7XHJcbmltcG9ydCB7IE1vZGVsVmlldyB9IGZyb20gXCIuLi91dGlsL21vZGVsLXZpZXdcIjtcclxuaW1wb3J0IENvbm5lY3RJbmRleENvbXBvbmVudCBmcm9tICcuLi9jb21wb25lbnRzL2Nvbm5lY3QvaW5kZXguZjcuaHRtbCc7XHJcbmltcG9ydCBjb250cm9sbGVyIGZyb20gXCIuLi9kZWNvcmF0b3JzL2NvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgUm91dGluZ1NlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9yb3V0aW5nLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU2NoZW1hU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlL3NjaGVtYS1zZXJ2aWNlXCI7XHJcbmxldCBDb25uZWN0Q29udHJvbGxlciA9IGNsYXNzIENvbm5lY3RDb250cm9sbGVyIHtcclxuICAgIGNvbnN0cnVjdG9yKGNvbm5lY3RTZXJ2aWNlLCB1aVNlcnZpY2UsIHJvdXRpbmdTZXJ2aWNlLCBzY2hlbWFTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5jb25uZWN0U2VydmljZSA9IGNvbm5lY3RTZXJ2aWNlO1xyXG4gICAgICAgIHRoaXMudWlTZXJ2aWNlID0gdWlTZXJ2aWNlO1xyXG4gICAgICAgIHRoaXMucm91dGluZ1NlcnZpY2UgPSByb3V0aW5nU2VydmljZTtcclxuICAgICAgICB0aGlzLnNjaGVtYVNlcnZpY2UgPSBzY2hlbWFTZXJ2aWNlO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgaW5kZXgoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBNb2RlbFZpZXcoYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgdm0gPSB7XHJcbiAgICAgICAgICAgICAgICBjb25uZWN0OiBhd2FpdCB0aGlzLmNvbm5lY3RTZXJ2aWNlLmdldENvbm5lY3RJbmZvKCksXHJcbiAgICAgICAgICAgICAgICBzY2hlbWE6IGF3YWl0IHRoaXMuc2NoZW1hU2VydmljZS5nZXRTY2hlbWFJbmZvKClcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgcmV0dXJuIHZtO1xyXG4gICAgICAgIH0sIENvbm5lY3RJbmRleENvbXBvbmVudCk7XHJcbiAgICB9XHJcbiAgICBhc3luYyBhZGRQZWVyKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgTW9kZWxWaWV3KGFzeW5jIChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmNvbm5lY3RTZXJ2aWNlLmFkZFBlZXIoZGF0YS5wZWVyQWRkcmVzcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2F0Y2ggKGV4KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93UG9wdXAoZXgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMucm91dGluZ1NlcnZpY2UubmF2aWdhdGUoe1xyXG4gICAgICAgICAgICAgICAgcGF0aDogXCIvY29ubmVjdFwiXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sIENvbm5lY3RJbmRleENvbXBvbmVudCk7XHJcbiAgICB9XHJcbn07XHJcbl9fZGVjb3JhdGUoW1xyXG4gICAgcm91dGVNYXAoXCIvY29ubmVjdFwiKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246dHlwZVwiLCBGdW5jdGlvbiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpyZXR1cm50eXBlXCIsIFByb21pc2UpXHJcbl0sIENvbm5lY3RDb250cm9sbGVyLnByb3RvdHlwZSwgXCJpbmRleFwiLCBudWxsKTtcclxuX19kZWNvcmF0ZShbXHJcbiAgICByb3V0ZU1hcChcIi9jb25uZWN0L2FkZFBlZXJcIiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnR5cGVcIiwgRnVuY3Rpb24pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtdKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cmV0dXJudHlwZVwiLCBQcm9taXNlKVxyXG5dLCBDb25uZWN0Q29udHJvbGxlci5wcm90b3R5cGUsIFwiYWRkUGVlclwiLCBudWxsKTtcclxuQ29ubmVjdENvbnRyb2xsZXIgPSBfX2RlY29yYXRlKFtcclxuICAgIGNvbnRyb2xsZXIoKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbQ29ubmVjdFNlcnZpY2UsXHJcbiAgICAgICAgVWlTZXJ2aWNlLFxyXG4gICAgICAgIFJvdXRpbmdTZXJ2aWNlLFxyXG4gICAgICAgIFNjaGVtYVNlcnZpY2VdKVxyXG5dLCBDb25uZWN0Q29udHJvbGxlcik7XHJcbmV4cG9ydCB7IENvbm5lY3RDb250cm9sbGVyIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG52YXIgX19wYXJhbSA9ICh0aGlzICYmIHRoaXMuX19wYXJhbSkgfHwgZnVuY3Rpb24gKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn07XHJcbmltcG9ydCB7IGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IHsgV2FsbGV0U2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlL3dhbGxldC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFVpU2VydmljZSB9IGZyb20gXCIuLi9zZXJ2aWNlL3VpLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTW9kZWxWaWV3IH0gZnJvbSBcIi4uL3V0aWwvbW9kZWwtdmlld1wiO1xyXG5pbXBvcnQgTGFuZGluZ0NvbXBvbmVudCBmcm9tICcuLi9jb21wb25lbnRzL3dhbGxldC9sYW5kaW5nLmY3Lmh0bWwnO1xyXG5pbXBvcnQgRW50ZXJSZWNvdmVyeUNvbXBvbmVudCBmcm9tICcuLi9jb21wb25lbnRzL3dhbGxldC9lbnRlci1yZWNvdmVyeS5mNy5odG1sJztcclxuaW1wb3J0IENyZWF0ZVdhbGxldENvbXBvbmVudCBmcm9tICcuLi9jb21wb25lbnRzL3dhbGxldC9jcmVhdGUtd2FsbGV0LmY3Lmh0bWwnO1xyXG5pbXBvcnQgQ3JlYXRlV2FsbGV0U3VjY2Vzc0NvbXBvbmVudCBmcm9tICcuLi9jb21wb25lbnRzL3dhbGxldC9jcmVhdGUtd2FsbGV0LXN1Y2Nlc3MuZjcuaHRtbCc7XHJcbmltcG9ydCB7IHJvdXRlTWFwIH0gZnJvbSBcIi4uL2RlY29yYXRvcnMvcm91dGUtbWFwXCI7XHJcbmltcG9ydCBjb250cm9sbGVyIGZyb20gXCIuLi9kZWNvcmF0b3JzL2NvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgUm91dGluZ1NlcnZpY2UgfSBmcm9tIFwiLi4vc2VydmljZS9yb3V0aW5nLXNlcnZpY2VcIjtcclxuaW1wb3J0IFNwYWNlTVZDIGZyb20gXCIuLlwiO1xyXG5sZXQgV2FsbGV0Q29udHJvbGxlciA9IGNsYXNzIFdhbGxldENvbnRyb2xsZXIge1xyXG4gICAgY29uc3RydWN0b3Iod2FsbGV0U2VydmljZSwgdWlTZXJ2aWNlLCByb3V0aW5nU2VydmljZSwgb3duZXJBZGRyZXNzLCBzdG9yZURlZmluaXRpb25zKSB7XHJcbiAgICAgICAgdGhpcy53YWxsZXRTZXJ2aWNlID0gd2FsbGV0U2VydmljZTtcclxuICAgICAgICB0aGlzLnVpU2VydmljZSA9IHVpU2VydmljZTtcclxuICAgICAgICB0aGlzLnJvdXRpbmdTZXJ2aWNlID0gcm91dGluZ1NlcnZpY2U7XHJcbiAgICAgICAgdGhpcy5vd25lckFkZHJlc3MgPSBvd25lckFkZHJlc3M7XHJcbiAgICAgICAgdGhpcy5zdG9yZURlZmluaXRpb25zID0gc3RvcmVEZWZpbml0aW9ucztcclxuICAgIH1cclxuICAgIGFzeW5jIF9pbml0QXBwKCkge1xyXG4gICAgICAgIHRoaXMudWlTZXJ2aWNlLnNob3dTcGlubmVyKCk7XHJcbiAgICAgICAgYXdhaXQgU3BhY2VNVkMuaW5pdEZpbmlzaCh0aGlzLm93bmVyQWRkcmVzcywgdGhpcy5zdG9yZURlZmluaXRpb25zKTtcclxuICAgICAgICB0aGlzLnVpU2VydmljZS5oaWRlU3Bpbm5lcigpO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgc2hvd0xhbmRpbmcoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBNb2RlbFZpZXcoYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgZXhpc3RpbmdXYWxsZXQgPSBhd2FpdCB0aGlzLndhbGxldFNlcnZpY2UuZ2V0V2FsbGV0KCk7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBzaG93VW5sb2NrOiAoZXhpc3RpbmdXYWxsZXQpLFxyXG4gICAgICAgICAgICAgICAgYXBwTmFtZTogU3BhY2VNVkMubmFtZSgpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSwgTGFuZGluZ0NvbXBvbmVudCk7XHJcbiAgICB9XHJcbiAgICBhc3luYyBzaG93Q3JlYXRlV2FsbGV0KCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgTW9kZWxWaWV3KGFzeW5jICgpID0+IHtcclxuICAgICAgICB9LCBDcmVhdGVXYWxsZXRDb21wb25lbnQpO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgY3JlYXRlV2FsbGV0KCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgTW9kZWxWaWV3KGFzeW5jIChmb3JtRGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAvL0NyZWF0ZSBuZXcgd2FsbGV0XHJcbiAgICAgICAgICAgIGxldCBtbmVtb25pYyA9IGF3YWl0IHRoaXMud2FsbGV0U2VydmljZS5jcmVhdGVXYWxsZXQoZm9ybURhdGEubmV3UGFzc3dvcmQpO1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRpbmdTZXJ2aWNlLm5hdmlnYXRlKHtcclxuICAgICAgICAgICAgICAgIHBhdGg6IFwiL2NyZWF0ZVdhbGxldFN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICBjb250ZXh0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbW5lbW9uaWM6IG1uZW1vbmljXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgY3JlYXRlV2FsbGV0U3VjY2VzcygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE1vZGVsVmlldyhhc3luYyAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICB9LCBDcmVhdGVXYWxsZXRTdWNjZXNzQ29tcG9uZW50KTtcclxuICAgIH1cclxuICAgIGFzeW5jIGNyZWF0ZVdhbGxldERvbmUoKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBNb2RlbFZpZXcoYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5faW5pdEFwcCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhdGNoIChleCkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51aVNlcnZpY2UuaGlkZVNwaW5uZXIoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudWlTZXJ2aWNlLnNob3dQb3B1cChcIlVuYWJsZSB0byBpbml0aWFsaXplIGFwcFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgdW5sb2NrV2FsbGV0KCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgTW9kZWxWaWV3KGFzeW5jIChmb3JtRGF0YSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93U3Bpbm5lcignVW5sb2NraW5nIHdhbGxldC4uLicpO1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy53YWxsZXRTZXJ2aWNlLnVubG9ja1dhbGxldChmb3JtRGF0YS5wYXNzd29yZCk7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9pbml0QXBwKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2F0Y2ggKGV4KSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhleCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVpU2VydmljZS5oaWRlU3Bpbm5lcigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51aVNlcnZpY2Uuc2hvd1BvcHVwKFwiSW5jb3JyZWN0IHBhc3N3b3JkIHBsZWFzZSB0cnkgYWdhaW4uXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBhc3luYyBzaG93RW50ZXJSZWNvdmVyeSgpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE1vZGVsVmlldyhhc3luYyAoKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICB9LCBFbnRlclJlY292ZXJ5Q29tcG9uZW50KTtcclxuICAgIH1cclxuICAgIGFzeW5jIHJlc3RvcmVCdXR0b25DbGljaygpIHtcclxuICAgICAgICByZXR1cm4gbmV3IE1vZGVsVmlldyhhc3luYyAoZm9ybURhdGEpID0+IHtcclxuICAgICAgICAgICAgdGhpcy51aVNlcnZpY2Uuc2hvd1NwaW5uZXIoJ1Jlc3RvcmluZyB3YWxsZXQuLi4nKTtcclxuICAgICAgICAgICAgbGV0IHdhbGxldCA9IGF3YWl0IHRoaXMud2FsbGV0U2VydmljZS5yZXN0b3JlV2FsbGV0KGZvcm1EYXRhLnJlY292ZXJ5U2VlZCwgZm9ybURhdGEubmV3UGFzc3dvcmQpO1xyXG4gICAgICAgICAgICBpZiAod2FsbGV0KSB7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLl9pbml0QXBwKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93UG9wdXAoXCJFcnJvciByZXN0b3Jpbmcgd2FsbGV0LiBQbGVhc2UgdHJ5IGFnYWluLlwiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMudWlTZXJ2aWNlLmhpZGVTcGlubmVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufTtcclxuX19kZWNvcmF0ZShbXHJcbiAgICByb3V0ZU1hcChcIi93YWxsZXRcIiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnR5cGVcIiwgRnVuY3Rpb24pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtdKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cmV0dXJudHlwZVwiLCBQcm9taXNlKVxyXG5dLCBXYWxsZXRDb250cm9sbGVyLnByb3RvdHlwZSwgXCJzaG93TGFuZGluZ1wiLCBudWxsKTtcclxuX19kZWNvcmF0ZShbXHJcbiAgICByb3V0ZU1hcChcIi9zaG93Q3JlYXRlV2FsbGV0XCIpLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjp0eXBlXCIsIEZ1bmN0aW9uKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbXSksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnJldHVybnR5cGVcIiwgUHJvbWlzZSlcclxuXSwgV2FsbGV0Q29udHJvbGxlci5wcm90b3R5cGUsIFwic2hvd0NyZWF0ZVdhbGxldFwiLCBudWxsKTtcclxuX19kZWNvcmF0ZShbXHJcbiAgICByb3V0ZU1hcChcIi9jcmVhdGVXYWxsZXRcIiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnR5cGVcIiwgRnVuY3Rpb24pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtdKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cmV0dXJudHlwZVwiLCBQcm9taXNlKVxyXG5dLCBXYWxsZXRDb250cm9sbGVyLnByb3RvdHlwZSwgXCJjcmVhdGVXYWxsZXRcIiwgbnVsbCk7XHJcbl9fZGVjb3JhdGUoW1xyXG4gICAgcm91dGVNYXAoXCIvY3JlYXRlV2FsbGV0U3VjY2Vzc1wiKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246dHlwZVwiLCBGdW5jdGlvbiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpyZXR1cm50eXBlXCIsIFByb21pc2UpXHJcbl0sIFdhbGxldENvbnRyb2xsZXIucHJvdG90eXBlLCBcImNyZWF0ZVdhbGxldFN1Y2Nlc3NcIiwgbnVsbCk7XHJcbl9fZGVjb3JhdGUoW1xyXG4gICAgcm91dGVNYXAoXCIvY3JlYXRlV2FsbGV0RG9uZVwiKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246dHlwZVwiLCBGdW5jdGlvbiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpyZXR1cm50eXBlXCIsIFByb21pc2UpXHJcbl0sIFdhbGxldENvbnRyb2xsZXIucHJvdG90eXBlLCBcImNyZWF0ZVdhbGxldERvbmVcIiwgbnVsbCk7XHJcbl9fZGVjb3JhdGUoW1xyXG4gICAgcm91dGVNYXAoXCIvdW5sb2NrV2FsbGV0XCIpLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjp0eXBlXCIsIEZ1bmN0aW9uKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbXSksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnJldHVybnR5cGVcIiwgUHJvbWlzZSlcclxuXSwgV2FsbGV0Q29udHJvbGxlci5wcm90b3R5cGUsIFwidW5sb2NrV2FsbGV0XCIsIG51bGwpO1xyXG5fX2RlY29yYXRlKFtcclxuICAgIHJvdXRlTWFwKFwiL2VudGVyUmVjb3ZlcnlcIiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnR5cGVcIiwgRnVuY3Rpb24pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtdKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cmV0dXJudHlwZVwiLCBQcm9taXNlKVxyXG5dLCBXYWxsZXRDb250cm9sbGVyLnByb3RvdHlwZSwgXCJzaG93RW50ZXJSZWNvdmVyeVwiLCBudWxsKTtcclxuX19kZWNvcmF0ZShbXHJcbiAgICByb3V0ZU1hcChcIi9yZXN0b3JlQWNjb3VudFwiKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246dHlwZVwiLCBGdW5jdGlvbiksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpyZXR1cm50eXBlXCIsIFByb21pc2UpXHJcbl0sIFdhbGxldENvbnRyb2xsZXIucHJvdG90eXBlLCBcInJlc3RvcmVCdXR0b25DbGlja1wiLCBudWxsKTtcclxuV2FsbGV0Q29udHJvbGxlciA9IF9fZGVjb3JhdGUoW1xyXG4gICAgY29udHJvbGxlcigpLFxyXG4gICAgX19wYXJhbSgzLCBpbmplY3QoXCJvd25lckFkZHJlc3NcIikpLFxyXG4gICAgX19wYXJhbSg0LCBpbmplY3QoXCJzdG9yZURlZmluaXRpb25zXCIpKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbV2FsbGV0U2VydmljZSxcclxuICAgICAgICBVaVNlcnZpY2UsXHJcbiAgICAgICAgUm91dGluZ1NlcnZpY2UsIFN0cmluZywgQXJyYXldKVxyXG5dLCBXYWxsZXRDb250cm9sbGVyKTtcclxuZXhwb3J0IHsgV2FsbGV0Q29udHJvbGxlciB9O1xyXG4iLCJ2YXIgX19kZWNvcmF0ZSA9ICh0aGlzICYmIHRoaXMuX19kZWNvcmF0ZSkgfHwgZnVuY3Rpb24gKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59O1xyXG52YXIgX19tZXRhZGF0YSA9ICh0aGlzICYmIHRoaXMuX19tZXRhZGF0YSkgfHwgZnVuY3Rpb24gKGssIHYpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcclxufTtcclxuaW1wb3J0IHsgaW5qZWN0YWJsZSB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuY29uc3QgbGV2ZWx1cCA9IHJlcXVpcmUoJ2xldmVsdXAnKTtcclxuY29uc3QgbGV2ZWxqcyA9IHJlcXVpcmUoJ2xldmVsLWpzJyk7XHJcbmxldCBXYWxsZXREYW8gPSBjbGFzcyBXYWxsZXREYW8ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5za2lwQmluZGluZyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kYiA9IGxldmVsdXAobGV2ZWxqcygnc3BhY2VtdmMtd2FsbGV0JykpO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgc2F2ZVdhbGxldCh3YWxsZXQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kYi5wdXQoJ3dhbGxldCcsIEJ1ZmZlci5mcm9tKEpTT04uc3RyaW5naWZ5KHdhbGxldCkpKTtcclxuICAgIH1cclxuICAgIGFzeW5jIGxvYWRXYWxsZXQoKSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBhd2FpdCB0aGlzLmRiLmdldCgnd2FsbGV0Jyk7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UodmFsdWUudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgY2F0Y2ggKGV4KSB7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKGV4KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufTtcclxuV2FsbGV0RGFvID0gX19kZWNvcmF0ZShbXHJcbiAgICBpbmplY3RhYmxlKCksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pXHJcbl0sIFdhbGxldERhbyk7XHJcbmV4cG9ydCB7IFdhbGxldERhbyB9O1xyXG4iLCJpbXBvcnQgeyBpbmplY3RhYmxlIH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5mdW5jdGlvbiBjb250cm9sbGVyKCkge1xyXG4gICAgY29uc3QgaW5qZWN0YWJsZURlY29yYXRvciA9IGluamVjdGFibGUoKTtcclxuICAgIHJldHVybiBmdW5jdGlvbiAoY29udHJvbGxlclR5cGUpIHtcclxuICAgICAgICBpbmplY3RhYmxlRGVjb3JhdG9yKGNvbnRyb2xsZXJUeXBlKTtcclxuICAgIH07XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgY29udHJvbGxlcjtcclxuIiwiaW1wb3J0IHsgaW5qZWN0YWJsZSB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuZnVuY3Rpb24gZGFvKGFyZ3MgPSB7fSkge1xyXG4gICAgY29uc3QgaW5qZWN0YWJsZURlY29yYXRvciA9IGluamVjdGFibGUoKTtcclxuICAgIHJldHVybiBmdW5jdGlvbiAoY29udHJvbGxlclR5cGUpIHtcclxuICAgICAgICBpbmplY3RhYmxlRGVjb3JhdG9yKGNvbnRyb2xsZXJUeXBlKTtcclxuICAgIH07XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgZGFvO1xyXG4iLCJmdW5jdGlvbiByb3V0ZU1hcCh2YWx1ZSkge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIHByb3BlcnR5S2V5LCBkZXNjcmlwdG9yKSB7XHJcbiAgICAgICAgaWYgKCFnbG9iYWxUaGlzLm1hcHBlZFJvdXRlcylcclxuICAgICAgICAgICAgZ2xvYmFsVGhpcy5tYXBwZWRSb3V0ZXMgPSBbXTtcclxuICAgICAgICBnbG9iYWxUaGlzLm1hcHBlZFJvdXRlcy5wdXNoKHtcclxuICAgICAgICAgICAgcGF0aDogdmFsdWUsXHJcbiAgICAgICAgICAgIGNvbnRyb2xsZXJDbGFzczogdGFyZ2V0LmNvbnN0cnVjdG9yLFxyXG4gICAgICAgICAgICBhY3Rpb246IHByb3BlcnR5S2V5XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59XHJcbmV4cG9ydCB7IHJvdXRlTWFwIH07XHJcbiIsImltcG9ydCB7IGluamVjdGFibGUgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmZ1bmN0aW9uIHNlcnZpY2UoYXJncyA9IHt9KSB7XHJcbiAgICBjb25zdCBpbmplY3RhYmxlRGVjb3JhdG9yID0gaW5qZWN0YWJsZSgpO1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb250cm9sbGVyVHlwZSkge1xyXG4gICAgICAgIGluamVjdGFibGVEZWNvcmF0b3IoY29udHJvbGxlclR5cGUpO1xyXG4gICAgfTtcclxufVxyXG5leHBvcnQgZGVmYXVsdCBzZXJ2aWNlO1xyXG4iLCJpbXBvcnQgXCJyZWZsZWN0LW1ldGFkYXRhXCI7XHJcbmltcG9ydCB7IE1vZGVsVmlldyB9IGZyb20gXCIuL3V0aWwvbW9kZWwtdmlld1wiO1xyXG5pbXBvcnQgeyBQcm9taXNlVmlldyB9IGZyb20gXCIuL3V0aWwvcHJvbWlzZS12aWV3XCI7XHJcbmltcG9ydCB7IFF1ZXVlU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvcXVldWVfc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTcGFjZU1WQyB9IGZyb20gXCIuL3NwYWNlLW12Y1wiO1xyXG5pbXBvcnQgeyBVaVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3VpLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUGFnaW5nU2VydmljZSwgUGFnaW5nVmlld01vZGVsIH0gZnJvbSBcIi4vc2VydmljZS9wYWdpbmctc2VydmljZVwiO1xyXG5pbXBvcnQgeyBXYWxsZXRTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS93YWxsZXQtc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTY2hlbWFTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9zY2hlbWEtc2VydmljZVwiO1xyXG5pbXBvcnQgeyBpbmplY3QsIENvbnRhaW5lciB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IHsgSXBmc1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL2lwZnMtc2VydmljZVwiO1xyXG5pbXBvcnQgeyBJbnZhbGlkV2FsbGV0RXhjZXB0aW9uIH0gZnJvbSBcIi4vc2VydmljZS9leGNlcHRpb24vaW52YWxpZC13YWxsZXQtZXhjZXB0aW9uXCI7XHJcbmltcG9ydCB7IFdhbGxldENvbnRyb2xsZXIgfSBmcm9tIFwiLi9jb250cm9sbGVyL3dhbGxldC1jb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IFJvdXRpbmdTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9yb3V0aW5nLXNlcnZpY2VcIjtcclxuaW1wb3J0IGNvbnRyb2xsZXIgZnJvbSBcIi4vZGVjb3JhdG9ycy9jb250cm9sbGVyXCI7XHJcbmltcG9ydCBzZXJ2aWNlIGZyb20gXCIuL2RlY29yYXRvcnMvc2VydmljZVwiO1xyXG5pbXBvcnQgeyByb3V0ZU1hcCB9IGZyb20gXCIuL2RlY29yYXRvcnMvcm91dGUtbWFwXCI7XHJcbmltcG9ydCBkYW8gZnJvbSBcIi4vZGVjb3JhdG9ycy9kYW9cIjtcclxuaW1wb3J0IHsgU3BhY2VDb21wb25lbnQgfSBmcm9tIFwiLi91dGlsL3NwYWNlLWNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBPcmJpdFNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL29yYml0LXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgaW5pdFRlc3RDb250YWluZXIgfSBmcm9tIFwiLi4vdGVzdC90ZXN0LWludmVyc2lmeS5jb25maWdcIjtcclxuZXhwb3J0IHsgUXVldWVTZXJ2aWNlLCBQYWdpbmdTZXJ2aWNlLCBQYWdpbmdWaWV3TW9kZWwsIFVpU2VydmljZSwgV2FsbGV0U2VydmljZSwgSW52YWxpZFdhbGxldEV4Y2VwdGlvbiwgU2NoZW1hU2VydmljZSwgSXBmc1NlcnZpY2UsIE9yYml0U2VydmljZSwgUm91dGluZ1NlcnZpY2UsIFdhbGxldENvbnRyb2xsZXIsIE1vZGVsVmlldywgUHJvbWlzZVZpZXcsIENvbnRhaW5lciwgU3BhY2VDb21wb25lbnQsIGluamVjdCwgc2VydmljZSwgY29udHJvbGxlciwgZGFvLCByb3V0ZU1hcCwgaW5pdFRlc3RDb250YWluZXIgfTtcclxuZXhwb3J0IGRlZmF1bHQgU3BhY2VNVkM7XHJcbiIsImltcG9ydCB7IFVpU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvdWktc2VydmljZVwiO1xyXG5pbXBvcnQgeyBQYWdpbmdTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9wYWdpbmctc2VydmljZVwiO1xyXG5pbXBvcnQgeyBJZGVudGl0eVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL2lkZW50aXR5LXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgV2FsbGV0U2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2Uvd2FsbGV0LXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU2NoZW1hU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2Uvc2NoZW1hLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgT3JiaXRTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9vcmJpdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IElwZnNTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9pcGZzLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgV2FsbGV0RGFvIH0gZnJvbSBcIi4vZGFvL3dhbGxldC1kYW9cIjtcclxuaW1wb3J0IHsgV2FsbGV0Q29udHJvbGxlciB9IGZyb20gXCIuL2NvbnRyb2xsZXIvd2FsbGV0LWNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgUm91dGluZ1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3JvdXRpbmctc2VydmljZVwiO1xyXG5pbXBvcnQgeyBRdWV1ZVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3F1ZXVlX3NlcnZpY2VcIjtcclxuaW1wb3J0IHsgQ29ubmVjdENvbnRyb2xsZXIgfSBmcm9tIFwiLi9jb250cm9sbGVyL2Nvbm5lY3QtY29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBDb25uZWN0U2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvY29ubmVjdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEJpbmRpbmdTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9iaW5kaW5nLXNlcnZpY2VcIjtcclxuZXhwb3J0IGZ1bmN0aW9uIGJ1aWxkQ29udGFpbmVyKGNvbnRhaW5lciwgY29uZmlnKSB7XHJcbiAgICBmdW5jdGlvbiBmcmFtZXdvcms3KCkge1xyXG4gICAgICAgIGNvbnN0IEZyYW1ld29yazcgPSByZXF1aXJlKCdmcmFtZXdvcms3L2pzL2ZyYW1ld29yazcuYnVuZGxlJyk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBGcmFtZXdvcms3KGNvbmZpZyk7XHJcbiAgICB9XHJcbiAgICBjb250YWluZXIuYmluZChcImZyYW1ld29yazdcIikudG9Db25zdGFudFZhbHVlKGZyYW1ld29yazcoKSk7XHJcbiAgICAvL0RlZmF1bHQgSVBGUyBvcHRpb25zIGlmIHdlJ3JlIG5vdCBnaXZlbiBhbnlcclxuICAgIGlmICghY29udGFpbmVyLmlzQm91bmQoXCJpcGZzT3B0aW9uc1wiKSkge1xyXG4gICAgICAgIGNvbnRhaW5lci5iaW5kKFwiaXBmc09wdGlvbnNcIikudG9Db25zdGFudFZhbHVlKHtcclxuICAgICAgICAgICAgcmVwbzogYHNwYWNlLW12Yy1kZWZhdWx0YCxcclxuICAgICAgICAgICAgRVhQRVJJTUVOVEFMOiB7XHJcbiAgICAgICAgICAgICAgICBpcG5zUHVic3ViOiB0cnVlXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHByZWxvYWQ6IHtcclxuICAgICAgICAgICAgICAgIGVuYWJsZWQ6IGZhbHNlXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJlbGF5OiB7XHJcbiAgICAgICAgICAgICAgICBlbmFibGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgaG9wOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW5hYmxlZDogdHJ1ZSAvLyBlbmFibGUgY2lyY3VpdCByZWxheSBIT1AgKG1ha2UgdGhpcyBub2RlIGEgcmVsYXkpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgQWRkcmVzc2VzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgU3dhcm06IFtdXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICghY29udGFpbmVyLmlzQm91bmQoXCJvcmJpdERiT3B0aW9uc1wiKSkge1xyXG4gICAgICAgIGNvbnRhaW5lci5iaW5kKFwib3JiaXREYk9wdGlvbnNcIikudG9Db25zdGFudFZhbHVlKHt9KTtcclxuICAgIH1cclxuICAgIGNvbnRhaW5lci5iaW5kKElkZW50aXR5U2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoV2FsbGV0RGFvKS50b1NlbGYoKS5pblNpbmdsZXRvblNjb3BlKCk7XHJcbiAgICBjb250YWluZXIuYmluZChVaVNlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIGNvbnRhaW5lci5iaW5kKFF1ZXVlU2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoUGFnaW5nU2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoU2NoZW1hU2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoT3JiaXRTZXJ2aWNlKS50b1NlbGYoKS5pblNpbmdsZXRvblNjb3BlKCk7XHJcbiAgICBjb250YWluZXIuYmluZChJcGZzU2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoV2FsbGV0U2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoV2FsbGV0Q29udHJvbGxlcikudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoQ29ubmVjdFNlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIGNvbnRhaW5lci5iaW5kKENvbm5lY3RDb250cm9sbGVyKS50b1NlbGYoKS5pblNpbmdsZXRvblNjb3BlKCk7XHJcbiAgICBjb250YWluZXIuYmluZChSb3V0aW5nU2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoQmluZGluZ1NlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIHJldHVybiBjb250YWluZXI7XHJcbn1cclxuIiwidmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufTtcclxuaW1wb3J0IHNlcnZpY2UgZnJvbSBcIi4uL2RlY29yYXRvcnMvc2VydmljZVwiO1xyXG5sZXQgQmluZGluZ1NlcnZpY2UgPSBjbGFzcyBCaW5kaW5nU2VydmljZSB7XHJcbiAgICBhc3luYyBpbml0QmluZGluZ3MoY29udGFpbmVyKSB7XHJcbiAgICAgICAgbGV0IGluaXRpbGl6YWJsZUJpbmRpbmdzID0gdGhpcy5fZ2V0SW5pdGlhbGl6YWJsZUJpbmRpbmdzKGNvbnRhaW5lcik7XHJcbiAgICAgICAgY29uc29sZS5sb2coaW5pdGlsaXphYmxlQmluZGluZ3MpO1xyXG4gICAgICAgIGZvciAobGV0IGJlYW4gb2YgaW5pdGlsaXphYmxlQmluZGluZ3MpIHtcclxuICAgICAgICAgICAgYXdhaXQgYmVhbi5pbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgX2dldEluaXRpYWxpemFibGVCaW5kaW5ncyhjb250YWluZXIpIHtcclxuICAgICAgICBsZXQgYmluZGluZ3MgPSBbXTtcclxuICAgICAgICAvL1RPRE86IHByb2JhYmx5IHRoaXMgc2hvdWxkbid0IGJlIGEgaGFyZGNvZGVkIGxpc3RcclxuICAgICAgICBsZXQgc2VydmljZUlkZW50aWZpZXJzID0gdGhpcy5fZ2V0U2VydmljZUlkZW50aWZpZXJzKGNvbnRhaW5lcik7XHJcbiAgICAgICAgc2VydmljZUlkZW50aWZpZXJzID0gc2VydmljZUlkZW50aWZpZXJzLmZpbHRlcihpZCA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc1NraXBCaW5kaW5nKGNvbnRhaW5lciwgaWQpKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAoaWQgPT0gXCJmcmFtZXdvcms3XCIpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0luaXRpYWxpemFibGUoY29udGFpbmVyLCBpZCkpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGZvciAobGV0IHNpIG9mIHNlcnZpY2VJZGVudGlmaWVycykge1xyXG4gICAgICAgICAgICBiaW5kaW5ncy5wdXNoKGNvbnRhaW5lci5nZXQoc2kpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGJpbmRpbmdzO1xyXG4gICAgfVxyXG4gICAgX2dldFNlcnZpY2VJZGVudGlmaWVycyhjb250YWluZXIpIHtcclxuICAgICAgICAvL0B0cy1pZ25vcmVcclxuICAgICAgICBsZXQgb3JpZ2luYWwgPSBjb250YWluZXIuX2JpbmRpbmdEaWN0aW9uYXJ5O1xyXG4gICAgICAgIGxldCBzZXJ2aWNlSWRlbnRpZmllcnMgPSBbXTtcclxuICAgICAgICBvcmlnaW5hbC50cmF2ZXJzZSgoa2V5LCB2YWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICB2YWx1ZS5mb3JFYWNoKChiaW5kaW5nKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZXJ2aWNlSWRlbnRpZmllcnMucHVzaChiaW5kaW5nLnNlcnZpY2VJZGVudGlmaWVyKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHNlcnZpY2VJZGVudGlmaWVycztcclxuICAgIH1cclxuICAgIF9pc0luaXRpYWxpemFibGUoY29udGFpbmVyLCBvYmopIHtcclxuICAgICAgICBsZXQgYmVhbiA9IGNvbnRhaW5lci5nZXQob2JqKTtcclxuICAgICAgICByZXR1cm4gYmVhbi5pbml0ICE9IHVuZGVmaW5lZDtcclxuICAgIH1cclxuICAgIF9pc1NraXBCaW5kaW5nKGNvbnRhaW5lciwgb2JqKSB7XHJcbiAgICAgICAgbGV0IGJlYW4gPSBjb250YWluZXIuZ2V0KG9iaik7XHJcbiAgICAgICAgcmV0dXJuIGJlYW4uc2tpcEJpbmRpbmcgPT0gdHJ1ZTtcclxuICAgIH1cclxufTtcclxuQmluZGluZ1NlcnZpY2UgPSBfX2RlY29yYXRlKFtcclxuICAgIHNlcnZpY2UoKVxyXG5dLCBCaW5kaW5nU2VydmljZSk7XHJcbmV4cG9ydCB7IEJpbmRpbmdTZXJ2aWNlIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG5pbXBvcnQgeyBJcGZzU2VydmljZSB9IGZyb20gXCIuL2lwZnMtc2VydmljZVwiO1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmxldCBDb25uZWN0U2VydmljZSA9IGNsYXNzIENvbm5lY3RTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKGlwZnNTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5pcGZzU2VydmljZSA9IGlwZnNTZXJ2aWNlO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgZ2V0Q29ubmVjdEluZm8oKSB7XHJcbiAgICAgICAgbGV0IHBlZXJzID0gYXdhaXQgdGhpcy5pcGZzU2VydmljZS5pcGZzLnN3YXJtLnBlZXJzKCk7XHJcbiAgICAgICAgcGVlcnMgPSBwZWVycy5tYXAoZSA9PiBlLmFkZHIudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgbGV0IHN1YnNjcmliZWQgPSBhd2FpdCB0aGlzLmlwZnNTZXJ2aWNlLmlwZnMucHVic3ViLmxzKCk7XHJcbiAgICAgICAgbGV0IGlkID0gYXdhaXQgdGhpcy5pcGZzU2VydmljZS5pcGZzLmlkKCk7XHJcbiAgICAgICAgbGV0IGlwZnNJbmZvID0ge1xyXG4gICAgICAgICAgICBhZGRyZXNzZXM6IGlkLmFkZHJlc3Nlcy5tYXAoZSA9PiBlLnRvU3RyaW5nKCkpLFxyXG4gICAgICAgICAgICBhZ2VudFZlcnNpb246IGlkLmFnZW50VmVyc2lvbixcclxuICAgICAgICAgICAgaWQ6IGlkLmlkLFxyXG4gICAgICAgICAgICBwdWJsaWNLZXk6IGlkLnB1YmxpY0tleVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcGVlcnM6IHBlZXJzLFxyXG4gICAgICAgICAgICBpcGZzSW5mbzogaXBmc0luZm8sXHJcbiAgICAgICAgICAgIHBlZXJDb3VudDogcGVlcnMubGVuZ3RoLFxyXG4gICAgICAgICAgICBzdWJzY3JpYmVkOiBzdWJzY3JpYmVkXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIGFzeW5jIGFkZFBlZXIocGVlckFkZHJlc3MpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pcGZzU2VydmljZS5pcGZzLnN3YXJtLmNvbm5lY3QocGVlckFkZHJlc3MpO1xyXG4gICAgfVxyXG59O1xyXG5Db25uZWN0U2VydmljZSA9IF9fZGVjb3JhdGUoW1xyXG4gICAgc2VydmljZSgpLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtJcGZzU2VydmljZV0pXHJcbl0sIENvbm5lY3RTZXJ2aWNlKTtcclxuZXhwb3J0IHsgQ29ubmVjdFNlcnZpY2UgfTtcclxuIiwiY2xhc3MgSW52YWxpZFdhbGxldEV4Y2VwdGlvbiBleHRlbmRzIEVycm9yIHtcclxufVxyXG5leHBvcnQgeyBJbnZhbGlkV2FsbGV0RXhjZXB0aW9uIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmNvbnN0IEV0aElkZW50aXR5UHJvdmlkZXIgPSByZXF1aXJlKCdvcmJpdC1kYi1pZGVudGl0eS1wcm92aWRlci9zcmMvZXRoZXJldW0taWRlbnRpdHktcHJvdmlkZXInKTtcclxuY29uc3QgSWRlbnRpdGllcyA9IHJlcXVpcmUoJ29yYml0LWRiLWlkZW50aXR5LXByb3ZpZGVyJyk7XHJcbmxldCBJZGVudGl0eVNlcnZpY2UgPSBjbGFzcyBJZGVudGl0eVNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgSWRlbnRpdGllcy5hZGRJZGVudGl0eVByb3ZpZGVyKEV0aElkZW50aXR5UHJvdmlkZXIpO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgZ2V0SWRlbnRpdHkoa2V5c3RvcmUsIHdhbGxldCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlkZW50aXR5KVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5pZGVudGl0eTtcclxuICAgICAgICBjb25zdCB0eXBlID0gRXRoSWRlbnRpdHlQcm92aWRlci50eXBlO1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIHR5cGU6IHR5cGUsXHJcbiAgICAgICAgICAgIGtleXN0b3JlOiBrZXlzdG9yZSxcclxuICAgICAgICAgICAgd2FsbGV0OiB3YWxsZXRcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuaWRlbnRpdHkgPSBhd2FpdCBJZGVudGl0aWVzLmNyZWF0ZUlkZW50aXR5KG9wdGlvbnMpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlkZW50aXR5O1xyXG4gICAgfVxyXG4gICAgZ2V0QWNjZXNzQ29udHJvbGxlcihvcmJpdGRiKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgd3JpdGU6IFsnKiddIC8vW29yYml0ZGIuaWRlbnRpdHkuaWRdXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufTtcclxuSWRlbnRpdHlTZXJ2aWNlID0gX19kZWNvcmF0ZShbXHJcbiAgICBzZXJ2aWNlKCksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pXHJcbl0sIElkZW50aXR5U2VydmljZSk7XHJcbmV4cG9ydCB7IElkZW50aXR5U2VydmljZSB9O1xyXG4iLCJ2YXIgX19kZWNvcmF0ZSA9ICh0aGlzICYmIHRoaXMuX19kZWNvcmF0ZSkgfHwgZnVuY3Rpb24gKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59O1xyXG52YXIgX19tZXRhZGF0YSA9ICh0aGlzICYmIHRoaXMuX19tZXRhZGF0YSkgfHwgZnVuY3Rpb24gKGssIHYpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcclxufTtcclxudmFyIF9fcGFyYW0gPSAodGhpcyAmJiB0aGlzLl9fcGFyYW0pIHx8IGZ1bmN0aW9uIChwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59O1xyXG5pbXBvcnQgeyBpbmplY3QgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCBzZXJ2aWNlIGZyb20gXCIuLi9kZWNvcmF0b3JzL3NlcnZpY2VcIjtcclxuY29uc3QgSVBGUyA9IHJlcXVpcmUoJ2lwZnMnKTtcclxubGV0IElwZnNTZXJ2aWNlID0gY2xhc3MgSXBmc1NlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoaXBmc09wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLmlwZnNPcHRpb25zID0gaXBmc09wdGlvbnM7XHJcbiAgICAgICAgdGhpcy5za2lwQmluZGluZyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBnZXRDbGFzc05hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIFwiSXBmc1NlcnZpY2VcIjtcclxuICAgIH1cclxuICAgIGFzeW5jIGluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5pcGZzID0gYXdhaXQgSVBGUy5jcmVhdGUodGhpcy5pcGZzT3B0aW9ucyk7XHJcbiAgICB9XHJcbn07XHJcbklwZnNTZXJ2aWNlID0gX19kZWNvcmF0ZShbXHJcbiAgICBzZXJ2aWNlKCksXHJcbiAgICBfX3BhcmFtKDAsIGluamVjdChcImlwZnNPcHRpb25zXCIpKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbT2JqZWN0XSlcclxuXSwgSXBmc1NlcnZpY2UpO1xyXG5leHBvcnQgeyBJcGZzU2VydmljZSB9O1xyXG4iLCJ2YXIgX19kZWNvcmF0ZSA9ICh0aGlzICYmIHRoaXMuX19kZWNvcmF0ZSkgfHwgZnVuY3Rpb24gKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XHJcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcclxuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XHJcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xyXG59O1xyXG52YXIgX19tZXRhZGF0YSA9ICh0aGlzICYmIHRoaXMuX19tZXRhZGF0YSkgfHwgZnVuY3Rpb24gKGssIHYpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcclxufTtcclxudmFyIF9fcGFyYW0gPSAodGhpcyAmJiB0aGlzLl9fcGFyYW0pIHx8IGZ1bmN0aW9uIChwYXJhbUluZGV4LCBkZWNvcmF0b3IpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBrZXkpIHsgZGVjb3JhdG9yKHRhcmdldCwga2V5LCBwYXJhbUluZGV4KTsgfVxyXG59O1xyXG5pbXBvcnQgeyBpbmplY3QgfSBmcm9tIFwiaW52ZXJzaWZ5XCI7XHJcbmltcG9ydCB7IElkZW50aXR5U2VydmljZSB9IGZyb20gXCIuL2lkZW50aXR5LXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgSXBmc1NlcnZpY2UgfSBmcm9tIFwiLi9pcGZzLXNlcnZpY2VcIjtcclxuaW1wb3J0IHNlcnZpY2UgZnJvbSBcIi4uL2RlY29yYXRvcnMvc2VydmljZVwiO1xyXG5sZXQgT3JiaXREQiA9IHJlcXVpcmUoJ29yYml0LWRiJyk7XHJcbmxldCBLZXlzdG9yZSA9IHJlcXVpcmUoJ29yYml0LWRiLWtleXN0b3JlJyk7XHJcbmxldCBNZnNTdG9yZSA9IHJlcXVpcmUoJ29yYml0LWRiLW1mc3N0b3JlJyk7XHJcbmxldCBPcmJpdFNlcnZpY2UgPSBjbGFzcyBPcmJpdFNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoaXBmc1NlcnZpY2UsIGlkZW50aXR5U2VydmljZSwgb3JiaXREYk9wdGlvbnMpIHtcclxuICAgICAgICB0aGlzLmlwZnNTZXJ2aWNlID0gaXBmc1NlcnZpY2U7XHJcbiAgICAgICAgdGhpcy5pZGVudGl0eVNlcnZpY2UgPSBpZGVudGl0eVNlcnZpY2U7XHJcbiAgICAgICAgdGhpcy5vcmJpdERiT3B0aW9ucyA9IG9yYml0RGJPcHRpb25zO1xyXG4gICAgICAgIHRoaXMuc2tpcEJpbmRpbmcgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgZ2V0Q2xhc3NOYW1lKCkge1xyXG4gICAgICAgIHJldHVybiBcIk9yYml0U2VydmljZVwiO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgaW5pdCh3YWxsZXQpIHtcclxuICAgICAgICBPcmJpdERCLmFkZERhdGFiYXNlVHlwZShcIm1mc3N0b3JlXCIsIE1mc1N0b3JlKTtcclxuICAgICAgICBpZiAod2FsbGV0KSB7XHJcbiAgICAgICAgICAgIGxldCBrZXlzdG9yZSA9IG5ldyBLZXlzdG9yZSgpO1xyXG4gICAgICAgICAgICBsZXQgaWRlbnRpdHkgPSBhd2FpdCB0aGlzLmlkZW50aXR5U2VydmljZS5nZXRJZGVudGl0eShrZXlzdG9yZSwgd2FsbGV0KTtcclxuICAgICAgICAgICAgdGhpcy5vcmJpdERiT3B0aW9uc1snaWRlbnRpdHknXSA9IGlkZW50aXR5O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9yYml0RGIgPSBhd2FpdCBPcmJpdERCLmNyZWF0ZUluc3RhbmNlKHRoaXMuaXBmc1NlcnZpY2UuaXBmcywgdGhpcy5vcmJpdERiT3B0aW9ucyk7XHJcbiAgICB9XHJcbn07XHJcbk9yYml0U2VydmljZSA9IF9fZGVjb3JhdGUoW1xyXG4gICAgc2VydmljZSgpLFxyXG4gICAgX19wYXJhbSgyLCBpbmplY3QoXCJvcmJpdERiT3B0aW9uc1wiKSksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW0lwZnNTZXJ2aWNlLFxyXG4gICAgICAgIElkZW50aXR5U2VydmljZSwgT2JqZWN0XSlcclxuXSwgT3JiaXRTZXJ2aWNlKTtcclxuZXhwb3J0IHsgT3JiaXRTZXJ2aWNlIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmxldCBQYWdpbmdTZXJ2aWNlID0gY2xhc3MgUGFnaW5nU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG4gICAgYnVpbGRQYWdpbmdWaWV3TW9kZWwob2Zmc2V0LCBsaW1pdCwgY291bnQpIHtcclxuICAgICAgICBsZXQgdmlld01vZGVsID0gbmV3IFBhZ2luZ1ZpZXdNb2RlbCgpO1xyXG4gICAgICAgIHZpZXdNb2RlbC5vZmZzZXQgPSBvZmZzZXQgPyBvZmZzZXQgOiAwO1xyXG4gICAgICAgIHZpZXdNb2RlbC5saW1pdCA9IGxpbWl0O1xyXG4gICAgICAgIHZpZXdNb2RlbC5jb3VudCA9IGNvdW50O1xyXG4gICAgICAgIHZpZXdNb2RlbC5zdGFydCA9IHZpZXdNb2RlbC5vZmZzZXQgKyAxO1xyXG4gICAgICAgIHZpZXdNb2RlbC5lbmQgPSBNYXRoLm1pbih2aWV3TW9kZWwub2Zmc2V0ICsgbGltaXQsIGNvdW50KTtcclxuICAgICAgICB2aWV3TW9kZWwucHJldmlvdXNPZmZzZXQgPSBNYXRoLm1heCh2aWV3TW9kZWwub2Zmc2V0IC0gbGltaXQsIDApO1xyXG4gICAgICAgIGlmICgodmlld01vZGVsLm9mZnNldCArIGxpbWl0KSA8IGNvdW50IC0gMSkge1xyXG4gICAgICAgICAgICB2aWV3TW9kZWwubmV4dE9mZnNldCA9IHZpZXdNb2RlbC5vZmZzZXQgKyBsaW1pdDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZpZXdNb2RlbDtcclxuICAgIH1cclxufTtcclxuUGFnaW5nU2VydmljZSA9IF9fZGVjb3JhdGUoW1xyXG4gICAgc2VydmljZSgpLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtdKVxyXG5dLCBQYWdpbmdTZXJ2aWNlKTtcclxuY2xhc3MgUGFnaW5nVmlld01vZGVsIHtcclxufVxyXG5leHBvcnQgeyBQYWdpbmdTZXJ2aWNlLCBQYWdpbmdWaWV3TW9kZWwgfTtcclxuIiwidmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufTtcclxudmFyIF9fbWV0YWRhdGEgPSAodGhpcyAmJiB0aGlzLl9fbWV0YWRhdGEpIHx8IGZ1bmN0aW9uIChrLCB2KSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEoaywgdik7XHJcbn07XHJcbmltcG9ydCBzZXJ2aWNlIGZyb20gXCIuLi9kZWNvcmF0b3JzL3NlcnZpY2VcIjtcclxubGV0IFF1ZXVlU2VydmljZSA9IGNsYXNzIFF1ZXVlU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG4gICAgYXN5bmMgcXVldWVQcm9taXNlVmlldyhwcm9taXNlVmlldykge1xyXG4gICAgICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIGxldCBxdWV1ZUl0ZW0gPSBuZXcgUXVldWVJdGVtKEd1aWQubmV3R3VpZCgpLCBwcm9taXNlVmlldy5pY29uLCBwcm9taXNlVmlldy50aXRsZSwgcHJvbWlzZVZpZXcudmlldywgcHJvbWlzZVZpZXcuY29udGV4dCk7XHJcbiAgICAgICAgbGV0IGJlZm9yZSA9IGFzeW5jIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgIHNlbGYuYmVmb3JlU2F2ZUFjdGlvbihxdWV1ZUl0ZW0pO1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGxldCBkdXJpbmcgPSBhc3luYyBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwcm9taXNlVmlldy5wcm9taXNlO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgbGV0IGFmdGVyID0gYXN5bmMgZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICBxdWV1ZUl0ZW0uY29udGV4dCA9IHJlc3VsdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2VsZi5hZnRlclNhdmVBY3Rpb24ocXVldWVJdGVtKTtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gYmVmb3JlKClcclxuICAgICAgICAgICAgLnRoZW4oZHVyaW5nKVxyXG4gICAgICAgICAgICAudGhlbihhZnRlcik7XHJcbiAgICB9XHJcbiAgICBiZWZvcmVTYXZlQWN0aW9uKHF1ZXVlSXRlbSkge1xyXG4gICAgICAgIC8vIHF1ZXVlSXRlbS50aXRsZSA9IHRoaXMuX3BhcnNlVGl0bGUocXVldWVJdGVtLnRpdGxlVGVtcGxhdGUsIHF1ZXVlSXRlbS5jb250ZXh0KVxyXG4gICAgICAgIC8vIENyZWF0ZSB0b2FzdCB3aXRoIGNsb3NlIGJ1dHRvblxyXG4gICAgICAgIC8vIHF1ZXVlSXRlbS50b2FzdCA9IEdsb2JhbC5hcHAudG9hc3QuY3JlYXRlKHtcclxuICAgICAgICAvLyAgIHRleHQ6IHF1ZXVlSXRlbS50aXRsZSxcclxuICAgICAgICAvLyAgIGNsb3NlQnV0dG9uOiB0cnVlXHJcbiAgICAgICAgLy8gfSlcclxuICAgICAgICBxdWV1ZUl0ZW0udG9hc3Qub3BlbigpO1xyXG4gICAgfVxyXG4gICAgYWZ0ZXJTYXZlQWN0aW9uKHF1ZXVlSXRlbSkge1xyXG4gICAgICAgIHF1ZXVlSXRlbS50b2FzdC5jbG9zZSgpO1xyXG4gICAgICAgIC8vIHF1ZXVlSXRlbS5saW5rID0gdGhpcy5fcGFyc2VMaW5rKHF1ZXVlSXRlbS5saW5rVGVtcGxhdGUsIHF1ZXVlSXRlbS5jb250ZXh0KVxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHF1ZXVlSXRlbS5jb250ZXh0KVxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHF1ZXVlSXRlbS5saW5rKVxyXG4gICAgICAgIC8vIEdsb2JhbC5hcHAudG9hc3QuY3JlYXRlKHtcclxuICAgICAgICAvLyAgIHRleHQ6IFwiU2F2ZSBDb21wbGV0ZVwiLFxyXG4gICAgICAgIC8vICAgY2xvc2VCdXR0b246IHRydWUsXHJcbiAgICAgICAgLy8gICBjbG9zZUJ1dHRvblRleHQ6IFwiVmlld1wiLFxyXG4gICAgICAgIC8vICAgY2xvc2VUaW1lb3V0OiA1MDAwLFxyXG4gICAgICAgIC8vICAgb246IHtcclxuICAgICAgICAvLyAgICAgdG9hc3RDbG9zZUJ1dHRvbkNsaWNrOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAvLyAgICAgICAvLyBHbG9iYWwubmF2aWdhdGUocXVldWVJdGVtLmxpbmspXHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyB9KS5vcGVuKClcclxuICAgIH1cclxufTtcclxuUXVldWVTZXJ2aWNlID0gX19kZWNvcmF0ZShbXHJcbiAgICBzZXJ2aWNlKCksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW10pXHJcbl0sIFF1ZXVlU2VydmljZSk7XHJcbmNsYXNzIFF1ZXVlSXRlbSB7XHJcbiAgICBjb25zdHJ1Y3RvcihpZCwgaWNvbiwgdGl0bGVUZW1wbGF0ZSwgbGlua1RlbXBsYXRlLCBjb250ZXh0KSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xyXG4gICAgICAgIHRoaXMuaWNvbiA9IGljb247XHJcbiAgICAgICAgdGhpcy50aXRsZVRlbXBsYXRlID0gdGl0bGVUZW1wbGF0ZTtcclxuICAgICAgICB0aGlzLmxpbmtUZW1wbGF0ZSA9IGxpbmtUZW1wbGF0ZTtcclxuICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xyXG4gICAgfVxyXG59XHJcbi8vZnJvbSBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8yNjUwMTY4OC9hLXR5cGVzY3JpcHQtZ3VpZC1jbGFzc1xyXG5jbGFzcyBHdWlkIHtcclxuICAgIHN0YXRpYyBuZXdHdWlkKCkge1xyXG4gICAgICAgIHJldHVybiAneHh4eHh4eHgteHh4eC00eHh4LXl4eHgteHh4eHh4eHh4eHh4Jy5yZXBsYWNlKC9beHldL2csIGZ1bmN0aW9uIChjKSB7XHJcbiAgICAgICAgICAgIHZhciByID0gTWF0aC5yYW5kb20oKSAqIDE2IHwgMCwgdiA9IGMgPT0gJ3gnID8gciA6IChyICYgMHgzIHwgMHg4KTtcclxuICAgICAgICAgICAgcmV0dXJuIHYudG9TdHJpbmcoMTYpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCB7IFF1ZXVlU2VydmljZSwgUXVldWVJdGVtIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG52YXIgX19wYXJhbSA9ICh0aGlzICYmIHRoaXMuX19wYXJhbSkgfHwgZnVuY3Rpb24gKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn07XHJcbmltcG9ydCB7IGluamVjdCB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IHsgVWlTZXJ2aWNlIH0gZnJvbSBcIi4vdWktc2VydmljZVwiO1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmxldCBSb3V0aW5nU2VydmljZSA9IGNsYXNzIFJvdXRpbmdTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHVpU2VydmljZSwgYXBwKSB7XHJcbiAgICAgICAgdGhpcy51aVNlcnZpY2UgPSB1aVNlcnZpY2U7XHJcbiAgICAgICAgdGhpcy5hcHAgPSBhcHA7XHJcbiAgICB9XHJcbiAgICBuYXZpZ2F0ZShuYXZpZ2F0ZVBhcmFtcywgcm91dGVPcHRpb25zLCB2aWV3TmFtZSA9ICdtYWluJykge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGAke3ZpZXdOYW1lfTogbmF2aWdhdGluZyB0byAke0pTT04uc3RyaW5naWZ5KG5hdmlnYXRlUGFyYW1zKX1gKTtcclxuICAgICAgICBpZiAoIXJvdXRlT3B0aW9ucylcclxuICAgICAgICAgICAgcm91dGVPcHRpb25zID0ge1xyXG4gICAgICAgICAgICAgICAgcmVsb2FkQ3VycmVudDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGlnbm9yZUNhY2hlOiBmYWxzZVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIGxldCB2aWV3ID0gdGhpcy5hcHAudmlld1t2aWV3TmFtZV07XHJcbiAgICAgICAgaWYgKHZpZXcpIHtcclxuICAgICAgICAgICAgdmlldy5yb3V0ZXIubmF2aWdhdGUobmF2aWdhdGVQYXJhbXMsIHJvdXRlT3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgQ291bGQgbm90IGZpbmQgdmlldyAke3ZpZXdOYW1lfWApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGJ1aWxkUm91dGVzRm9yQ29udGFpbmVyKGV4aXN0aW5nLCBjb250YWluZXIpIHtcclxuICAgICAgICBsZXQgcm91dGVzID0gW107XHJcbiAgICAgICAgcm91dGVzID0gcm91dGVzLmNvbmNhdChleGlzdGluZyk7XHJcbiAgICAgICAgLy9Mb29rIHVwIHJlcXVlc3RNYXBwaW5ncyBcclxuICAgICAgICBmb3IgKGxldCBtYXBwZWRSb3V0ZSBvZiBnbG9iYWxUaGlzLm1hcHBlZFJvdXRlcykge1xyXG4gICAgICAgICAgICAvL0xvb2sgdXAgbWF0Y2hpbmcgYmVhblxyXG4gICAgICAgICAgICBsZXQgY29udHJvbGxlckJlYW4gPSBjb250YWluZXIuZ2V0KG1hcHBlZFJvdXRlLmNvbnRyb2xsZXJDbGFzcyk7XHJcbiAgICAgICAgICAgIHJvdXRlcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHBhdGg6IG1hcHBlZFJvdXRlLnBhdGgsXHJcbiAgICAgICAgICAgICAgICBhc3luYzogYXN5bmMgKHJvdXRlVG8sIHJvdXRlRnJvbSwgcmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5fZG9SZXNvbHZlKHJvdXRlVG8sIHJvdXRlRnJvbSwgcmVzb2x2ZSwgcmVqZWN0LCBjb250cm9sbGVyQmVhblttYXBwZWRSb3V0ZS5hY3Rpb25dKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBjYXRjaCAoZXgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51aVNlcnZpY2Uuc2hvd0V4Y2VwdGlvblBvcHVwKGV4KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvL05lZWRzIHRvIGJlIGxhc3RcclxuICAgICAgICByb3V0ZXMucHVzaCh7XHJcbiAgICAgICAgICAgIHBhdGg6ICcoLiopJyxcclxuICAgICAgICAgICAgLy8gdXJsOiAncGFnZXMvNDA0Lmh0bWwnLFxyXG4gICAgICAgICAgICBhc3luYyBhc3luYyhyb3V0ZVRvLCByb3V0ZUZyb20sIHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy51aVNlcnZpY2Uuc2hvd1BvcHVwKFwiUGFnZSB3YXMgbm90IGZvdW5kXCIpXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgNDA0IGVycm9yOiAke3JvdXRlVG8ucGF0aH1gKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHJvdXRlcyk7XHJcbiAgICAgICAgcmV0dXJuIHJvdXRlcztcclxuICAgIH1cclxuICAgIGFzeW5jIF9kb1Jlc29sdmUocm91dGVUbywgcm91dGVGcm9tLCByZXNvbHZlLCByZWplY3QsIGNvbnRyb2xsZXJfcHJvbWlzZSkge1xyXG4gICAgICAgIHZhciBfYSwgX2I7XHJcbiAgICAgICAgLy9UT0RPOiB0aGlzIHNob3VsZCBwcm9iYWJseSBiZSBvcHRpb25hbCBzb21laG93LiBcclxuICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93U3Bpbm5lcigpO1xyXG4gICAgICAgIGxldCBtb2RlbFZpZXcgPSBhd2FpdCBjb250cm9sbGVyX3Byb21pc2U7XHJcbiAgICAgICAgaWYgKCFtb2RlbFZpZXcpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICBsZXQgbW9kZWwgPSBhd2FpdCBtb2RlbFZpZXcubW9kZWw7XHJcbiAgICAgICAgbGV0IGNvbnRleHREYXRhO1xyXG4gICAgICAgIC8vSWYgd2UgZ2V0IHBhc3NlZCBhIGNvbXBvbmVudCBpdCBtZWFucyB3ZSdyZSB0cnlpbmcgdG8gcmVzb2x2ZSBiYWNrIHRvIHRoZSBzYW1lIG9uZS5cclxuICAgICAgICAvL1NlZSBTcGFjZUNvbXBvbmVudC5mb3JtU3VibWl0LiBTdGlsbCBhIHdvcmsgaW4gcHJvZ3Jlc3MuIFxyXG4gICAgICAgIGlmICgoX2EgPSByb3V0ZVRvLmNvbnRleHQpID09PSBudWxsIHx8IF9hID09PSB2b2lkIDAgPyB2b2lkIDAgOiBfYS5jb21wb25lbnQpIHtcclxuICAgICAgICAgICAgY29udGV4dERhdGEgPSAoX2IgPSByb3V0ZVRvLmNvbnRleHQpID09PSBudWxsIHx8IF9iID09PSB2b2lkIDAgPyB2b2lkIDAgOiBfYi5kYXRhO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgY29udGV4dERhdGEgPSByb3V0ZVRvLmNvbnRleHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCB1cGRhdGVkU3RhdGUgPSBhd2FpdCBtb2RlbChjb250ZXh0RGF0YSk7XHJcbiAgICAgICAgaWYgKG1vZGVsVmlldy52aWV3KSB7XHJcbiAgICAgICAgICAgIC8vTG9hZCB0aGUgbmV3IGNvbXBvbmVudCBpZiBpdCdzIGdpdmVuIHRvIHVzLiBcclxuICAgICAgICAgICAgcmVzb2x2ZSh7XHJcbiAgICAgICAgICAgICAgICBjb21wb25lbnQ6IG1vZGVsVmlldy52aWV3XHJcbiAgICAgICAgICAgIH0sIHtcclxuICAgICAgICAgICAgICAgIGNvbnRleHQ6IHVwZGF0ZWRTdGF0ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vT3RoZXJ3aXNlIHVwZGF0ZSB0aGUgc3RhdGUgb2YgdGhlIGN1cnJlbnQgY29tcG9uZW50LlxyXG4gICAgICAgICAgICBpZiAocm91dGVUby5jb250ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY29tcG9uZW50ID0gcm91dGVUby5jb250ZXh0LmNvbXBvbmVudDtcclxuICAgICAgICAgICAgICAgIGNvbXBvbmVudC4kc2V0U3RhdGUodXBkYXRlZFN0YXRlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnVpU2VydmljZS5oaWRlU3Bpbm5lcigpO1xyXG4gICAgfVxyXG59O1xyXG5Sb3V0aW5nU2VydmljZSA9IF9fZGVjb3JhdGUoW1xyXG4gICAgc2VydmljZSgpLFxyXG4gICAgX19wYXJhbSgxLCBpbmplY3QoXCJmcmFtZXdvcms3XCIpKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbVWlTZXJ2aWNlLCBPYmplY3RdKVxyXG5dLCBSb3V0aW5nU2VydmljZSk7XHJcbmV4cG9ydCB7IFJvdXRpbmdTZXJ2aWNlIH07XHJcbiIsInZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn07XHJcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xyXG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xyXG59O1xyXG5pbXBvcnQgeyBPcmJpdFNlcnZpY2UgfSBmcm9tIFwiLi9vcmJpdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IElwZnNTZXJ2aWNlIH0gZnJvbSBcIi4vaXBmcy1zZXJ2aWNlXCI7XHJcbmltcG9ydCBzZXJ2aWNlIGZyb20gXCIuLi9kZWNvcmF0b3JzL3NlcnZpY2VcIjtcclxuY29uc3QgV0FMTEVUX1NDSEVNQSA9IFwiV0FMTEVUX1NDSEVNQVwiO1xyXG5sZXQgU2NoZW1hU2VydmljZSA9IGNsYXNzIFNjaGVtYVNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoaXBmc1NlcnZpY2UsIG9yYml0U2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuaXBmc1NlcnZpY2UgPSBpcGZzU2VydmljZTtcclxuICAgICAgICB0aGlzLm9yYml0U2VydmljZSA9IG9yYml0U2VydmljZTtcclxuICAgICAgICB0aGlzLnNraXBCaW5kaW5nID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmxvYWRlZFNjaGVtYXMgPSB7fTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIFNjaGVtYSBvYmplY3QgZnJvbSB0aGUgcGFzc2VkIHBhcmFtZXRlcnMgYW5kIHN0b3JlcyBpdCBpbiBJUEZTLiBSZXR1cm5zIHRoZSBTY2hlbWEuXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG93bmVyQWRkcmVzc1xyXG4gICAgICogQHBhcmFtIHN0b3JlRGVmaW5pdGlvbnNcclxuICAgICAqL1xyXG4gICAgYXN5bmMgY3JlYXRlKG93bmVyQWRkcmVzcywgc3RvcmVEZWZpbml0aW9ucykge1xyXG4gICAgICAgIGxldCBzY2hlbWEgPSB7XHJcbiAgICAgICAgICAgIGFkZHJlc3Nlczoge30sXHJcbiAgICAgICAgICAgIHN0b3JlRGVmaW5pdGlvbnM6IHN0b3JlRGVmaW5pdGlvbnNcclxuICAgICAgICB9O1xyXG4gICAgICAgIGxldCBkZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgY3JlYXRlOiB0cnVlLFxyXG4gICAgICAgICAgICBhY2Nlc3NDb250cm9sbGVyOiB7XHJcbiAgICAgICAgICAgICAgICB3cml0ZTogW293bmVyQWRkcmVzc11cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKHN0b3JlRGVmaW5pdGlvbnMpIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgc3RvcmUgb2Ygc3RvcmVEZWZpbml0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgLy8gbGV0IGNyZWF0ZWRTdG9yZVxyXG4gICAgICAgICAgICAgICAgbGV0IHN0b3JlTmFtZSA9IGAke3N0b3JlLm5hbWV9LSR7b3duZXJBZGRyZXNzLnRvTG93ZXJDYXNlKCl9YDtcclxuICAgICAgICAgICAgICAgIGxldCBjcmVhdGVkU3RvcmU7XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHN0b3JlLnR5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIFwia3ZzdG9yZVwiOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVkU3RvcmUgPSBhd2FpdCB0aGlzLm9yYml0U2VydmljZS5vcmJpdERiLmt2c3RvcmUoc3RvcmVOYW1lLCBkZWZhdWx0T3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgXCJkb2NzXCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWRTdG9yZSA9IGF3YWl0IHRoaXMub3JiaXRTZXJ2aWNlLm9yYml0RGIuZG9jcyhzdG9yZU5hbWUsIGRlZmF1bHRPcHRpb25zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBcIm1mc3N0b3JlXCI6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBvcHRpb25zID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShkZWZhdWx0T3B0aW9ucykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zLnR5cGUgPSBcIm1mc3N0b3JlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnMuc2NoZW1hID0gc3RvcmUuc2NoZW1hO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVkU3RvcmUgPSBhd2FpdCB0aGlzLm9yYml0U2VydmljZS5vcmJpdERiLm9wZW4oc3RvcmVOYW1lLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBzY2hlbWEuYWRkcmVzc2VzW3N0b3JlLm5hbWVdID0gY3JlYXRlZFN0b3JlLmFkZHJlc3MudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBzY2hlbWEub3duZXIgPSBvd25lckFkZHJlc3M7XHJcbiAgICAgICAgLy9Xcml0ZSBnbG9iYWwgc2NoZW1hIHRvIElQRlNcclxuICAgICAgICBsZXQgYnVmZmVyID0gQnVmZmVyLmZyb20oSlNPTi5zdHJpbmdpZnkoc2NoZW1hKSk7XHJcbiAgICAgICAgbGV0IGNpZCA9IGF3YWl0IHRoaXMuaXBmc1NlcnZpY2UuaXBmcy5vYmplY3QucHV0KGJ1ZmZlcik7XHJcbiAgICAgICAgc2NoZW1hLmNpZCA9IGNpZC50b1N0cmluZygpO1xyXG4gICAgICAgIHJldHVybiBzY2hlbWE7XHJcbiAgICB9XHJcbiAgICAvKipcclxuICAgICAqIExvYWRzIFNjaGVtYSBmcm9tIElQRlMgYmFzZWQgb24gY2lkXHJcbiAgICAgKiBAcGFyYW0gY2lkXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIHJlYWQoY2lkKSB7XHJcbiAgICAgICAgbGV0IGxvYWRlZCA9IGF3YWl0IHRoaXMuaXBmc1NlcnZpY2UuaXBmcy5vYmplY3QuZ2V0KGNpZCk7XHJcbiAgICAgICAgbGV0IGRhdGEgPSBsb2FkZWQuRGF0YSA/IGxvYWRlZC5EYXRhLnRvU3RyaW5nKCkgOiBsb2FkZWQuZGF0YS50b1N0cmluZygpO1xyXG4gICAgICAgIGxldCBzY2hlbWEgPSBKU09OLnBhcnNlKGRhdGEpO1xyXG4gICAgICAgIHNjaGVtYS5jaWQgPSBjaWQ7XHJcbiAgICAgICAgcmV0dXJuIHNjaGVtYTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogVGFrZXMgYSBTY2hlbWEgb2JqZWN0IGFuZCB0aGVuIGxvYWRzIGFsbCB0aGUgb3JiaXQgc3RvcmVzIGFzc29jaWF0ZWQgd2l0aCBpdC5cclxuICAgICAqXHJcbiAgICAgKiBSZWFkcyB0aGUgc3RvcmVEZWZpbml0aW9ucyBmcm9tIHRoZSBzY2hlbWEgYW5kIHRoZW4gY2FsbHMgbG9hZCgpIG9uIHRoZSBvcmJpdCBzdG9yZS5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ga2V5XHJcbiAgICAgKiBAcGFyYW0gc2NoZW1hXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGxvYWQoc2NoZW1hKSB7XHJcbiAgICAgICAgbGV0IGxvYWRlZFNjaGVtYSA9IHtcclxuICAgICAgICAgICAgc3RvcmVzOiB7fSxcclxuICAgICAgICAgICAgc2NoZW1hOiBzY2hlbWFcclxuICAgICAgICB9O1xyXG4gICAgICAgIGZvciAobGV0IGZpZWxkIGluIHNjaGVtYS5hZGRyZXNzZXMpIHtcclxuICAgICAgICAgICAgbG9hZGVkU2NoZW1hLnN0b3Jlc1tmaWVsZF0gPSBhd2FpdCB0aGlzLm9yYml0U2VydmljZS5vcmJpdERiLm9wZW4oc2NoZW1hLmFkZHJlc3Nlc1tmaWVsZF0pO1xyXG4gICAgICAgICAgICBsZXQgZGVmaW5pdGlvbiA9IHNjaGVtYS5zdG9yZURlZmluaXRpb25zLmZpbHRlcihkZWYgPT4gZGVmLm5hbWUgPT0gZmllbGQpWzBdO1xyXG4gICAgICAgICAgICBpZiAoZGVmaW5pdGlvbi5sb2FkKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhgTG9hZGluZyAke2ZpZWxkfWApO1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgbG9hZGVkU2NoZW1hLnN0b3Jlc1tmaWVsZF0ubG9hZChkZWZpbml0aW9uLmxvYWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coYFNraXBwaW5nICR7ZmllbGR9YCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5sb2FkZWRTY2hlbWFzW1dBTExFVF9TQ0hFTUFdID0gbG9hZGVkU2NoZW1hO1xyXG4gICAgfVxyXG4gICAgZ2V0U3RvcmUoc3RvcmVOYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubG9hZGVkU2NoZW1hc1tXQUxMRVRfU0NIRU1BXS5zdG9yZXNbc3RvcmVOYW1lXTtcclxuICAgIH1cclxuICAgIGFzeW5jIGdldFNjaGVtYUluZm8oKSB7XHJcbiAgICAgICAgbGV0IHN0b3JlcyA9IHRoaXMubG9hZGVkU2NoZW1hc1tXQUxMRVRfU0NIRU1BXS5zdG9yZXM7XHJcbiAgICAgICAgbGV0IHRyYW5zbGF0ZWQgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gc3RvcmVzKSB7XHJcbiAgICAgICAgICAgIGxldCBzdG9yZSA9IHN0b3Jlc1trZXldO1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSBhd2FpdCBzdG9yZS5jb3VudCgpO1xyXG4gICAgICAgICAgICB0cmFuc2xhdGVkLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgbmFtZToga2V5LFxyXG4gICAgICAgICAgICAgICAgYWRkcmVzczogc3RvcmUuYWRkcmVzcy50b1N0cmluZygpLFxyXG4gICAgICAgICAgICAgICAgbG9hZGVkOiBjb3VudFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy9HZXQgYSBjb3B5IG9mIHRoZSBnbG9iYWwgc2NoZW1hXHJcbiAgICAgICAgbGV0IGdsb2JhbFNjaGVtYSA9IGF3YWl0IHRoaXMubG9hZGVkU2NoZW1hc1tXQUxMRVRfU0NIRU1BXS5zY2hlbWE7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgc3RvcmVzOiB0cmFuc2xhdGVkLFxyXG4gICAgICAgICAgICBnbG9iYWxTY2hlbWE6IGdsb2JhbFNjaGVtYVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn07XHJcblNjaGVtYVNlcnZpY2UgPSBfX2RlY29yYXRlKFtcclxuICAgIHNlcnZpY2UoKSxcclxuICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbSXBmc1NlcnZpY2UsXHJcbiAgICAgICAgT3JiaXRTZXJ2aWNlXSlcclxuXSwgU2NoZW1hU2VydmljZSk7XHJcbmV4cG9ydCB7IFNjaGVtYVNlcnZpY2UgfTtcclxuIiwidmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufTtcclxudmFyIF9fbWV0YWRhdGEgPSAodGhpcyAmJiB0aGlzLl9fbWV0YWRhdGEpIHx8IGZ1bmN0aW9uIChrLCB2KSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEoaywgdik7XHJcbn07XHJcbnZhciBfX3BhcmFtID0gKHRoaXMgJiYgdGhpcy5fX3BhcmFtKSB8fCBmdW5jdGlvbiAocGFyYW1JbmRleCwgZGVjb3JhdG9yKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwga2V5KSB7IGRlY29yYXRvcih0YXJnZXQsIGtleSwgcGFyYW1JbmRleCk7IH1cclxufTtcclxuaW1wb3J0IHsgaW5qZWN0IH0gZnJvbSBcImludmVyc2lmeVwiO1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmxldCBVaVNlcnZpY2UgPSBjbGFzcyBVaVNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoYXBwKSB7XHJcbiAgICAgICAgdGhpcy5hcHAgPSBhcHA7XHJcbiAgICB9XHJcbiAgICBzaG93RXhjZXB0aW9uUG9wdXAoZXgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhleCk7XHJcbiAgICAgICAgdGhpcy5hcHAuZGlhbG9nLmFsZXJ0KGV4Lm1lc3NhZ2UsIFwiVGhlcmUgd2FzIGFuIGVycm9yXCIpO1xyXG4gICAgfVxyXG4gICAgc2hvd1BvcHVwKG1lc3NhZ2UpIHtcclxuICAgICAgICB0aGlzLmFwcC5kaWFsb2cuYWxlcnQobWVzc2FnZSk7XHJcbiAgICB9XHJcbiAgICBzaG93U3Bpbm5lcihtZXNzYWdlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3Bpbm5lckRpYWxvZylcclxuICAgICAgICAgICAgdGhpcy5oaWRlU3Bpbm5lcigpO1xyXG4gICAgICAgIHRoaXMuc3Bpbm5lckRpYWxvZyA9IHRoaXMuYXBwLmRpYWxvZy5wcmVsb2FkZXIobWVzc2FnZSA/IG1lc3NhZ2UgOiBcIkxvYWRpbmdcIik7XHJcbiAgICB9XHJcbiAgICBoaWRlU3Bpbm5lcigpIHtcclxuICAgICAgICBpZiAodGhpcy5zcGlubmVyRGlhbG9nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3Bpbm5lckRpYWxvZy5jbG9zZSgpO1xyXG4gICAgICAgICAgICB0aGlzLnNwaW5uZXJEaWFsb2cgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufTtcclxuVWlTZXJ2aWNlID0gX19kZWNvcmF0ZShbXHJcbiAgICBzZXJ2aWNlKCksXHJcbiAgICBfX3BhcmFtKDAsIGluamVjdChcImZyYW1ld29yazdcIikpLFxyXG4gICAgX19tZXRhZGF0YShcImRlc2lnbjpwYXJhbXR5cGVzXCIsIFtPYmplY3RdKVxyXG5dLCBVaVNlcnZpY2UpO1xyXG5leHBvcnQgeyBVaVNlcnZpY2UgfTtcclxuIiwidmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufTtcclxudmFyIF9fbWV0YWRhdGEgPSAodGhpcyAmJiB0aGlzLl9fbWV0YWRhdGEpIHx8IGZ1bmN0aW9uIChrLCB2KSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEoaywgdik7XHJcbn07XHJcbmNvbnN0IHsgZXRoZXJzLCBXYWxsZXQsIHByb3ZpZGVycyB9ID0gcmVxdWlyZSgnZXRoZXJzJyk7XHJcbmltcG9ydCB7IFdhbGxldERhbyB9IGZyb20gXCIuLi9kYW8vd2FsbGV0LWRhb1wiO1xyXG5pbXBvcnQgc2VydmljZSBmcm9tIFwiLi4vZGVjb3JhdG9ycy9zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFVpU2VydmljZSB9IGZyb20gXCIuL3VpLXNlcnZpY2VcIjtcclxubGV0IFdhbGxldFNlcnZpY2UgPSBjbGFzcyBXYWxsZXRTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHdhbGxldERhbywgdWlTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy53YWxsZXREYW8gPSB3YWxsZXREYW87XHJcbiAgICAgICAgdGhpcy51aVNlcnZpY2UgPSB1aVNlcnZpY2U7XHJcbiAgICAgICAgdGhpcy5za2lwQmluZGluZyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBnZXRDbGFzc05hbWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIFwiV2FsbGV0U2VydmljZVwiO1xyXG4gICAgfVxyXG4gICAgYXN5bmMgaW5pdCgpIHtcclxuICAgICAgICAvL1RPRE86IFByb2JhYmx5IGJyZWFrIHRoaXMgb3V0IGludG8gZGlmZmVyZW50IGltcGxlbWVudGF0aW9uIGRlcGVuZGluZyBvbiB0aGUgc2NlbmFyaW8uIFxyXG4gICAgICAgIC8vVGhlbiBqdXN0IGluamVjdCB0aGUgcmlnaHQgc2NlbmFyaW8gYXQgc3RhcnR1cC4gTWF5YmUgYnVpbHQgdG8gYSBwcm9maWxlIG9yIHdoYXRldmVyIGludmVyc2lmeSBjYWxscyB0aGVtLiBcclxuICAgICAgICBpZiAod2luZG93WydldGhlcmV1bSddKSB7XHJcbiAgICAgICAgICAgIC8vIFJlcXVlc3QgYWNjb3VudCBhY2Nlc3NcclxuICAgICAgICAgICAgYXdhaXQgd2luZG93WydldGhlcmV1bSddLmVuYWJsZSgpO1xyXG4gICAgICAgICAgICAvL0B0cy1pZ25vcmVcclxuICAgICAgICAgICAgd2luZG93LndlYjNQcm92aWRlciA9IHdpbmRvdy5ldGhlcmV1bTtcclxuICAgICAgICAgICAgLy9AdHMtaWdub3JlXHJcbiAgICAgICAgICAgIHdlYjMgPSBuZXcgV2ViMyh3aW5kb3cud2ViM1Byb3ZpZGVyKTtcclxuICAgICAgICAgICAgLy9AdHMtaWdub3JlXHJcbiAgICAgICAgICAgIHRoaXMucHJvdmlkZXIgPSBuZXcgcHJvdmlkZXJzLldlYjNQcm92aWRlcih3ZWIzLmN1cnJlbnRQcm92aWRlcik7XHJcbiAgICAgICAgICAgIHRoaXMud2FsbGV0ID0gdGhpcy5wcm92aWRlci5nZXRTaWduZXIoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vV2UgbmVlZCBvdXIgb3duIHByb3ZpZGVyLlxyXG4gICAgICAgICAgICB0aGlzLnByb3ZpZGVyID0gZXRoZXJzLmdldERlZmF1bHRQcm92aWRlcihcImhvbWVzdGVhZFwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBhc3luYyBjcmVhdGVXYWxsZXQocGFzc3dvcmQpIHtcclxuICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93U3Bpbm5lcihcIkNyZWF0aW5nIHdhbGxldC4gUGxlYXNlIHdhaXQuXCIpO1xyXG4gICAgICAgIC8vR2VuZXJhdGUgcmFuZG9tIHdvcmRzXHJcbiAgICAgICAgbGV0IHdhbGxldCA9IGF3YWl0IFdhbGxldC5jcmVhdGVSYW5kb20oKTtcclxuICAgICAgICB0aGlzLnVpU2VydmljZS5zaG93U3Bpbm5lcihcIkVuY3J5cHRpbmcgd2FsbGV0Li4uXCIpO1xyXG4gICAgICAgIC8vRW5jcnlwdCB3aXRoIGVudGVyZWQgcGFzc3dvcmRcclxuICAgICAgICBsZXQgZW5jcnlwdGVkSnNvbldhbGxldCA9IGF3YWl0IHdhbGxldC5lbmNyeXB0KHBhc3N3b3JkKTtcclxuICAgICAgICBhd2FpdCB0aGlzLndhbGxldERhby5zYXZlV2FsbGV0KGVuY3J5cHRlZEpzb25XYWxsZXQpO1xyXG4gICAgICAgIGF3YWl0IHRoaXMuY29ubmVjdFdhbGxldCh3YWxsZXQpO1xyXG4gICAgICAgIHRoaXMudWlTZXJ2aWNlLmhpZGVTcGlubmVyKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMud2FsbGV0Lm1uZW1vbmljLnBocmFzZS5zcGxpdChcIiBcIik7XHJcbiAgICB9XHJcbiAgICBhc3luYyBjb25uZWN0V2FsbGV0KHdhbGxldCkge1xyXG4gICAgICAgIHRoaXMud2FsbGV0ID0gd2FsbGV0O1xyXG4gICAgICAgIHRoaXMud2FsbGV0ID0gYXdhaXQgdGhpcy53YWxsZXQuY29ubmVjdCh0aGlzLnByb3ZpZGVyKTtcclxuICAgIH1cclxuICAgIGFzeW5jIHVubG9ja1dhbGxldChwYXNzd29yZCkge1xyXG4gICAgICAgIGxldCBzYXZlZFdhbGxldCA9IGF3YWl0IHRoaXMud2FsbGV0RGFvLmxvYWRXYWxsZXQoKTtcclxuICAgICAgICBpZiAoIXNhdmVkV2FsbGV0KVxyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyB3YWxsZXQgdG8gdW5sb2NrXCIpO1xyXG4gICAgICAgIGxldCB3YWxsZXQgPSBhd2FpdCBXYWxsZXQuZnJvbUVuY3J5cHRlZEpzb24oc2F2ZWRXYWxsZXQsIHBhc3N3b3JkKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb25uZWN0V2FsbGV0KHdhbGxldCk7XHJcbiAgICB9XHJcbiAgICBhc3luYyByZXN0b3JlV2FsbGV0KHJlY292ZXJ5U2VlZCwgcGFzc3dvcmQpIHtcclxuICAgICAgICBsZXQgd2FsbGV0ID0gYXdhaXQgV2FsbGV0LmZyb21NbmVtb25pYyhyZWNvdmVyeVNlZWQpO1xyXG4gICAgICAgIGlmICghd2FsbGV0KVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgLy9FbmNyeXB0IHdpdGggZW50ZXJlZCBwYXNzd29yZFxyXG4gICAgICAgIGxldCBlbmNyeXB0ZWRKc29uV2FsbGV0ID0gYXdhaXQgd2FsbGV0LmVuY3J5cHQocGFzc3dvcmQpO1xyXG4gICAgICAgIGF3YWl0IHRoaXMud2FsbGV0RGFvLnNhdmVXYWxsZXQoZW5jcnlwdGVkSnNvbldhbGxldCk7XHJcbiAgICAgICAgYXdhaXQgdGhpcy5jb25uZWN0V2FsbGV0KHdhbGxldCk7XHJcbiAgICAgICAgcmV0dXJuIHdhbGxldDtcclxuICAgIH1cclxuICAgIGdldFdhbGxldCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy53YWxsZXREYW8ubG9hZFdhbGxldCgpO1xyXG4gICAgfVxyXG4gICAgbG9nb3V0KCkge1xyXG4gICAgICAgIHRoaXMud2FsbGV0ID0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG59O1xyXG5XYWxsZXRTZXJ2aWNlID0gX19kZWNvcmF0ZShbXHJcbiAgICBzZXJ2aWNlKCksXHJcbiAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW1dhbGxldERhbyxcclxuICAgICAgICBVaVNlcnZpY2VdKVxyXG5dLCBXYWxsZXRTZXJ2aWNlKTtcclxuZXhwb3J0IHsgV2FsbGV0U2VydmljZSB9O1xyXG4iLCJpbXBvcnQgeyBVaVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3VpLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgUGFnaW5nU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2UvcGFnaW5nLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgSWRlbnRpdHlTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9pZGVudGl0eS1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFdhbGxldFNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3dhbGxldC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gXCJldmVudHNcIjtcclxuaW1wb3J0IHsgU2NoZW1hU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2Uvc2NoZW1hLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgT3JiaXRTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9vcmJpdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IElwZnNTZXJ2aWNlIH0gZnJvbSBcIi4vc2VydmljZS9pcGZzLXNlcnZpY2VcIjtcclxuaW1wb3J0IHsgSW52YWxpZFdhbGxldEV4Y2VwdGlvbiB9IGZyb20gXCIuL3NlcnZpY2UvZXhjZXB0aW9uL2ludmFsaWQtd2FsbGV0LWV4Y2VwdGlvblwiO1xyXG5pbXBvcnQgeyBidWlsZENvbnRhaW5lciB9IGZyb20gXCIuL2ludmVyc2lmeS5jb25maWdcIjtcclxuaW1wb3J0IHsgUm91dGluZ1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3JvdXRpbmctc2VydmljZVwiO1xyXG5pbXBvcnQgeyBRdWV1ZVNlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL3F1ZXVlX3NlcnZpY2VcIjtcclxuaW1wb3J0IHsgQmluZGluZ1NlcnZpY2UgfSBmcm9tIFwiLi9zZXJ2aWNlL2JpbmRpbmctc2VydmljZVwiO1xyXG5sZXQgYXBwTmFtZTtcclxubGV0IGV2ZW50RW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuZXhwb3J0IHZhciBTcGFjZU1WQztcclxuKGZ1bmN0aW9uIChTcGFjZU1WQykge1xyXG4gICAgZnVuY3Rpb24gZ2V0RXZlbnRFbWl0dGVyKCkge1xyXG4gICAgICAgIHJldHVybiBldmVudEVtaXR0ZXI7XHJcbiAgICB9XHJcbiAgICBTcGFjZU1WQy5nZXRFdmVudEVtaXR0ZXIgPSBnZXRFdmVudEVtaXR0ZXI7XHJcbiAgICBmdW5jdGlvbiBnZXRDb250YWluZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIGdsb2JhbFRoaXMuY29udGFpbmVyO1xyXG4gICAgfVxyXG4gICAgU3BhY2VNVkMuZ2V0Q29udGFpbmVyID0gZ2V0Q29udGFpbmVyO1xyXG4gICAgZnVuY3Rpb24gbmFtZSgpIHtcclxuICAgICAgICByZXR1cm4gYXBwTmFtZTtcclxuICAgIH1cclxuICAgIFNwYWNlTVZDLm5hbWUgPSBuYW1lO1xyXG4gICAgZnVuY3Rpb24gYXBwKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoXCJmcmFtZXdvcms3XCIpO1xyXG4gICAgfVxyXG4gICAgU3BhY2VNVkMuYXBwID0gYXBwO1xyXG4gICAgZnVuY3Rpb24gZ2V0V2FsbGV0KCkge1xyXG4gICAgICAgIHJldHVybiBfd2FsbGV0U2VydmljZSgpLndhbGxldDtcclxuICAgIH1cclxuICAgIFNwYWNlTVZDLmdldFdhbGxldCA9IGdldFdhbGxldDtcclxuICAgIGFzeW5jIGZ1bmN0aW9uIGluaXQoY29udGFpbmVyLCBmN0NvbmZpZywgb3duZXJBZGRyZXNzLCBzdG9yZURlZmluaXRpb25zKSB7XHJcbiAgICAgICAgY29udGFpbmVyLmJpbmQoXCJvd25lckFkZHJlc3NcIikudG9Db25zdGFudFZhbHVlKG93bmVyQWRkcmVzcyk7XHJcbiAgICAgICAgY29udGFpbmVyLmJpbmQoJ3N0b3JlRGVmaW5pdGlvbnMnKS50b0NvbnN0YW50VmFsdWUoc3RvcmVEZWZpbml0aW9ucyk7XHJcbiAgICAgICAgYXdhaXQgU3BhY2VNVkMuaW5pdFdhbGxldChjb250YWluZXIsIGY3Q29uZmlnKTtcclxuICAgICAgICBpZiAoU3BhY2VNVkMuZ2V0V2FsbGV0KCkpIHtcclxuICAgICAgICAgICAgU3BhY2VNVkMuaW5pdE1haW5WaWV3KCk7XHJcbiAgICAgICAgICAgIGF3YWl0IFNwYWNlTVZDLmluaXRGaW5pc2gob3duZXJBZGRyZXNzLCBzdG9yZURlZmluaXRpb25zKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIFNwYWNlTVZDLmluaXRNYWluVmlldygpO1xyXG4gICAgICAgICAgICBfcm91dGluZ1NlcnZpY2UoKS5uYXZpZ2F0ZSh7XHJcbiAgICAgICAgICAgICAgICBwYXRoOiBcIi93YWxsZXRcIlxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBTcGFjZU1WQy5pbml0ID0gaW5pdDtcclxuICAgIGFzeW5jIGZ1bmN0aW9uIGluaXRXYWxsZXQoY29udGFpbmVyLCBmN0NvbmZpZykge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiSW5pdGlhbGl6aW5nIFNwYWNlTVZDIHdhbGxldFwiKTtcclxuICAgICAgICBhcHBOYW1lID0gZjdDb25maWcubmFtZTtcclxuICAgICAgICBnbG9iYWxUaGlzLmNvbnRhaW5lciA9IGJ1aWxkQ29udGFpbmVyKGNvbnRhaW5lciwgZjdDb25maWcpO1xyXG4gICAgICAgIC8vSW5jbHVkZSBkZWZhdWx0IHJvdXRpbmdcclxuICAgICAgICBsZXQgcm91dGVzID0gX3JvdXRpbmdTZXJ2aWNlKCkuYnVpbGRSb3V0ZXNGb3JDb250YWluZXIoU3BhY2VNVkMuYXBwKCkucm91dGVzLCBnbG9iYWxUaGlzLmNvbnRhaW5lcik7XHJcbiAgICAgICAgU3BhY2VNVkMuYXBwKCkucm91dGVzID0gcm91dGVzO1xyXG4gICAgICAgIC8vR2V0IElQRlMgb3B0aW9ucyBhbmQgaW5pdGlhbGl6ZVxyXG4gICAgICAgIGF3YWl0IF9pcGZzU2VydmljZSgpLmluaXQoKTtcclxuICAgICAgICAvL0luaXRpYWxpemUgd2FsbGV0XHJcbiAgICAgICAgYXdhaXQgX3dhbGxldFNlcnZpY2UoKS5pbml0KCk7XHJcbiAgICB9XHJcbiAgICBTcGFjZU1WQy5pbml0V2FsbGV0ID0gaW5pdFdhbGxldDtcclxuICAgIGFzeW5jIGZ1bmN0aW9uIGluaXRTY2hlbWEob3duZXJBZGRyZXNzLCBzdG9yZURlZmluaXRpb25zKSB7XHJcbiAgICAgICAgaWYgKCFTcGFjZU1WQy5nZXRXYWxsZXQoKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgSW52YWxpZFdhbGxldEV4Y2VwdGlvbihcIk5vIHdhbGxldCBmb3VuZC4gVW5hYmxlIHRvIGluaXRpYWxpemUuXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgd2FsbGV0QWRkcmVzcyA9IGF3YWl0IFNwYWNlTVZDLmdldFdhbGxldCgpLmdldEFkZHJlc3MoKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhgSW5pdGlhbGl6aW5nICR7YXBwTmFtZX0gZm9yIHdhbGxldDogJHt3YWxsZXRBZGRyZXNzfWApO1xyXG4gICAgICAgIF91aVNlcnZpY2UoKS5zaG93U3Bpbm5lcignSW5pdGlhbGl6aW5nIHdhbGxldCcpO1xyXG4gICAgICAgIGF3YWl0IF9vcmJpdFNlcnZpY2UoKS5pbml0KFNwYWNlTVZDLmdldFdhbGxldCgpKTtcclxuICAgICAgICBsZXQgc2NoZW1hID0gYXdhaXQgX3NjaGVtYVNlcnZpY2UoKS5jcmVhdGUoYXdhaXQgU3BhY2VNVkMuZ2V0V2FsbGV0KCkuZ2V0QWRkcmVzcygpLCBzdG9yZURlZmluaXRpb25zKTtcclxuICAgICAgICBhd2FpdCBfc2NoZW1hU2VydmljZSgpLmxvYWQoc2NoZW1hKTtcclxuICAgICAgICAvL0luaXQgYWxsIHRoZSBJbml0aWFsaXphYmxlIGJpbmRpbmdzIGluIHRoZSBjb250YWluZXIgXHJcbiAgICAgICAgYXdhaXQgX2JpbmRpbmdTZXJ2aWNlKCkuaW5pdEJpbmRpbmdzKGdldENvbnRhaW5lcigpKTtcclxuICAgICAgICBfdWlTZXJ2aWNlKCkuaGlkZVNwaW5uZXIoKTtcclxuICAgIH1cclxuICAgIFNwYWNlTVZDLmluaXRTY2hlbWEgPSBpbml0U2NoZW1hO1xyXG4gICAgZnVuY3Rpb24gaW5pdE1haW5WaWV3KCkge1xyXG4gICAgICAgIC8vQ3JlYXRlIG1haW4gdmlld1xyXG4gICAgICAgIFNwYWNlTVZDLmFwcCgpLnZpZXdzLmNyZWF0ZSgnLnZpZXctbWFpbicsIHtcclxuICAgICAgICAgICAgcHVzaFN0YXRlOiB0cnVlLFxyXG4gICAgICAgICAgICBsb2FkSW5pdGlhbFBhZ2U6IGZhbHNlXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBTcGFjZU1WQy5pbml0TWFpblZpZXcgPSBpbml0TWFpblZpZXc7XHJcbiAgICBhc3luYyBmdW5jdGlvbiBpbml0RmluaXNoKG93bmVyQWRkcmVzcywgc3RvcmVEZWZpbml0aW9ucykge1xyXG4gICAgICAgIGF3YWl0IFNwYWNlTVZDLmluaXRTY2hlbWEob3duZXJBZGRyZXNzLCBzdG9yZURlZmluaXRpb25zKTtcclxuICAgICAgICBfcm91dGluZ1NlcnZpY2UoKS5uYXZpZ2F0ZSh7XHJcbiAgICAgICAgICAgIHBhdGg6IFwiL1wiXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgZXZlbnRFbWl0dGVyLmVtaXQoJ3NwYWNlbXZjOmluaXRGaW5pc2gnKTtcclxuICAgIH1cclxuICAgIFNwYWNlTVZDLmluaXRGaW5pc2ggPSBpbml0RmluaXNoO1xyXG4gICAgZnVuY3Rpb24gX2lwZnNTZXJ2aWNlKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoSXBmc1NlcnZpY2UpO1xyXG4gICAgfVxyXG4gICAgZnVuY3Rpb24gX29yYml0U2VydmljZSgpIHtcclxuICAgICAgICByZXR1cm4gZ2V0Q29udGFpbmVyKCkuZ2V0KE9yYml0U2VydmljZSk7XHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiBfcXVldWVTZXJ2aWNlKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoUXVldWVTZXJ2aWNlKTtcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIF9wYWdpbmdTZXJ2aWNlKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoUGFnaW5nU2VydmljZSk7XHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiBfdWlTZXJ2aWNlKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoVWlTZXJ2aWNlKTtcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIF9pZGVudGl0eVNlcnZpY2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIGdldENvbnRhaW5lcigpLmdldChJZGVudGl0eVNlcnZpY2UpO1xyXG4gICAgfVxyXG4gICAgZnVuY3Rpb24gX3dhbGxldFNlcnZpY2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIGdldENvbnRhaW5lcigpLmdldChXYWxsZXRTZXJ2aWNlKTtcclxuICAgIH1cclxuICAgIGZ1bmN0aW9uIF9zY2hlbWFTZXJ2aWNlKCkge1xyXG4gICAgICAgIHJldHVybiBnZXRDb250YWluZXIoKS5nZXQoU2NoZW1hU2VydmljZSk7XHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiBfcm91dGluZ1NlcnZpY2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIGdldENvbnRhaW5lcigpLmdldChSb3V0aW5nU2VydmljZSk7XHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiBfYmluZGluZ1NlcnZpY2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIGdldENvbnRhaW5lcigpLmdldChCaW5kaW5nU2VydmljZSk7XHJcbiAgICB9XHJcbn0pKFNwYWNlTVZDIHx8IChTcGFjZU1WQyA9IHt9KSk7XHJcbiIsImNsYXNzIE1vZGVsVmlldyB7XHJcbiAgICBjb25zdHJ1Y3Rvcihtb2RlbCwgdmlldykge1xyXG4gICAgICAgIHRoaXMubW9kZWwgPSBtb2RlbDtcclxuICAgICAgICB0aGlzLnZpZXcgPSB2aWV3O1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCB7IE1vZGVsVmlldyB9O1xyXG4iLCJjbGFzcyBQcm9taXNlVmlldyB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcm9taXNlLCB0aXRsZSwgaWNvbiwgY29udGV4dCwgdmlldykge1xyXG4gICAgICAgIHRoaXMucHJvbWlzZSA9IHByb21pc2U7XHJcbiAgICAgICAgdGhpcy50aXRsZSA9IHRpdGxlO1xyXG4gICAgICAgIHRoaXMuaWNvbiA9IGljb247XHJcbiAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcclxuICAgICAgICB0aGlzLnZpZXcgPSB2aWV3O1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCB7IFByb21pc2VWaWV3IH07XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ2ZyYW1ld29yazcnO1xyXG5pbXBvcnQgeyBTcGFjZU1WQyB9IGZyb20gJy4uL3NwYWNlLW12Yyc7XHJcbmltcG9ydCB7IFJvdXRpbmdTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZS9yb3V0aW5nLXNlcnZpY2UnO1xyXG5jbGFzcyBTcGFjZUNvbXBvbmVudCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBnZXRBcHAoKSB7XHJcbiAgICAgICAgcmV0dXJuIFNwYWNlTVZDLmFwcCgpO1xyXG4gICAgfVxyXG4gICAgZ2V0Um91dGluZ1NlcnZpY2UoKSB7XHJcbiAgICAgICAgcmV0dXJuIFNwYWNlTVZDLmdldENvbnRhaW5lcigpLmdldChSb3V0aW5nU2VydmljZSk7XHJcbiAgICB9XHJcbiAgICBuYXZpZ2F0ZShuYXZpZ2F0ZVBhcmFtcywgcm91dGVPcHRpb25zLCB2aWV3TmFtZSkge1xyXG4gICAgICAgIHRoaXMuZ2V0Um91dGluZ1NlcnZpY2UoKS5uYXZpZ2F0ZShuYXZpZ2F0ZVBhcmFtcywgcm91dGVPcHRpb25zLCB2aWV3TmFtZSk7XHJcbiAgICB9XHJcbiAgICBzdWJtaXRGb3JtKGUsIGZvcm1JZCkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBsZXQgZGF0YSA9IFNwYWNlTVZDLmFwcCgpLmZvcm0uY29udmVydFRvRGF0YShmb3JtSWQpO1xyXG4gICAgICAgIC8vQHRzLWlnbm9yZVxyXG4gICAgICAgIGxldCBmb3JtID0gdGhpcy4kJChmb3JtSWQpWzBdO1xyXG4gICAgICAgIC8vQHRzLWlnbm9yZVxyXG4gICAgICAgIGlmICghZm9ybS5jaGVja1ZhbGlkaXR5KCkpXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAvL0B0cy1pZ25vcmVcclxuICAgICAgICBsZXQgYWN0aW9uID0gdGhpcy4kJChmb3JtKS5hdHRyKCdhY3Rpb24nKTtcclxuICAgICAgICB0aGlzLmdldFJvdXRpbmdTZXJ2aWNlKCkubmF2aWdhdGUoe1xyXG4gICAgICAgICAgICBwYXRoOiBhY3Rpb25cclxuICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgIGlnbm9yZUNhY2hlOiB0cnVlLFxyXG4gICAgICAgICAgICBjb250ZXh0OiB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50OiB0aGlzXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG5leHBvcnQgeyBTcGFjZUNvbXBvbmVudCB9O1xyXG4iLCJpbXBvcnQgXCJyZWZsZWN0LW1ldGFkYXRhXCI7XHJcbmltcG9ydCB7IENvbnRhaW5lciB9IGZyb20gXCJpbnZlcnNpZnlcIjtcclxuaW1wb3J0IHsgSXBmc1NlcnZpY2UgfSBmcm9tIFwiLi4vc3JjL3NlcnZpY2UvaXBmcy1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE9yYml0U2VydmljZSB9IGZyb20gXCIuLi9zcmMvc2VydmljZS9vcmJpdC1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IElkZW50aXR5U2VydmljZSB9IGZyb20gXCIuLi9zcmMvc2VydmljZS9pZGVudGl0eS1zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEJpbmRpbmdTZXJ2aWNlIH0gZnJvbSBcIi4uL3NyYy9zZXJ2aWNlL2JpbmRpbmctc2VydmljZVwiO1xyXG5pbXBvcnQgeyBTY2hlbWFTZXJ2aWNlIH0gZnJvbSBcIi4uL3NyYy9zZXJ2aWNlL3NjaGVtYS1zZXJ2aWNlXCI7XHJcbmxldCBjb250YWluZXI7XHJcbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBpbml0VGVzdENvbnRhaW5lcihjb250YWluZXIsIHN0b3JlRGVmaW5pdGlvbnMpIHtcclxuICAgIC8vRGVmYXVsdCBJUEZTIG9wdGlvbnMgaWYgd2UncmUgbm90IGdpdmVuIGFueVxyXG4gICAgaWYgKCFjb250YWluZXIuaXNCb3VuZChcImlwZnNPcHRpb25zXCIpKSB7XHJcbiAgICAgICAgY29udGFpbmVyLmJpbmQoXCJpcGZzT3B0aW9uc1wiKS50b0NvbnN0YW50VmFsdWUoe1xyXG4gICAgICAgICAgICByZXBvOiAnLi90ZXN0Ly50bXAvdGVzdC1yZXBvcy8nICsgTWF0aC5yYW5kb20oKS50b1N0cmluZygpXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoIWNvbnRhaW5lci5pc0JvdW5kKFwib3JiaXREYk9wdGlvbnNcIikpIHtcclxuICAgICAgICBjb250YWluZXIuYmluZChcIm9yYml0RGJPcHRpb25zXCIpLnRvQ29uc3RhbnRWYWx1ZSh7XHJcbiAgICAgICAgICAgIGRpcmVjdG9yeTogXCIuL3Rlc3QvLnRtcC9vcmJpdGRiL1wiICsgTWF0aC5yYW5kb20oKS50b1N0cmluZygpXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBjb250YWluZXIuYmluZChJZGVudGl0eVNlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIGNvbnRhaW5lci5iaW5kKE9yYml0U2VydmljZSkudG9TZWxmKCkuaW5TaW5nbGV0b25TY29wZSgpO1xyXG4gICAgY29udGFpbmVyLmJpbmQoSXBmc1NlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIGNvbnRhaW5lci5iaW5kKFNjaGVtYVNlcnZpY2UpLnRvU2VsZigpLmluU2luZ2xldG9uU2NvcGUoKTtcclxuICAgIGNvbnRhaW5lci5iaW5kKEJpbmRpbmdTZXJ2aWNlKS50b1NlbGYoKS5pblNpbmdsZXRvblNjb3BlKCk7XHJcbiAgICBsZXQgaXBmc1NlcnZpY2UgPSBjb250YWluZXIuZ2V0KElwZnNTZXJ2aWNlKTtcclxuICAgIGxldCBvcmJpdFNlcnZpY2UgPSBjb250YWluZXIuZ2V0KE9yYml0U2VydmljZSk7XHJcbiAgICBsZXQgc2NoZW1hU2VydmljZSA9IGNvbnRhaW5lci5nZXQoU2NoZW1hU2VydmljZSk7XHJcbiAgICBsZXQgYmluZGluZ1NlcnZpY2UgPSBjb250YWluZXIuZ2V0KEJpbmRpbmdTZXJ2aWNlKTtcclxuICAgIGF3YWl0IGlwZnNTZXJ2aWNlLmluaXQoKTtcclxuICAgIGF3YWl0IG9yYml0U2VydmljZS5pbml0KCk7XHJcbiAgICBsZXQgc2NoZW1hID0gYXdhaXQgc2NoZW1hU2VydmljZS5jcmVhdGUob3JiaXRTZXJ2aWNlLm9yYml0RGIuaWRlbnRpdHkuaWQsIHN0b3JlRGVmaW5pdGlvbnMpO1xyXG4gICAgYXdhaXQgc2NoZW1hU2VydmljZS5sb2FkKHNjaGVtYSk7XHJcbiAgICBhd2FpdCBiaW5kaW5nU2VydmljZS5pbml0QmluZGluZ3MoY29udGFpbmVyKTtcclxuICAgIHJldHVybiBjb250YWluZXI7XHJcbn1cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRlc3RDb250YWluZXIoKSB7XHJcbiAgICBpZiAoY29udGFpbmVyKVxyXG4gICAgICAgIHJldHVybiBjb250YWluZXI7XHJcbiAgICBjb250YWluZXIgPSBuZXcgQ29udGFpbmVyKCk7XHJcbiAgICByZXR1cm4gaW5pdFRlc3RDb250YWluZXIoY29udGFpbmVyLCB1bmRlZmluZWQpO1xyXG59XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV0aGVyc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJldmVudHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnJhbWV3b3JrN1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmcmFtZXdvcms3L2pzL2ZyYW1ld29yazcuYnVuZGxlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImludmVyc2lmeVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJpcGZzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImxldmVsLWpzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImxldmVsdXBcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwib3JiaXQtZGJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwib3JiaXQtZGItaWRlbnRpdHktcHJvdmlkZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwib3JiaXQtZGItaWRlbnRpdHktcHJvdmlkZXIvc3JjL2V0aGVyZXVtLWlkZW50aXR5LXByb3ZpZGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm9yYml0LWRiLWtleXN0b3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm9yYml0LWRiLW1mc3N0b3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZmxlY3QtbWV0YWRhdGFcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwidGVtcGxhdGU3XCIpOyJdLCJzb3VyY2VSb290IjoiIn0=