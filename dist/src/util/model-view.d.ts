declare class ModelView {
    model: (data: any) => any;
    view?: any;
    constructor(model: (data: any) => any, view?: any);
}
export { ModelView };
