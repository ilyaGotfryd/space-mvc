import { IdentityService } from "./identity-service";
import { IpfsService } from "./ipfs-service";
declare class OrbitService {
    private ipfsService;
    private identityService;
    private orbitDbOptions;
    skipBinding: boolean;
    orbitDb: any;
    constructor(ipfsService: IpfsService, identityService: IdentityService, orbitDbOptions: any);
    getClassName(): string;
    init(wallet?: any): Promise<void>;
}
export { OrbitService };
