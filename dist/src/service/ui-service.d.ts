declare class UiService {
    app: any;
    constructor(app: any);
    showExceptionPopup(ex: any): void;
    showPopup(message: any): void;
    /**
     * Spinner
     */
    spinnerDialog: any;
    showSpinner(message?: string): void;
    hideSpinner(): void;
}
export { UiService };
