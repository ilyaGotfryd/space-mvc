declare class IdentityService {
    private identity;
    constructor();
    getIdentity(keystore: any, wallet: any): Promise<any>;
    getAccessController(orbitdb: any): {
        write: string[];
    };
}
export { IdentityService };
