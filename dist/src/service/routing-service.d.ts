import { Container } from "inversify";
import { UiService } from "./ui-service";
import { Router } from "framework7/modules/router/router";
declare class RoutingService {
    private uiService;
    app: any;
    constructor(uiService: UiService, app: any);
    navigate(navigateParams: Router.NavigateParameters, routeOptions?: Router.RouteOptions, viewName?: string): void;
    buildRoutesForContainer(existing: any, container: Container): Router.RouteParameters[];
    private _doResolve;
}
interface Route {
    path: string;
    method: string;
}
export { RoutingService, Route };
