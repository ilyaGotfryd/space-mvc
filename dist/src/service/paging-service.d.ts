declare class PagingService {
    constructor();
    buildPagingViewModel(offset: number, limit: number, count: number): PagingViewModel;
}
declare class PagingViewModel {
    offset: number;
    limit: number;
    count: number;
    start: number;
    end: number;
    previousOffset: number;
    nextOffset: number;
}
export { PagingService, PagingViewModel };
