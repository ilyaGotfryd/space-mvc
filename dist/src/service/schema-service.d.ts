import { OrbitService } from "./orbit-service";
import { IpfsService } from "./ipfs-service";
import { LoadedSchema, Schema } from "../dto/schema";
import { StoreDefinition } from "../dto/store-definition";
declare class SchemaService {
    private ipfsService;
    private orbitService;
    skipBinding: boolean;
    loadedSchemas: {
        [key: string]: LoadedSchema;
    };
    constructor(ipfsService: IpfsService, orbitService: OrbitService);
    /**
     * Creates a Schema object from the passed parameters and stores it in IPFS. Returns the Schema.
     *
     * @param ownerAddress
     * @param storeDefinitions
     */
    create(ownerAddress: string, storeDefinitions: StoreDefinition[]): Promise<Schema>;
    /**
     * Loads Schema from IPFS based on cid
     * @param cid
     */
    read(cid: string): Promise<Schema>;
    /**
     * Takes a Schema object and then loads all the orbit stores associated with it.
     *
     * Reads the storeDefinitions from the schema and then calls load() on the orbit store.
     *
     * @param key
     * @param schema
     */
    load(schema: Schema): Promise<void>;
    getStore(storeName: string): any;
    getSchemaInfo(): Promise<{
        stores: any[];
        globalSchema: Schema;
    }>;
}
export { SchemaService, Schema, StoreDefinition };
