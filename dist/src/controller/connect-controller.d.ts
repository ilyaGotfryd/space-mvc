import { UiService } from "../service/ui-service";
import { ConnectService } from "../service/connect-service";
import { ModelView } from "../util/model-view";
import { RoutingService } from "../service/routing-service";
import { SchemaService } from "../service/schema-service";
declare class ConnectController {
    private connectService;
    private uiService;
    private routingService;
    private schemaService;
    constructor(connectService: ConnectService, uiService: UiService, routingService: RoutingService, schemaService: SchemaService);
    index(): Promise<ModelView>;
    addPeer(): Promise<ModelView>;
}
export { ConnectController };
