declare class WalletDao {
    skipBinding: boolean;
    db: any;
    constructor();
    saveWallet(wallet: any): Promise<any>;
    loadWallet(): Promise<any>;
}
export { WalletDao };
