declare function routeMap(value: string): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void;
export { routeMap };
