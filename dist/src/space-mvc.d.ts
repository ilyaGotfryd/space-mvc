/// <reference types="node" />
import { EventEmitter } from "events";
import { StoreDefinition } from "./dto/store-definition";
import { Container } from "inversify";
export declare namespace SpaceMVC {
    function getEventEmitter(): EventEmitter;
    function getContainer(): Container;
    function name(): string;
    function app(): any;
    function getWallet(): any;
    function init(container: Container, f7Config: any, ownerAddress: string, storeDefinitions: StoreDefinition[]): Promise<void>;
    function initWallet(container: Container, f7Config: any): Promise<void>;
    function initSchema(ownerAddress: string, storeDefinitions: StoreDefinition[]): Promise<void>;
    function initMainView(): void;
    function initFinish(ownerAddress: string, storeDefinitions: StoreDefinition[]): Promise<void>;
}
