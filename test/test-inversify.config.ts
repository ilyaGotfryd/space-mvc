import "reflect-metadata"

import { Container } from "inversify";
import { IpfsService } from "../src/service/ipfs-service";
import { OrbitService } from "../src/service/orbit-service";
import { IdentityService } from "../src/service/identity-service";
import { BindingService } from "../src/service/binding-service";
import { StoreDefinition } from "../src/dto/store-definition";
import { SchemaService } from "../src/service/schema-service";


let container:Container 

export async function initTestContainer(container: Container, storeDefinitions:StoreDefinition[]): Promise<Container> {
    
    //Default IPFS options if we're not given any
    if (!container.isBound("ipfsOptions")) {
        container.bind("ipfsOptions").toConstantValue({
            repo: './test/.tmp/test-repos/' + Math.random().toString()
        })
    }

    if (!container.isBound("orbitDbOptions")) {
        container.bind("orbitDbOptions").toConstantValue({
            directory: "./test/.tmp/orbitdb/" + Math.random().toString()
        })
    }


    container.bind(IdentityService).toSelf().inSingletonScope()
    container.bind(OrbitService).toSelf().inSingletonScope()
    container.bind(IpfsService).toSelf().inSingletonScope()
    container.bind(SchemaService).toSelf().inSingletonScope()
    container.bind(BindingService).toSelf().inSingletonScope()


    let ipfsService: IpfsService = container.get(IpfsService)
    let orbitService: OrbitService = container.get(OrbitService)
    let schemaService:SchemaService = container.get(SchemaService)
    let bindingService:BindingService = container.get(BindingService)

    await ipfsService.init()
    await orbitService.init()


    let schema = await schemaService.create(orbitService.orbitDb.identity.id, storeDefinitions)
    await schemaService.load(schema)

    
    await bindingService.initBindings(container)  

    return container
}


export async function getTestContainer() {
    if (container) return container

    container = new Container()
    return initTestContainer(container, undefined)

}
